﻿// <copyright file="Program.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.OfficeToPdf
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using CommandLine;
    using CommandLine.Text;

    public class Program
    {
        private const int MaxWaitSeconds = 28;

        public static void Main(string[] args)
        {
            var options = new Options();
            if (Parser.Default.ParseArguments(args, options))
            {
                if (options.Verbose)
                {
                    Console.WriteLine("Input File: {0}", options.InputFile);
                    Console.WriteLine("Output File: {0}", options.OutputFile);
                }

                Helper.OfficeToPdf(options.InputFile, options.OutputFile);
            }
            else
            {
                MonitorDirectoryForNewFiles();

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
        }

        private static void MonitorDirectoryForNewFiles()
        {
            var watcher = new FileSystemWatcher();
            watcher.Path = ConfigurationManager.AppSettings["FileStubPath"];
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.*";
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;

            Console.WriteLine("Monitoring directory: " + watcher.Path + " ...");
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Task.Run(() => Program.ConvertToPdf(e.FullPath));
        }

        private static Task<bool> ConvertToPdf(string path)
        {
            if (File.Exists(path))
            {
                string inputFilename = Path.Combine(ConfigurationManager.AppSettings["FileConvertPath"], Path.GetFileName(path));
                string outputFilename = Path.Combine(ConfigurationManager.AppSettings["FileConvertPath"], Path.GetFileNameWithoutExtension(path) + ".pdf");

                KillOlderProcesses();

                var process = new Process();
                process.StartInfo.FileName = ConfigurationManager.AppSettings["OfficeToPdfProcessPath"];
                process.StartInfo.Arguments = string.Format("-i \"{0}\" -o \"{1}\"", inputFilename, outputFilename);
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.ErrorDialog = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardInput = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.Start();

                Console.WriteLine("File queued: " + inputFilename);

                var s = new Stopwatch();
                s.Start();

                try
                {
                    while (!process.HasExited)
                    {
                        process.WaitForExit(1000);
                        if (!process.HasExited && s.ElapsedMilliseconds >= MaxWaitSeconds * 1000)
                        {
                            process.Kill();
                            KillOlderProcesses();
                            Console.WriteLine();
                            Console.WriteLine("Aborted Office to PDF conversion due to timeout: " + inputFilename);
                            Console.WriteLine();
                        }
                    }
                }
                finally
                {
                    if (File.Exists(inputFilename))
                    {
                        File.Delete(inputFilename);
                    }

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                }

                Console.WriteLine("Office to PDF processing completed for: " + inputFilename);
            }

            return Task.FromResult(true);
        }

        private static void KillOlderProcesses()
        {
            var officeProcesses = Process.GetProcesses().Where(pr => pr.ProcessName.ToLower() == "winword"
                                                                  || pr.ProcessName.ToLower() == "excel"
                                                                  || pr.ProcessName.ToLower() == "powerpnt");

            foreach (var process in officeProcesses)
            {
                if (DateTime.Now > process.StartTime.AddSeconds(30))
                {
                    process.Kill();
                }
            }
        }

        private class Options
        {
            [Option('i', "input", Required = true, HelpText = "Input file to be processed.")]
            public string InputFile { get; set; }

            [Option('o', "output", Required = true, HelpText = "Output file to be generated.")]
            public string OutputFile { get; set; }

            [Option('v', "verbose", DefaultValue = true, HelpText = "Prints all messages to standard output.")]
            public bool Verbose { get; set; }

            [ParserState]
            public IParserState LastParserState { get; set; }

            [HelpOption]
            public string GetUsage()
            {
                return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
            }
        }
    }
}
