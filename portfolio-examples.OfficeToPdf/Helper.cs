﻿// <copyright file="Helper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.OfficeToPdf
{
    using System;
    using System.IO;
    using System.Linq;

    public class Helper
    {
        public static void OfficeToPdf(string inputFilename, string outputFilename)
        {
            var wordDocumentTypes = new string[] { ".doc", ".docx", ".rtf" };
            var excelDocumentTypes = new string[] { ".xls", ".xlsx" };
            var powerPointDocumentTypes = new string[] { ".ppt", ".pptx" };

            var fileExtension = Path.GetExtension(inputFilename).ToLower();

            if (wordDocumentTypes.Contains(fileExtension))
            {
                WordToPdf(inputFilename, outputFilename);
            }
            else if (excelDocumentTypes.Contains(fileExtension))
            {
                ExcelToPdf(inputFilename, outputFilename);
            }
            else if (powerPointDocumentTypes.Contains(fileExtension))
            {
                PowerPointToPdf(inputFilename, outputFilename);
            }
        }

        // The following code is from: http://www.codeproject.com/Tips/592957/Converting-Document-Word-Excel
        // -------------------------------------------------------------------------------------------------
        public static short WordToPdf(object originalDocPath, object pdfPath)
        {
            short convertWord2PdfResult = -1;

            Microsoft.Office.Interop.Word.Application wordDoc = null;
            Microsoft.Office.Interop.Word.Document doc = null;

            // C# doesn't have optional arguments so we'll need a dummy value
            object missing = System.Reflection.Missing.Value;

            try
            {
                // Start MS word application
                wordDoc = new Microsoft.Office.Interop.Word.Application
                {
                    Visible = false,
                    ScreenUpdating = false
                };

                // Open Document
                doc = wordDoc.Documents.Open(
                    ref originalDocPath,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing,
                    ref missing);

                if (doc != null)
                {
                    doc.Activate();

                    // Save Document as PDF
                    object fileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF;
                    doc.SaveAs(
                        ref pdfPath,
                        ref fileFormat,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing,
                        ref missing);

                    convertWord2PdfResult = 0;
                }
                else
                {
                    Console.WriteLine("Error occured for conversion of office Word to PDF");
                    convertWord2PdfResult = 504;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured for conversion of office Word to PDF, Exception: ", ex);
                convertWord2PdfResult = 504;
            }
            finally
            {
                // Close and release the Document object.
                if (doc != null)
                {
                    object saveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                    ((Microsoft.Office.Interop.Word._Document)doc).Close(ref saveChanges, ref missing, ref missing);
                    ReleaseObject(doc);
                }

                // Quit Word and release the ApplicationClass object.
                ((Microsoft.Office.Interop.Word._Application)wordDoc).Quit(ref missing, ref missing, ref missing);
                ReleaseObject(wordDoc);
                wordDoc = null;
            }

            return convertWord2PdfResult;
        }

        /// <summary>
        ///  Convert Excel file to PDF by calling required method
        /// </summary>
        /// <param name="originalXlsPath">file path</param>
        /// <param name="pdfPath">Target PDF path</param>
        /// <returns>error code : 0(sucess)/ -1 or errorcode (unknown error or failure)</returns>
        public static short ExcelToPdf(string originalXlsPath, string pdfPath)
        {
            short convertExcel2PdfResult = -1;

            // Create COM Objects
            Microsoft.Office.Interop.Excel.Application excelApplication = null;
            Microsoft.Office.Interop.Excel.Workbook excelWorkbook = null;
            object unknownType = Type.Missing;

            // Create new instance of Excel
            try
            {
                // Open excel application
                excelApplication = new Microsoft.Office.Interop.Excel.Application
                {
                    ScreenUpdating = false,
                    DisplayAlerts = false
                };

                // Open excel sheet
                if (excelApplication != null)
                {
                    excelWorkbook = excelApplication.Workbooks.Open(
                        originalXlsPath,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType);
                }

                if (excelWorkbook != null)
                {
                    // Call Excel's native export function (valid in Office 2007 and Office 2010, AFAIK)
                    excelWorkbook.ExportAsFixedFormat(
                        Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF,
                        pdfPath,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType,
                        unknownType);

                    convertExcel2PdfResult = 0;
                }
                else
                {
                    Console.WriteLine("Error occured for conversion of office excel to PDF ");
                    convertExcel2PdfResult = 504;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured for conversion of office excel to PDF, Exception: ", ex);
                convertExcel2PdfResult = 504;
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                if (excelWorkbook != null)
                {
                    excelWorkbook.Close(unknownType, unknownType, unknownType);
                }

                if (excelApplication != null)
                {
                    excelApplication.Quit();
                }

                ReleaseObject(excelWorkbook);
                ReleaseObject(excelApplication);
            }

            return convertExcel2PdfResult;
        }

        /// <summary>
        ///  Convert PowerPoint file to PDF by calling required method
        /// </summary>
        /// <param name="originalPptPath">file path</param>
        /// <param name="pdfPath">Target PDF path</param>
        /// <returns>error code : 0(sucess)/ -1 or errorcode (unknown error or failure)</returns>
        public static short PowerPointToPdf(object originalPptPath, object pdfPath)
        {
            short convertPowerPoint2PdfResult = -1;

            Microsoft.Office.Interop.PowerPoint.Application pptApplication = null;
            Microsoft.Office.Interop.PowerPoint.Presentation pptPresentation = null;

            object unknownType = Type.Missing;

            try
            {
                // Start PowerPoint
                pptApplication = new Microsoft.Office.Interop.PowerPoint.Application();

                // Open PowerPoint document
                pptPresentation = pptApplication.Presentations.Open(
                    (string)originalPptPath,
                    Microsoft.Office.Core.MsoTriState.msoTrue,
                    Microsoft.Office.Core.MsoTriState.msoTrue,
                    Microsoft.Office.Core.MsoTriState.msoFalse);

                // Export PDF from PPT
                if (pptPresentation != null)
                {
                    pptPresentation.ExportAsFixedFormat(
                        (string)pdfPath,
                        Microsoft.Office.Interop.PowerPoint.PpFixedFormatType.ppFixedFormatTypePDF,
                        Microsoft.Office.Interop.PowerPoint.PpFixedFormatIntent.ppFixedFormatIntentPrint,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        Microsoft.Office.Interop.PowerPoint.PpPrintHandoutOrder.ppPrintHandoutVerticalFirst,
                        Microsoft.Office.Interop.PowerPoint.PpPrintOutputType.ppPrintOutputSlides,
                        Microsoft.Office.Core.MsoTriState.msoFalse,
                        null,
                        Microsoft.Office.Interop.PowerPoint.PpPrintRangeType.ppPrintAll,
                        string.Empty,
                        true,
                        true,
                        true,
                        true,
                        false,
                        unknownType);
                    convertPowerPoint2PdfResult = 0;
                }
                else
                {
                    Console.WriteLine("Error occured for conversion of office PowerPoint to PDF");
                    convertPowerPoint2PdfResult = 504;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured for conversion of office PowerPoint to PDF, Exception: ", ex);
                convertPowerPoint2PdfResult = 504;
            }
            finally
            {
                // Close and release the Document object.
                if (pptPresentation != null)
                {
                    pptPresentation.Close();
                    ReleaseObject(pptPresentation);
                    pptPresentation = null;
                }

                // Quit Word and release the ApplicationClass object.
                pptApplication.Quit();
                ReleaseObject(pptApplication);
                pptApplication = null;
            }

            return convertPowerPoint2PdfResult;
        }

        public static void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        // -------------------------------------------------------------------------------------------------
    }
}
