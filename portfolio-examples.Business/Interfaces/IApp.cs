﻿// <copyright file="IApp.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Data;
    using Helpers.App;

    public interface IApp
    {
        object ApplicationsGrid(int drawValue, int startValue, int lengthValue, string searchValue, int orderColumnValue, string orderDirectionValue, string orderColumnName);

        object GetDropdownItemList(string fieldType);

        List<AppTypeHelper> GetAppTypeList();

        object SaveAppTypeList(List<AppTypeHelper> appTypeList);

        object GetFieldList(int typeId);

        object SaveFieldList(int typeId, Guid? guid, List<FieldHelper> fieldList);

        List<WorkflowStepHelper> GetWorkflowStepList(int typeId, Guid? guid = null);

        object SaveWorkflowStepList(int typeId, Guid? guid, List<WorkflowStepHelper> workflowStepList);

        List<TypeTemplateHelper> GetTypeTemplateList(int typeId, Guid? guid = null);

        object SaveTypeTemplateList(int typeId, Guid? guid, List<TypeTemplateHelper> typeTemplateList);

        object GetAgreementTemplateList(int agreementId);

        object SaveFieldValueList(int agreementId, List<AgreementFieldValueHelper> fieldValueList);

        Agreement CreateAgreement();
    }
}
