﻿// <copyright file="IAjaxExample.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Interfaces
{
    using Data;

    public interface IAjaxExample
    {
        Customer GetCustomer(int? id);

        string GetHighlightedTextValue(string search);

        string CleanHtmlMarkup(string text);
    }
}
