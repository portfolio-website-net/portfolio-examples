﻿// <copyright file="IAddresses.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Interfaces
{
    using System.Linq;
    using Data;
    using ViewModels;

    public interface IAddresses
    {
        IQueryable<AddressViewModel> HydrateAll(Entities optionalEntitiesContext = null);

        AddressViewModel Hydrate(int id);

        int Dehydrate(AddressViewModel viewModel);

        void Remove(int id);
    }
}
