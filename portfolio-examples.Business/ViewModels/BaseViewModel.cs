﻿// <copyright file="BaseViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.ViewModels
{
    using PortfolioExamples.Business.Helpers.Enums;

    public class BaseViewModel
    {
        public ResponseType ResponseType { get; set; }

        public string PartialView { get; set; }

        public string RedirectToAction { get; set; }
    }
}
