﻿// <copyright file="AddressCollectionViewModel.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.ViewModels
{
    using System.Collections.Generic;

    public class AddressCollectionViewModel
    {
        public int DrawValue { get; set; }

        public int StartValue { get; set; }

        public int LengthValue { get; set; }

        public string SearchValue { get; set; }

        public int OrderColumnValue { get; set; }

        public string OrderDirectionValue { get; set; }

        public string OrderColumnName { get; set; }

        public int RecordsTotal { get; set; }

        public int RecordsFiltered { get; set; }

        public IEnumerable<AddressViewModel> DataResult { get; set; }
    }
}
