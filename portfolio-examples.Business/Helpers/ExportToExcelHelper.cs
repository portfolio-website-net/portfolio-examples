﻿// <copyright file="ExportToExcelHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using ClosedXML.Excel;
    using Data;
    using LINQtoCSV;

    public class ExportToExcelHelper
    {
        public MemoryStream GenerateExcelReport()
        {
            using (var db = new Entities())
            {
                var customers = (from c in db.Customers
                                 select new CustomerView
                                 {
                                     FirstName = c.FirstName,
                                     LastName = c.LastName,
                                     EmailAddress = c.EmailAddress,
                                     Phone = c.Phone
                                 }).Distinct().OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();

                var workbook = this.ConvertObjectsToXLWorkbook(customers, typeof(CustomerView), "My Report");
                var memoryStream = new MemoryStream();
                workbook.SaveAs(memoryStream);
                return memoryStream;
            }
        }

        private XLWorkbook ConvertObjectsToXLWorkbook(IEnumerable<object> objects, System.Type type, string tableName)
        {
            var workbook = new XLWorkbook();
            var dataTable = this.ConvertObjectsToDataTable(objects, type, tableName);
            var worksheet = workbook.Worksheets.Add(dataTable);

            worksheet.Table(0).ShowAutoFilter = true;
            worksheet.Columns().AdjustToContents(1, 100);
            int recordCount = dataTable.Rows.Count;
            worksheet.Cell(recordCount + 3, 1).Value = "Record Count: " + recordCount;

            return workbook;
        }

        private DataTable ConvertObjectsToDataTable(IEnumerable<object> objects, System.Type type, string tableName)
        {
            var tableColumns = new Dictionary<int, TableColumn>();

            foreach (var propertyInfo in type.GetProperties())
            {
                object[] attr = propertyInfo.GetCustomAttributes(typeof(CsvColumnAttribute), false);

                if (attr != null && attr.Length >= 1)
                {
                    int index = (attr[0] as CsvColumnAttribute).FieldIndex;
                    string fieldName = propertyInfo.Name;
                    string columnName = !string.IsNullOrEmpty((attr[0] as CsvColumnAttribute).Name) ? (attr[0] as CsvColumnAttribute).Name : propertyInfo.Name;

                    if (index > 0 && !tableColumns.ContainsKey(index))
                    {
                        tableColumns.Add(
                            (attr[0] as CsvColumnAttribute).FieldIndex,
                            new TableColumn { FieldName = fieldName, ColumnName = columnName });
                    }
                }
            }

            var dataColumns = tableColumns.OrderBy(x => x.Key).Select(x => new DataColumn(x.Value.ColumnName, typeof(string))).ToArray();

            var dataTable = new DataTable(tableName);
            dataTable.Columns.AddRange(dataColumns);

            foreach (var obj in objects)
            {
                this.AddObjectValuesToDataTableRow(obj, tableColumns, dataTable);
            }

            return dataTable;
        }

        private void AddObjectValuesToDataTableRow(object obj, Dictionary<int, TableColumn> tableColumns, DataTable dataTable)
        {
            var fieldNames = tableColumns.OrderBy(x => x.Key).Select(x => x.Value).ToList();
            var values = new List<object>();

            foreach (var propertyInfo in obj.GetType().GetProperties().Where(x => fieldNames.Any(y => y.FieldName == x.Name)))
            {
                values.Add(propertyInfo.GetValue(obj) != null ? propertyInfo.GetValue(obj).ToString() : string.Empty);
            }

            dataTable.Rows.Add(values.ToArray());
        }

        private class TableColumn
        {
            public string FieldName { get; set; }

            public string ColumnName { get; set; }
        }

        private class CustomerView
        {
            [CsvColumn(FieldIndex = 1, Name = "First Name")]
            public string FirstName { get; set; }

            [CsvColumn(FieldIndex = 2, Name = "Last Name")]
            public string LastName { get; set; }

            [CsvColumn(FieldIndex = 3, Name = "Email")]
            public string EmailAddress { get; set; }

            [CsvColumn(FieldIndex = 4, Name = "Phone")]
            public string Phone { get; set; }
        }
    }
}