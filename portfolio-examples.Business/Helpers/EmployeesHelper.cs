﻿// <copyright file="EmployeesHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading;
    using Data;
    using HtmlAgilityPack;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;
    using OpenQA.Selenium.Support.UI;

    public class EmployeesHelper
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private bool acceptNextAlert = true;

        public void Setup()
        {
            this.driver = new ChromeDriver();
            ////this.driver.Manage().Window.Position = new System.Drawing.Point(-10000, 0);
            this.verificationErrors = new StringBuilder();
        }

        public void Teardown()
        {
            try
            {
                this.driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        public void ProcessData(List<DataUrlInfo> dataUrlInfoList, bool singleIteration = false)
        {
            while (dataUrlInfoList.Any(x => x.CurrentPageNum <= x.PageCount))
            {
                foreach (var item in dataUrlInfoList.Where(x => x.CurrentPageNum <= x.PageCount))
                {
                    this.ProcessUrlData(item);
                }

                if (singleIteration)
                {
                    break;
                }
            }
        }

        public DataUrlInfo GetDataUrlInfoForDate(DateTime date)
        {
            this.Setup();
            var dataUrlInfo = this.GetDataUrlInfo(date);
            this.Teardown();

            return dataUrlInfo;
        }

        public DataUrlInfo GetDataUrlInfo(DateTime date)
        {
            string baseUrl = string.Empty;
            string chromeCookieUrl = string.Empty;
            using (var context = new Entities())
            {
                baseUrl = context.ApplicationSettings.First(x => x.Name == "EmployeesBaseUrl").Value;
                chromeCookieUrl = context.ApplicationSettings.First(x => x.Name == "ChromeCookieUrl").Value;
            }

            this.driver.Navigate().GoToUrl(baseUrl);
            this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("MSOPageViewerWebPart_WebPartWPQ1")));
            new SelectElement(this.driver.FindElement(By.Id("Date"))).SelectByText(date.ToString("M/d/yyyy"));
            this.driver.FindElement(By.Name("LastName")).Clear();
            this.driver.FindElement(By.Name("LastName")).SendKeys("%");
            this.driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            Thread.Sleep(20000);

            int pageCount = 0;

            bool isError = false;
            int failureCount = 0;
            int maxFailureCount = 20;
            do
            {
                isError = false;
                pageCount = 0;

                try
                {
                    Thread.Sleep(1000);
                    this.driver.SwitchTo().Frame(this.driver.FindElement(By.Name("if1")));

                    this.WaitForElement(By.Id("openDocChildFrame"));
                    this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("openDocChildFrame")));

                    this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("webiViewFrame")));

                    this.WaitForElement(By.Id("IconImg__dhtmlLib_199"));
                    this.driver.FindElement(By.Id("IconImg__dhtmlLib_199")).Click();

                    Thread.Sleep(2000);

                    this.WaitForElement(By.Id("IconImg__dhtmlLib_197"));
                    this.driver.FindElement(By.Id("IconImg__dhtmlLib_197")).Click();

                    Thread.Sleep(2000);

                    pageCount = int.Parse(this.driver.FindElement(By.Id("IconImg_Txt__dhtmlLib_194")).Text.Replace("Page ", string.Empty).Split(' ')[0]);
                }
                catch
                {
                    isError = true;
                    failureCount++;
                }

                if (isError || pageCount == 1)
                {
                    this.driver.SwitchTo().DefaultContent();
                    this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("MSOPageViewerWebPart_WebPartWPQ1")));
                }

                if (failureCount >= maxFailureCount)
                {
                    return null;
                }
            }
            while (isError);

            if (pageCount <= 1)
            {
                return null;
            }

            this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("Report")));
            var pageUrl = this.driver.FindElement(By.Id("idReportbloc")).GetAttribute("src");
            this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id("idReportbloc")));
            string recordsHtml = this.driver.PageSource;

            var process = new System.Diagnostics.Process();
            process.StartInfo.FileName = @"C:\Temp\GetCookie.exe";
            process.StartInfo.Arguments = chromeCookieUrl;
            process.Start();
            process.WaitForExit();

            string cookieValue = File.ReadLines(@"C:\Temp\Cookie.txt").First();

            int currentPageNum = 0;

            using (var context = new Entities())
            {
                var pageNumItems = context.Employees.Where(x => x.Date == date);
                if (pageNumItems.Any())
                {
                    currentPageNum = pageNumItems.Max(x => x.Page) + 1;
                }

                var employeePages = context.EmployeePages.FirstOrDefault(x => x.Date == date);
                if (employeePages == null)
                {
                    context.EmployeePages.Add(new EmployeePage
                    {
                        Date = date,
                        PageCount = pageCount,
                        CreatedById = 0,
                        CreatedDate = DateTime.Now
                    });

                    context.SaveChanges();
                }
            }

            return new DataUrlInfo
            {
                Date = date,
                PageUrl = pageUrl,
                PageCount = pageCount,
                CookieValue = cookieValue,
                CurrentPageNum = currentPageNum
            };
        }

        public void ProcessUrlData(DataUrlInfo dataUrlInfo)
        {
            var client = new WebClient();
            client.Headers.Add(HttpRequestHeader.Cookie, "JSESSIONID=" + dataUrlInfo.CookieValue);

            if (dataUrlInfo.CurrentPageNum == 0)
            {
                dataUrlInfo.CurrentPageNum = 1;
            }

            if (dataUrlInfo.CurrentPageNum > dataUrlInfo.PageCount)
            {
                return;
            }

            using (var context = new Entities())
            {
                string currentPageUrl = dataUrlInfo.PageUrl.Replace("Page=1", "Page=" + dataUrlInfo.CurrentPageNum);
                var employeeDataHtml = client.DownloadString(currentPageUrl);

                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(employeeDataHtml);

                var nodes = htmlDoc.DocumentNode.Descendants("span")
                    .Where(d => d.Attributes.Contains("class")
                            && d.Attributes["class"].Value.Contains("nwt")
                            && d.InnerText.Contains("Last Name"));

                var foundEmployee = false;

                foreach (var node in nodes)
                {
                    var departmentTable = node.Ancestors("table").First().PreviousSibling;
                    string departmentName = departmentTable.InnerText;
                    var department = context.Departments.FirstOrDefault(x => x.Name == departmentName);

                    if (department == null)
                    {
                        department = new Department
                        {
                            Name = departmentName,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        };
                    }

                    var tableRows = node.Ancestors("table").First().Descendants("tr");

                    int rowCount = 0;

                    foreach (var tr in tableRows)
                    {
                        var employee = new Employee
                        {
                            Department = department,
                            Date = dataUrlInfo.Date,
                            Page = dataUrlInfo.CurrentPageNum,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        };

                        int index = 0;

                        foreach (var td in tr.Descendants("td"))
                        {
                            if (rowCount == 0)
                            {
                                continue;
                            }

                            if (index == 0)
                            {
                                employee.LastName = td.InnerText;
                            }
                            else if (index == 1)
                            {
                                employee.FirstName = td.InnerText;
                            }
                            else if (index == 2)
                            {
                                employee.Position = td.InnerText;
                            }
                            else if (index == 3)
                            {
                                if (td.InnerText.StartsWith("$"))
                                {
                                    employee.Salary = decimal.Parse(td.InnerText.Replace("$", string.Empty).Replace(",", string.Empty));
                                }
                            }
                            else if (index == 4)
                            {
                                if (td.InnerText.Contains(" / Hour"))
                                {
                                    employee.Wage = decimal.Parse(td.InnerText.Replace(" / Hour", string.Empty).Replace("$", string.Empty).Replace(",", string.Empty));
                                }
                            }

                            index++;
                        }

                        if (rowCount > 0
                            && !string.IsNullOrEmpty(employee.LastName)
                            && !string.IsNullOrEmpty(employee.Position))
                        {
                            foundEmployee = true;
                            context.Employees.Add(employee);
                        }

                        rowCount++;
                    }
                }

                try
                {
                    if (foundEmployee)
                    {
                        context.SaveChanges();
                    }
                    else
                    {
                        dataUrlInfo.CurrentPageNum = dataUrlInfo.PageCount;
                    }
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                        }
                    }
                }

                dataUrlInfo.CurrentPageNum++;
            }
        }

        private void WaitForElement(By by)
        {
            var waitTime = 100;
            var currentWaitTime = 0;
            var totalWaitTime = 10000;
            while (!this.IsElementPresent(by))
            {
                Thread.Sleep(waitTime);
                currentWaitTime += waitTime;

                if (currentWaitTime >= totalWaitTime)
                {
                    break;
                }
            }
        }

        private bool IsElementPresent(By by)
        {
            try
            {
                this.driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                this.driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                var alert = this.driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (this.acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }

                return alertText;
            }
            finally
            {
                this.acceptNextAlert = true;
            }
        }

        public class DataUrlInfo
        {
            public DateTime Date { get; set; }

            public string PageUrl { get; set; }

            public int PageCount { get; set; }

            public string CookieValue { get; set; }

            public int CurrentPageNum { get; set; }
        }
    }
}