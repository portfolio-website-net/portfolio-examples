﻿// <copyright file="PlaylistHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Script.Serialization;
    using Data;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Firefox;
    using OpenQA.Selenium.Support.UI;

    public class PlaylistHelper
    {
        private IWebDriver driver;

        public void Setup()
        {
            this.driver = new FirefoxDriver();
            this.driver.Manage().Window.Position = new System.Drawing.Point(-10000, 0);
        }

        public void Teardown()
        {
            try
            {
                this.driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to quit
            }
        }

        public void GetAndSavePlaylistItemInfoList()
        {
            var stationIdList = new List<int>();
            using (var context = new Entities())
            {
                stationIdList = context.Stations.Select(x => x.StationId).ToList();
            }

            this.Setup();

            foreach (var stationId in stationIdList)
            {
                try
                {
                    var playlistItemInfoList = this.GetPlaylistItemInfoListData(stationId);

                    using (var context = new Entities())
                    {
                        foreach (var item in playlistItemInfoList)
                        {
                            if (!context.PlaylistItems.Any(x =>
                                x.Time == item.TimeValue.Value
                                && x.Artist == item.Artist
                                && x.Song == item.Song))
                            {
                                context.PlaylistItems.Add(new PlaylistItem
                                {
                                    StationId = stationId,
                                    Time = item.TimeValue.Value,
                                    Artist = item.Artist.Trim(),
                                    Song = item.Song.Trim(),
                                    CreatedDate = DateTime.Now
                                });
                            }
                        }

                        context.SaveChanges();
                    }
                }
                catch
                {
                    // Ignore errors
                }
            }

            this.Teardown();
        }

        public List<PlaylistItemInfo> GetPlaylistItemInfoListData(int stationId)
        {
            var playlistItemInfoList = new List<PlaylistItemInfo>();
            string url = string.Empty;
            string frameId = string.Empty;
            string script = string.Empty;
            using (var context = new Entities())
            {
                var station = context.Stations.FirstOrDefault(x => x.StationId == stationId);

                if (station != null)
                {
                    url = station.Url;
                    frameId = station.FrameId;
                    script = station.Script;
                }
            }

            if (!string.IsNullOrEmpty(url))
            {
                this.driver.Navigate().GoToUrl(url);

                if (!string.IsNullOrEmpty(frameId))
                {
                    this.WaitForElement(By.Id(frameId));
                    this.driver.SwitchTo().Frame(this.driver.FindElement(By.Id(frameId)));
                }

                var js = (IJavaScriptExecutor)this.driver;
                var result = (string)js.ExecuteScript(script);

                playlistItemInfoList = new JavaScriptSerializer()
                    .Deserialize<List<PlaylistItemInfo>>(result);

                playlistItemInfoList.ForEach(x =>
                {
                    x.TimeValue = this.GetTimeValue(x.Time);
                    x.Artist = this.TrimToMaxLength(x.Artist.Trim(), 200);
                    x.Song = this.TrimToMaxLength(x.Song.Trim(), 200);
                });

                return playlistItemInfoList
                    .Where(x => x.TimeValue != null && x.Artist != null && x.Song != null)
                    .OrderBy(x => x.TimeValue)
                    .ToList();
            }
            else
            {
                return playlistItemInfoList;
            }
        }

        private DateTime? GetTimeValue(string time)
        {
            DateTime? dateTimeResult = null;
            DateTime dateTimeOut;

            if (!string.IsNullOrEmpty(time) && DateTime.TryParse(time, out dateTimeOut))
            {
                if (DateTime.TryParse(DateTime.Now.Date.ToShortDateString() + " " + time.Trim(), out dateTimeOut))
                {
                    if (dateTimeOut > DateTime.Now)
                    {
                        dateTimeOut = dateTimeOut.AddDays(-1);
                    }

                    dateTimeResult = dateTimeOut;
                }
            }

            return dateTimeResult;
        }

        private string TrimToMaxLength(string value, int length)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (value.Length > length)
                {
                    value = value.Substring(0, length);
                }
            }
            else
            {
                value = null;
            }

            return value;
        }

        private void WaitForElement(By by)
        {
            var wait = new WebDriverWait(this.driver, TimeSpan.FromSeconds(10));
            wait.Until(driver => driver.FindElement(by).Displayed);
        }

        public class PlaylistItemInfo
        {
            public string Time { get; set; }

            public DateTime? TimeValue { get; set; }

            public string Song { get; set; }

            public string Artist { get; set; }
        }
    }
}