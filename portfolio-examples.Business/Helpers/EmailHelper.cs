﻿// <copyright file="EmailHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;

    public class EmailHelper
    {
        public static void SendEmail()
        {
            var message = GetMailMessage();
            var cred = new NetworkCredential("user@domain.com", "password");
            var smtp = new SmtpClient("smtp.domain.com", 587);
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = cred;

            smtp.Send(message);
        }

        // Reference URL: https://stackoverflow.com/questions/18358534/send-inline-image-in-email
        private static MailMessage GetMailMessage()
        {
            var mail = new MailMessage();
            mail.IsBodyHtml = true;
            mail.AlternateViews.Add(GetEmbeddedImage("c:/image.jpg"));
            mail.From = new MailAddress("user@domain.com");
            mail.To.Add("user@domain.com");
            mail.Subject = "Test Message";
            return mail;
        }

        private static AlternateView GetEmbeddedImage(string filePath)
        {
            var resource = new LinkedResource(filePath, MediaTypeNames.Image.Jpeg);
            resource.ContentId = Guid.NewGuid().ToString();
            string htmlBody = @"<img src='cid:" + resource.ContentId + @"'/>";
            var alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(resource);
            return alternateView;
        }
    }
}