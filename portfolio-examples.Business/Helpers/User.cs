﻿// <copyright file="User.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.Collections.Generic;

    public class User
    {
        public string UserName { get; set; }

        public List<ModulePermission> ModulePermissions { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}