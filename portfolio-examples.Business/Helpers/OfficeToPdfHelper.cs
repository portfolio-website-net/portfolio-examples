﻿// <copyright file="OfficeToPdfHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using EO.Pdf;

    public static class OfficeToPdfHelper
    {
        private const int MaxWaitSeconds = 30;

        public static Stream ConvertDocuments(List<string> fileNameList)
        {
            var tasks = new List<Task<PdfDocument>>();

            foreach (var fileName in fileNameList)
            {
                tasks.Add(Task.Run(() => GetFileAsPdf(fileName)));
            }

            var pdfDocuments = new List<PdfDocument>();
            var results = Task.WhenAll(tasks).Result;

            foreach (var result in results)
            {
                if (result != null)
                {
                    pdfDocuments.Add(result);
                }
            }

            MemoryStream stream = null;

            if (pdfDocuments.Count > 0)
            {
                var mergedDoc = PdfDocument.Merge(pdfDocuments.ToArray());

                string styleStr = @"<style>
                    * {
                        font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
                        font-size: 14px;
                        line-height: 1.25;
                    }
                    .pagefooterleft {
                        float: left;
                        text-align: left;
                    }
                    .pagefooterright {
                        float: right;
                        text-align: right;                                                                        
                    }
                    .pagefooter {
                        border-top: 1px solid lightgray;
                        margin-top: 20px;
                        font-size: 20px;
                    }
                    </style>";

                var options = new HtmlToPdfOptions();
                options.PageSize = new SizeF(8.5f, 11f);
                options.OutputArea = new RectangleF(0.5f, 10.3f, 7.5f, 1.0f);

                using (var session = HtmlToPdfSession.Create(options))
                {
                    for (int i = 0; i < mergedDoc.Pages.Count; i++)
                    {
                        string footerHtml = styleStr + "<div class='pagefooter'><div class='pagefooterright'>Page " + (i + 1) + " of " + mergedDoc.Pages.Count + "</div></div>";
                        session.LoadHtml(footerHtml);
                        session.RenderAsPDF(mergedDoc.Pages[i]);
                    }
                }

                stream = new MemoryStream();
                mergedDoc.Save(stream);
            }

            return stream;
        }

        private static async Task<PdfDocument> GetFileAsPdf(string fileName)
        {
            PdfDocument result = null;
            string filePath = Path.GetDirectoryName(fileName);
            string tempFilename = "Temp_" + Guid.NewGuid().ToString() + ".pdf";
            string tempPath = Path.Combine(filePath, tempFilename);

            var officeDocumentTypes = new string[] { ".doc", ".docx", ".rtf", ".xls", ".xlsx", ".ppt", ".pptx" };
            var imageDocumentTypes = new string[] { ".jpg", ".jpeg", ".png", ".gif", ".tiff", ".tif", ".bmp" };
            var plainTextDocumentTypes = new string[] { ".txt" };
            var pdfDocumentTypes = new string[] { ".pdf" };

            var fileExtension = Path.GetExtension(fileName).ToLower();

            try
            {
                if (officeDocumentTypes.Contains(fileExtension))
                {
                    var data = await OfficeToPdfFromFileName(fileName);
                    result = new PdfDocument(new MemoryStream(data));
                }
                else if (imageDocumentTypes.Contains(fileExtension))
                {
                    var imageDoc = new PdfDocument();
                    var imgOptions = new HtmlToPdfOptions();
                    imgOptions.PageSize = new SizeF(8.5f, 11f);
                    imgOptions.OutputArea = new RectangleF(0.5f, 0.5f, 7.5f, 10.0f);

                    using (HtmlToPdfSession session = HtmlToPdfSession.Create(imgOptions))
                    {
                        session.LoadHtml(ConvertImageToHtmlTag(filePath, fileName));
                        session.RenderAsPDF(imageDoc);
                    }

                    result = imageDoc;
                }
                else if (plainTextDocumentTypes.Contains(fileExtension))
                {
                    string styleStr = @"<style>
                            * {
                                font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
                                font-size: 14px;
                            }
                            .forcebreak {
                                word-break: break-word;
                            }
                            </style>";

                    var txtDoc = new PdfDocument();
                    var txtOptions = new HtmlToPdfOptions();
                    txtOptions.PageSize = new SizeF(8.5f, 11f);
                    txtOptions.OutputArea = new RectangleF(0.5f, 0.5f, 7.5f, 10.0f);

                    using (var session = HtmlToPdfSession.Create(txtOptions))
                    {
                        session.LoadHtml(styleStr + ConvertTextToHtml(fileName));
                        session.RenderAsPDF(txtDoc);
                    }

                    result = txtDoc;
                }
                else if (pdfDocumentTypes.Contains(fileExtension))
                {
                    File.Copy(fileName, tempPath);
                }
            }
            catch
            {
                // Ignore errors
            }

            try
            {
                if (File.Exists(tempPath))
                {
                    result = new PdfDocument(tempPath);
                    File.Delete(tempPath);
                }
            }
            finally
            {
                if (File.Exists(tempPath))
                {
                    File.Delete(tempPath);
                }
            }

            return result;
        }

        private static string ConvertImageToHtmlTag(string filePath, string inputFilename)
        {
            var image = Image.FromFile(inputFilename);
            string tempFile = Path.Combine(filePath, "Temp_" + Guid.NewGuid().ToString() + ".png");
            image.Save(tempFile, ImageFormat.Png);

            byte[] fileBytes;
            try
            {
                using (var fs = new FileStream(tempFile, FileMode.Open, FileAccess.Read))
                {
                    fileBytes = new byte[fs.Length];
                    fs.Read(fileBytes, 0, Convert.ToInt32(fs.Length));
                }
            }
            finally
            {
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                }
            }

            return "<img src=\"data:image/png;base64," + Convert.ToBase64String(fileBytes, Base64FormattingOptions.None) + "\" />";
        }

        private static string ConvertTextToHtml(string inputFilename)
        {
            return "<pre class='forcebreak'>" + File.ReadAllText(inputFilename) + "</pre>";
        }

        private static async Task<byte[]> OfficeToPdfFromFileName(string fileName)
        {
            byte[] outputFileBytes = null;

            string conversionBasePath = ConfigurationManager.AppSettings["FileConvertPath"];

            string filenameGuid = Guid.NewGuid().ToString();
            string inputFilename = Path.Combine(conversionBasePath, filenameGuid + Path.GetExtension(fileName));
            string outputFilename = Path.Combine(conversionBasePath, filenameGuid + ".pdf");
            string stubFilename = Path.Combine(ConfigurationManager.AppSettings["FileStubPath"], Path.GetFileName(inputFilename));

            File.Copy(fileName, inputFilename);

            File.WriteAllText(stubFilename, inputFilename);

            try
            {
                outputFileBytes = await TimeoutAfter(OfficeToPdfTask(stubFilename, inputFilename, outputFilename), TimeSpan.FromSeconds(30));
            }
            finally
            {
                FileCleanup(stubFilename, inputFilename, outputFilename);
            }

            return outputFileBytes;
        }

        private static void FileCleanup(string stubFilename, string inputFilename, string outputFilename)
        {
            if (File.Exists(stubFilename))
            {
                File.Delete(stubFilename);
            }

            if (File.Exists(inputFilename))
            {
                File.Delete(inputFilename);
            }

            if (File.Exists(outputFilename))
            {
                File.Delete(outputFilename);
            }
        }

        private static Task<byte[]> OfficeToPdfTask(string stubFilename, string inputFilename, string outputFilename)
        {
            byte[] outputFileBytes = null;

            var s = new Stopwatch();
            s.Start();
            while (File.Exists(inputFilename))
            {
                Thread.Sleep(100);
                if (s.ElapsedMilliseconds >= MaxWaitSeconds * 1000)
                {
                    break;
                }
            }

            if (File.Exists(outputFilename))
            {
                using (var fs = new FileStream(outputFilename, FileMode.Open, FileAccess.Read))
                {
                    outputFileBytes = new byte[fs.Length];
                    fs.Read(outputFileBytes, 0, Convert.ToInt32(fs.Length));
                }
            }

            return Task.FromResult(outputFileBytes);
        }

        // Source URL: https://stackoverflow.com/questions/4238345/asynchronously-wait-for-taskt-to-complete-with-timeout
        private static async Task<TResult> TimeoutAfter<TResult>(this Task<TResult> task, TimeSpan timeout)
        {
            using (var timeoutCancellationTokenSource = new CancellationTokenSource())
            {
                var completedTask = await Task.WhenAny(task, Task.Delay(timeout, timeoutCancellationTokenSource.Token));
                if (completedTask == task)
                {
                    timeoutCancellationTokenSource.Cancel();
                    return await task;  // Very important in order to propagate exceptions
                }
                else
                {
                    throw new TimeoutException("The operation has timed out.");
                }
            }
        }
    }
}