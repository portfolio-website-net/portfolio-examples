﻿// <copyright file="SmmryJsonHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "JSON API")]
    public class SmmryJsonHelper
    {
        public string url { get; set; }

        public string sm_api_message { get; set; }

        public string sm_api_character_count { get; set; }

        public string sm_api_content_reduced { get; set; }

        public string sm_api_title { get; set; }

        public string sm_api_content { get; set; }

        public string sm_api_limitation { get; set; }
    }
}