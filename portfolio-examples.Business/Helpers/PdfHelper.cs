﻿// <copyright file="PdfHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Web.Mvc;
    using EO.Pdf;

    public class PdfHelper
    {
        public static PdfDocument GenerateReport(string text, Controller controller)
        {
            controller.ViewBag.Text = text;
            string parsedView = RenderRazorViewToString(controller, @"~/Views/Reports/MyReport.cshtml", null);

            var doc = new PdfDocument();
            doc.Info.Title = "My Report";
            doc.Info.CreationDate = DateTime.Now;
            doc.Info.Author = "Your Name";
            var options = new HtmlToPdfOptions();
            options.PageSize = new SizeF(8.5f, 11f);
            options.OutputArea = new RectangleF(0.35f, 0.2f, 7.5f, 10.0f);
            options.ZoomLevel = 1.00f;
            string styleStr = @"<style>
                    * {
                        font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
                        font-size: 14px;
                        line-height: 1.25;
                    }
                    .pagefooter {
                        width: 100%;
                        text-align: center;
                        font-size: 20px;
                    }
                    </style>";
            options.FooterHtmlFormat = styleStr + "<div class='pagefooter'>{page_number}</div>";

            using (var session = HtmlToPdfSession.Create(options))
            {
                session.Options.TriggerMode = HtmlToPdfTriggerMode.Auto;
                session.LoadHtml(parsedView);
                session.RenderAsPDF(doc);
            }

            return doc;
        }

        public static string RenderRazorViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}