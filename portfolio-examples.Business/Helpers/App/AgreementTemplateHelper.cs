﻿// <copyright file="AgreementTemplateHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers.App
{
    using System;

    [Serializable]
    public class AgreementTemplateHelper
    {
        public int AgreementTemplateId { get; set; }

        public int AgreementId { get; set; }

        public int TypeTemplateId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Content { get; set; }

        public string TypeTemplateTypeCode { get; set; }

        public bool RenderAsFormInd { get; set; }

        public int Order { get; set; }

        public Guid? Guid { get; set; }
    }
}