﻿// <copyright file="FieldHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers.App
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class FieldHelper
    {
        public int FieldId { get; set; }

        public int? ParentFieldId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string FieldTypeCode { get; set; }

        public bool FieldMultiplicityInd { get; set; }

        public int? MaxLength { get; set; }

        public bool RequiredInd { get; set; }

        public int Order { get; set; }

        public string FieldRenderingTypeCode { get; set; }

        public IEnumerable<FieldChildHelper> Children { get; set; }

        public IEnumerable<ListItemHelper> ListItems { get; set; }

        public Guid? Guid { get; set; }
    }
}