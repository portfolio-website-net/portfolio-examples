﻿// <copyright file="ListItemHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers.App
{
    using System;

    [Serializable]
    public class ListItemHelper
    {
        public int ListItemId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int Order { get; set; }

        public Guid? Guid { get; set; }
    }
}