﻿// <copyright file="AgreementFieldValueHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers.App
{
    using System;

    [Serializable]
    public class AgreementFieldValueHelper
    {
        public int AgreementFieldValueId { get; set; }

        public int FieldId { get; set; }

        public string Value { get; set; }

        public Guid Guid { get; set; }
    }
}