﻿// <copyright file="DocumentHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Wordprocessing;

    public class DocumentHelper
    {
        public static void FillTemplate(WordprocessingDocument doc, string text)
        {
            ReplaceTemplatePlaceholder(doc, "TestField", text);
        }

        // Code Reference: https://stackoverflow.com/questions/17919651/word-openxml-replace-token-text?rq=1
        public static void ReplaceTemplatePlaceholder(WordprocessingDocument doc, string searchValue, string replaceValue)
        {
            if (replaceValue == null)
            {
                replaceValue = string.Empty;
            }

            searchValue = "[" + searchValue + "]";

            var items = doc.MainDocumentPart.Document.Body.Descendants<Text>();

            var fields = new List<Text>();
            bool isItemFound = false;

            foreach (var item in items)
            {
                if (item.Text.StartsWith("["))
                {
                    fields.Clear();
                    fields.Add(item);
                    isItemFound = false;
                }
                else if (fields.Count() > 0)
                {
                    fields.Add(item);
                }

                if (item.Text.EndsWith("]"))
                {
                    if (!fields.Contains(item))
                    {
                        fields.Add(item);
                    }

                    isItemFound = true;
                }

                if (isItemFound && IsSearchValueFound(fields, searchValue))
                {
                    isItemFound = false;
                    var parent = item.Parent;
                    var newToken = item.CloneNode(true);
                    var lines = Regex.Split(replaceValue, "\r\n|\r|\n");

                    ((Text)newToken).Text = lines[0];

                    for (int i = 1; i < lines.Length; i++)
                    {
                        parent.AppendChild(new Break());
                        parent.AppendChild(new Text(lines[i]));
                    }

                    item.InsertAfterSelf(newToken);

                    foreach (var fieldItem in fields)
                    {
                        fieldItem.Remove();
                    }
                }
            }

            doc.MainDocumentPart.Document.Save();
        }

        private static bool IsSearchValueFound(List<Text> fieldItems, string searchText)
        {
            var buffer = new StringBuilder();

            foreach (var item in fieldItems)
            {
                buffer.Append(item.Text);
            }

            return buffer.ToString().Trim() == searchText;
        }
    }
}