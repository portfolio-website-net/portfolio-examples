﻿// <copyright file="SearchHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    public class SearchHelper
    {
        public static string GetHighlightedTextValue(string[] keywords, string textValue)
        {
            // Filter and sort the list of keywords
            var filteredKeywordList = GetFilteredKeywordList(keywords);

            // Look for each keyword within textValue and grab its surrounding text
            var keywordResults = GetKeywordResults(filteredKeywordList, textValue);

            // Refine the results by only keeping the results that do not overlap each other
            var keywordRefinedResults = GetNonOverlappingResults(filteredKeywordList, keywordResults);

            // Concatenate the refined results
            var highlightedTextResult = ConcatenateResults(filteredKeywordList, keywordRefinedResults);

            // Wrap each keyword with the characters '~' in front and '*' behind
            highlightedTextResult = filteredKeywordList.Select(word => word.Trim()).OrderByDescending(word => word.Length).Aggregate(
                highlightedTextResult,
                (current, pattern) =>
                Regex.Replace(current, Regex.Escape(pattern), string.Format("~{0}*", "$0"), RegexOptions.IgnoreCase));

            // Replace the single characters with HTML tags
            highlightedTextResult = Regex.Replace(highlightedTextResult, "\\~", "<strong>", RegexOptions.IgnoreCase);
            highlightedTextResult = Regex.Replace(highlightedTextResult, "\\*", "</strong>", RegexOptions.IgnoreCase);

            return highlightedTextResult;
        }

        private static List<string> GetFilteredKeywordList(string[] keywords)
        {
            return keywords.Where(word => word != null && word.Length > 1)
                .OrderByDescending(word => word.Length).ToList();
        }

        private static List<KeywordResult> GetKeywordResults(List<string> keywordList, string textValue)
        {
            var keywordResults = new List<KeywordResult>();
            int characterPadding = 30;

            foreach (string keyword in keywordList)
            {
                int keywordPos = 0;
                while (true)
                {
                    keywordPos = textValue.IndexOf(keyword, keywordPos, StringComparison.InvariantCultureIgnoreCase);

                    if (keywordPos == -1)
                    {
                        break;
                    }

                    string tempTextValue = textValue;

                    int startPos = Math.Min(textValue.IndexOf(" ", Math.Max(0, keywordPos - characterPadding), StringComparison.InvariantCultureIgnoreCase), keywordPos);
                    if (startPos == -1 || startPos == keywordPos)
                    {
                        startPos = Math.Max(0, keywordPos - characterPadding);
                    }

                    int endPos = Math.Min(textValue.LastIndexOf(" ", Math.Min(textValue.Length, keywordPos + characterPadding), StringComparison.InvariantCultureIgnoreCase), keywordPos + characterPadding);
                    if (endPos == -1 || endPos <= keywordPos + keyword.Length)
                    {
                        endPos = Math.Min(textValue.Length, keywordPos + characterPadding);
                    }

                    string paddedValue = textValue.Substring(startPos, endPos - startPos).Trim();

                    if (endPos < textValue.Length)
                    {
                        paddedValue += "... ";
                    }

                    keywordResults.Add(new KeywordResult
                    {
                        Result = paddedValue,
                        StartPos = startPos,
                        EndPos = endPos
                    });

                    keywordPos++;
                }
            }

            return keywordResults;
        }

        private static List<KeywordResult> GetNonOverlappingResults(List<string> filteredKeywordList, List<KeywordResult> keywordResults)
        {
            var keywordRefinedResults = new List<KeywordResult>();

            foreach (string keyword in filteredKeywordList)
            {
                bool addedEntry = false;

                foreach (var keywordResult in keywordResults.Where(x => x.Result.ToLower().Contains(keyword)).OrderByDescending(x => x.Result.Length))
                {
                    bool hasOverlap = false;

                    foreach (var refinedResult in keywordRefinedResults)
                    {
                        if (HasOverlapInKeywordResult(keywordResult.StartPos, keywordResult.EndPos, refinedResult.StartPos, refinedResult.EndPos))
                        {
                            hasOverlap = true;
                            break;
                        }
                    }

                    if (!hasOverlap)
                    {
                        keywordRefinedResults.Add(new KeywordResult
                        {
                            Result = keywordResult.Result,
                            StartPos = keywordResult.StartPos,
                            EndPos = keywordResult.EndPos
                        });

                        addedEntry = true;
                    }
                }

                // Add the result if it was missed during the overlap elimination process
                if (!addedEntry && keywordRefinedResults.Where(x => x.Result.ToLower().Contains(keyword)).Count() == 0)
                {
                    var keywordResult = keywordResults.Where(x => x.Result.ToLower().Contains(keyword)).OrderByDescending(x => x.Result.Length).FirstOrDefault();

                    if (keywordResult != null)
                    {
                        keywordRefinedResults.Add(new KeywordResult
                        {
                            Result = keywordResult.Result,
                            StartPos = keywordResult.StartPos,
                            EndPos = keywordResult.EndPos
                        });
                    }
                }
            }

            return keywordRefinedResults;
        }

        private static string ConcatenateResults(List<string> filteredKeywordList, List<KeywordResult> keywordRefinedResults)
        {
            var highlightedTextBuilder = new StringBuilder();
            foreach (string keyword in filteredKeywordList)
            {
                if (!highlightedTextBuilder.ToString().ToLower().Contains(keyword))
                {
                    foreach (var refinedResult in keywordRefinedResults
                        .Where(x => x.Result.ToLower().Contains(keyword))
                        .OrderByDescending(x => x.Result.Length).Take(3))
                    {
                        highlightedTextBuilder.Append(refinedResult.Result);
                    }
                }
            }

            return highlightedTextBuilder.ToString();
        }

        private static bool HasOverlapInKeywordResult(int startPos1, int endPos1, int startPos2, int endPos2)
        {
            return !(endPos1 < startPos2 || endPos2 < startPos1);
        }

        private class KeywordResult
        {
            public string Result { get; set; }

            public int StartPos { get; set; }

            public int EndPos { get; set; }
        }
    }
}