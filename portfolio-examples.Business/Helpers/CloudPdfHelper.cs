﻿// <copyright file="CloudPdfHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.IO;
    using System.Web.Mvc;

    public class CloudPdfHelper
    {
        public static string GenerateReport(string text, Controller controller)
        {
            controller.ViewBag.Text = text;
            string parsedView = RenderRazorViewToString(controller, @"~/Views/Reports/MyCloudReport.cshtml", null);

            return parsedView;
        }

        public static string RenderRazorViewToString(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}