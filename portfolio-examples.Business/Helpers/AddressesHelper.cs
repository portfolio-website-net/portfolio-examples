﻿// <copyright file="AddressesHelper.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Helpers
{
    using System.Linq;
    using System.Linq.Dynamic;
    using Data;
    using Interfaces;
    using Providers;
    using ViewModels;

    public class AddressesHelper
    {
        public static AddressCollectionViewModel GetGridResults(AddressCollectionViewModel viewModel, IAddresses provider)
        {
            Entities entities = null;
            if (provider is SQLServerAddresses)
            {
                entities = new Entities();
            }

            try
            {
                var searchValues = viewModel.SearchValue.Split(' ');

                IQueryable<AddressViewModel> dataAll;

                if (entities != null)
                {
                    dataAll = provider.HydrateAll(entities);
                }
                else
                {
                    dataAll = provider.HydrateAll();
                }

                var dataFiltered = dataAll;

                foreach (var value in searchValues)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        var searchVal = value.ToLower();

                        dataFiltered = dataFiltered.Where(x =>
                            x.AddressLine1.ToLower().Contains(searchVal)
                            || x.AddressLine2.ToLower().Contains(searchVal)
                            || x.City.ToLower().Contains(searchVal)
                            || x.StateProvince.ToLower().Contains(searchVal)
                            || x.CountryRegion.ToLower().Contains(searchVal)
                            || x.PostalCode.ToLower().Contains(searchVal));
                    }
                }

                var dataOrdered = dataFiltered.OrderBy(viewModel.OrderColumnName + " " + viewModel.OrderDirectionValue);
                var dataResult = dataOrdered.Skip(viewModel.StartValue).Take(viewModel.LengthValue).ToList();

                var addressCollectionViewModel = new AddressCollectionViewModel
                {
                    DrawValue = viewModel.DrawValue,
                    RecordsTotal = dataAll.Count(),
                    RecordsFiltered = dataFiltered.Count(),
                    DataResult = dataResult
                };

                return addressCollectionViewModel;
            }
            finally
            {
                if (entities != null)
                {
                    entities.Dispose();
                }
            }
        }
    }
}