﻿// <copyright file="SQLServerAddresses.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Dynamic;
    using Data;
    using Interfaces;
    using ViewModels;

    public class SQLServerAddresses : IAddresses
    {
        public IQueryable<AddressViewModel> HydrateAll(Entities optionalEntitiesContext = null)
        {
            if (optionalEntitiesContext != null)
            {
                return optionalEntitiesContext.Addresses
                    .Select(x => new AddressViewModel
                    {
                        AddressID = x.AddressID,
                        AddressLine1 = x.AddressLine1,
                        AddressLine2 = x.AddressLine2,
                        City = x.City,
                        StateProvince = x.StateProvince,
                        CountryRegion = x.CountryRegion,
                        PostalCode = x.PostalCode
                    });
            }
            else
            {
                return new List<AddressViewModel>().AsQueryable();
            }
        }

        public AddressViewModel Hydrate(int id)
        {
            var viewModel = new AddressViewModel();

            using (var entities = new Entities())
            {
                var address = entities.Addresses.FirstOrDefault(x => x.AddressID == id);
                if (address != null)
                {
                    viewModel.AddressID = address.AddressID;
                    viewModel.AddressLine1 = address.AddressLine1;
                    viewModel.AddressLine2 = address.AddressLine2;
                    viewModel.City = address.City;
                    viewModel.StateProvince = address.StateProvince;
                    viewModel.CountryRegion = address.CountryRegion;
                    viewModel.PostalCode = address.PostalCode;
                }

                return viewModel;
            }
        }

        public int Dehydrate(AddressViewModel viewModel)
        {
            using (var entities = new Entities())
            {
                var address = entities.Addresses.FirstOrDefault(x => x.AddressID == viewModel.AddressID);
                if (address == null)
                {
                    address = new Address();
                }

                address.AddressLine1 = viewModel.AddressLine1;
                address.AddressLine2 = viewModel.AddressLine2;
                address.City = viewModel.City;
                address.StateProvince = viewModel.StateProvince;
                address.CountryRegion = viewModel.CountryRegion;
                address.PostalCode = viewModel.PostalCode;

                if (address.AddressID == 0)
                {
                    address.rowguid = Guid.NewGuid();
                    entities.Addresses.Add(address);
                }

                address.ModifiedDate = DateTime.Now;

                entities.SaveChanges();

                return address.AddressID;
            }
        }

        public void Remove(int id)
        {
            using (var entities = new Entities())
            {
                var address = entities.Addresses.FirstOrDefault(x => x.AddressID == id);

                if (address != null)
                {
                    entities.Addresses.Remove(address);
                    entities.SaveChanges();
                }
            }
        }
    }
}
