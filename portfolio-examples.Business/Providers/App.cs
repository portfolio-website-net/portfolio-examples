﻿// <copyright file="App.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Dynamic;
    using Data;
    using Helpers.App;
    using Interfaces;
    using Newtonsoft.Json;

    public class App : IApp
    {
        public object ApplicationsGrid(int drawValue, int startValue, int lengthValue, string searchValue, int orderColumnValue, string orderDirectionValue, string orderColumnName)
        {
            var searchValues = searchValue.Split(' ');

            using (var context = new Entities())
            {
                var dataAll = context.ApplicationsGridViews
                    .Select(x => new
                    {
                        AgreementId = x.AgreementId,
                        Name = x.Name,
                        Type = x.Type,
                        Location = x.Location,
                        Contact = x.Contact,
                        Dates = x.Dates,
                        Status = x.Status
                    });

                var dataFiltered = dataAll;

                foreach (var value in searchValues)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        var searchVal = value.ToLower();

                        dataFiltered = dataFiltered.Where(x =>
                            x.Name.ToLower().Contains(searchVal)
                            || x.Type.ToLower().Contains(searchVal)
                            || x.Location.ToLower().Contains(searchVal)
                            || x.Contact.ToLower().Contains(searchVal)
                            || x.Dates.ToLower().Contains(searchVal)
                            || x.Status.ToLower().Contains(searchVal));
                    }
                }

                var dataOrdered = dataFiltered.OrderBy(orderColumnName + " " + orderDirectionValue);
                var dataResult = dataOrdered.Skip(startValue).Take(lengthValue).ToList();

                return new
                {
                    draw = drawValue,
                    recordsTotal = dataAll.Count(),
                    recordsFiltered = dataFiltered.Count(),
                    data = dataResult
                };
            }
        }

        public object GetDropdownItemList(string fieldType)
        {
            using (var context = new Entities())
            {
                if (fieldType == "FieldTypes")
                {
                    var items = (from ft in context.FieldTypes
                                 orderby ft.Order
                                 select new
                                 {
                                     FieldTypeId = ft.FieldTypeId,
                                     Name = ft.Name,
                                     Code = ft.Code
                                 }).ToList();

                    return items;
                }
                else if (fieldType == "AssignmentTypes")
                {
                    var items = (from at in context.AssignmentTypes
                                 orderby at.Order
                                 select new
                                 {
                                     AssignmentTypeId = at.AssignmentTypeId,
                                     Name = at.Name,
                                     Code = at.Code
                                 }).ToList();

                    return items;
                }
                else if (fieldType == "TypeTemplateTypes")
                {
                    var items = (from ttt in context.TypeTemplateTypes
                                 orderby ttt.Order
                                 select new
                                 {
                                     TypeTemplateTypeId = ttt.TypeTemplateTypeId,
                                     Name = ttt.Name,
                                     Code = ttt.Code
                                 }).ToList();

                    return items;
                }
            }

            return new { };
        }

        public List<AppTypeHelper> GetAppTypeList()
        {
            using (var context = new Entities())
            {
                var types = (from t in context.Types
                             orderby t.Order
                             select new AppTypeHelper
                             {
                                 TypeId = t.TypeId,
                                 Name = t.Name,
                                 Code = t.Code,
                                 Order = t.Order,
                                 Guid = t.Guid
                             }).ToList();

                return types;
            }
        }

        public object SaveAppTypeList(List<AppTypeHelper> appTypeList)
        {
            var originalAppTypes = this.GetAppTypeList();

            using (var context = new Entities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var appType in appTypeList)
                        {
                            var originalAppType = originalAppTypes.FirstOrDefault(x => x.TypeId == appType.TypeId);

                            if (originalAppType != null)
                            {
                                var originalAppTypeValue = JsonConvert.SerializeObject(originalAppType).Replace("[]", "null");
                                var newAppTypeValue = JsonConvert.SerializeObject(appType).Replace("[]", "null");

                                if (originalAppTypeValue == newAppTypeValue)
                                {
                                    continue;
                                }
                            }

                            if (appType.TypeId < 0)
                            {
                                context.Types.Add(new Data.Type
                                {
                                    Name = appType.Name,
                                    Code = appType.Code,
                                    Order = appType.Order,
                                    Guid = appType.Guid,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            else
                            {
                                var record = context.Types.First(x => x.TypeId == appType.TypeId);

                                record.Name = appType.Name;
                                record.Code = appType.Code;
                                record.Order = appType.Order;
                                record.UpdatedById = 0;
                                record.UpdatedDate = DateTime.Now;
                            }
                        }

                        // Process removed app types
                        var newAppTypeIds = appTypeList.Select(x => x.TypeId).ToList();
                        foreach (var originalAppType in originalAppTypes.Where(x => !newAppTypeIds.Contains(x.TypeId)))
                        {
                            var record = context.Types.FirstOrDefault(x => x.TypeId == originalAppType.TypeId);

                            if (record != null)
                            {
                                foreach (var typeField in record.TypeFields.ToList())
                                {
                                    foreach (var fieldListItem in typeField.Field.FieldListItems.ToList())
                                    {
                                        context.FieldListItems.Remove(fieldListItem);
                                        context.ListItems.Remove(fieldListItem.ListItem);
                                    }

                                    foreach (var childField in typeField.Field.Fields1.ToList())
                                    {
                                        context.FieldListItems.RemoveRange(childField.FieldListItems.ToList());
                                        context.Fields.Remove(childField);
                                    }

                                    context.Fields.Remove(typeField.Field);

                                    context.TypeFields.Remove(typeField);
                                }

                                foreach (var typeWorkflowStep in record.TypeWorkflowSteps.ToList())
                                {
                                    context.WorkflowSteps.Remove(typeWorkflowStep.WorkflowStep);
                                }

                                context.TypeWorkflowSteps.RemoveRange(record.TypeWorkflowSteps.ToList());

                                context.TypeTemplates.RemoveRange(record.TypeTemplates.ToList());

                                context.Types.Remove(record);
                            }
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return this.GetAppTypeList();
        }

        public object GetFieldList(int typeId)
        {
            var fields = this.GetFieldListByType(typeId, null);

            return fields;
        }

        public object SaveFieldList(int typeId, Guid? guid, List<FieldHelper> fieldList)
        {
            var originalFields = this.GetFieldListByType(typeId, guid);

            using (var context = new Entities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var field in fieldList)
                        {
                            var originalField = originalFields.FirstOrDefault(x => x.FieldId == field.FieldId);

                            if (originalField != null)
                            {
                                var originalFieldValue = JsonConvert.SerializeObject(originalField).Replace("[]", "null");
                                var newFieldValue = JsonConvert.SerializeObject(field).Replace("[]", "null");

                                if (originalFieldValue == newFieldValue)
                                {
                                    continue;
                                }
                            }

                            if (field.FieldId < 0)
                            {
                                var fieldType = context.FieldTypes.FirstOrDefault(x => x.Code == field.FieldTypeCode);
                                var fieldRenderingType = context.FieldRenderingTypes.FirstOrDefault(x => x.Code == field.FieldRenderingTypeCode);

                                var newField = new Field
                                {
                                    Name = field.Name,
                                    Code = field.Code,
                                    Order = field.Order,
                                    FieldType = fieldType,
                                    FieldMultiplicityInd = field.FieldMultiplicityInd,
                                    MaxLength = field.MaxLength,
                                    RequiredInd = field.RequiredInd,
                                    FieldRenderingType = fieldRenderingType,
                                    Guid = field.Guid,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                };

                                context.Fields.Add(newField);

                                this.UpdateFieldListItems(newField, field.ListItems, originalField, context);

                                this.UpdateFieldChildren(newField, field.Children, typeId, originalField, context);

                                context.TypeFields.Add(new TypeField
                                {
                                    TypeId = typeId,
                                    Field = newField,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            else
                            {
                                var record = context.Fields.First(x => x.FieldId == field.FieldId);

                                var fieldType = context.FieldTypes.FirstOrDefault(x => x.Code == field.FieldTypeCode);
                                var fieldRenderingType = context.FieldRenderingTypes.FirstOrDefault(x => x.Code == field.FieldRenderingTypeCode);

                                record.Name = field.Name;
                                record.Code = field.Code;
                                record.Order = field.Order;
                                record.FieldType = fieldType;
                                record.FieldMultiplicityInd = field.FieldMultiplicityInd;
                                record.MaxLength = field.MaxLength;
                                record.RequiredInd = field.RequiredInd;
                                record.FieldRenderingType = fieldRenderingType;
                                record.UpdatedById = 0;
                                record.UpdatedDate = DateTime.Now;

                                this.UpdateFieldListItems(record, field.ListItems, originalField, context);

                                this.UpdateFieldChildren(record, field.Children, typeId, originalField, context);
                            }
                        }

                        // Process removed fields
                        var newFieldIds = fieldList.Select(x => x.FieldId).ToList();
                        foreach (var originalField in originalFields.Where(x => !newFieldIds.Contains(x.FieldId)))
                        {
                            var record = context.Fields.FirstOrDefault(x => x.FieldId == originalField.FieldId);

                            if (record != null)
                            {
                                context.TypeFields.RemoveRange(record.TypeFields.ToList());

                                foreach (var fieldListItem in record.FieldListItems.ToList())
                                {
                                    context.FieldListItems.Remove(fieldListItem);
                                    context.ListItems.Remove(fieldListItem.ListItem);
                                }

                                foreach (var childField in record.Fields1.ToList())
                                {
                                    context.FieldListItems.RemoveRange(childField.FieldListItems.ToList());
                                    context.Fields.Remove(childField);
                                }

                                context.Fields.Remove(record);
                            }
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                    {
                        transaction.Rollback();

                        foreach (var entityValidationErrors in ex.EntityValidationErrors)
                        {
                            foreach (var validationError in entityValidationErrors.ValidationErrors)
                            {
                            }
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return this.GetFieldList(typeId);
        }

        public List<WorkflowStepHelper> GetWorkflowStepList(int typeId, Guid? guid = null)
        {
            using (var context = new Entities())
            {
                var workflowSteps = (from ws in context.WorkflowSteps
                                     join tws in context.TypeWorkflowSteps on ws.WorkflowStepId equals tws.WorkflowStepId
                                     where (typeId > 0 && tws.TypeId == typeId) || (guid != null && tws.Type.Guid == guid)
                                     orderby ws.Order
                                     select new WorkflowStepHelper
                                     {
                                         WorkflowStepId = ws.WorkflowStepId,
                                         Name = ws.Name,
                                         Code = ws.Code,
                                         StatusText = ws.StatusText,
                                         EmailTemplateText = ws.EmailTemplateText,
                                         AssignmentTypeCode = ws.AssignmentType.Code,
                                         Order = ws.Order,
                                         Guid = ws.Guid
                                     }).ToList();

                return workflowSteps;
            }
        }

        public object SaveWorkflowStepList(int typeId, Guid? guid, List<WorkflowStepHelper> workflowStepList)
        {
            var originalWorkflowSteps = this.GetWorkflowStepList(typeId, guid);

            using (var context = new Entities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var workflowStep in workflowStepList)
                        {
                            var originalWorkflowStep = originalWorkflowSteps.FirstOrDefault(x => x.WorkflowStepId == workflowStep.WorkflowStepId);

                            if (originalWorkflowStep != null)
                            {
                                var originalWorkflowStepValue = JsonConvert.SerializeObject(originalWorkflowStep).Replace("[]", "null");
                                var newWorkflowStepValue = JsonConvert.SerializeObject(workflowStep).Replace("[]", "null");

                                if (originalWorkflowStepValue == newWorkflowStepValue)
                                {
                                    continue;
                                }
                            }

                            if (workflowStep.WorkflowStepId < 0)
                            {
                                var assignmentType = context.AssignmentTypes.FirstOrDefault(x => x.Code == workflowStep.AssignmentTypeCode);

                                var newWorkflowStep = new WorkflowStep
                                {
                                    Name = workflowStep.Name,
                                    Code = workflowStep.Code,
                                    StatusText = workflowStep.StatusText,
                                    EmailTemplateText = workflowStep.EmailTemplateText,
                                    AssignmentType = assignmentType,
                                    Order = workflowStep.Order,
                                    Guid = workflowStep.Guid,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                };

                                context.WorkflowSteps.Add(newWorkflowStep);

                                context.TypeWorkflowSteps.Add(new TypeWorkflowStep
                                {
                                    TypeId = typeId,
                                    WorkflowStep = newWorkflowStep,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            else
                            {
                                var record = context.WorkflowSteps.First(x => x.WorkflowStepId == workflowStep.WorkflowStepId);

                                var assignmentType = context.AssignmentTypes.FirstOrDefault(x => x.Code == workflowStep.AssignmentTypeCode);

                                record.Name = workflowStep.Name;
                                record.Code = workflowStep.Code;
                                record.StatusText = workflowStep.StatusText;
                                record.EmailTemplateText = workflowStep.EmailTemplateText;
                                record.AssignmentType = assignmentType;
                                record.Order = workflowStep.Order;
                                record.UpdatedById = 0;
                                record.UpdatedDate = DateTime.Now;
                            }
                        }

                        // Process removed workflow steps
                        var newWorkflowStepIds = workflowStepList.Select(x => x.WorkflowStepId).ToList();
                        foreach (var originalWorkflowStep in originalWorkflowSteps.Where(x => !newWorkflowStepIds.Contains(x.WorkflowStepId)))
                        {
                            var record = context.WorkflowSteps.FirstOrDefault(x => x.WorkflowStepId == originalWorkflowStep.WorkflowStepId);

                            if (record != null)
                            {
                                context.TypeWorkflowSteps.RemoveRange(record.TypeWorkflowSteps.ToList());
                                context.WorkflowSteps.Remove(record);
                            }
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return this.GetWorkflowStepList(typeId);
        }

        public List<TypeTemplateHelper> GetTypeTemplateList(int typeId, Guid? guid = null)
        {
            using (var context = new Entities())
            {
                var typeTemplates = (from tt in context.TypeTemplates
                                     where (typeId > 0 && tt.TypeId == typeId) || (guid != null && tt.Guid == guid)
                                     orderby tt.Order
                                     select new TypeTemplateHelper
                                     {
                                         TypeTemplateId = tt.TypeTemplateId,
                                         Name = tt.Name,
                                         Code = tt.Code,
                                         TypeTemplateTypeCode = tt.TypeTemplateType.Code,
                                         Content = tt.Content,
                                         Order = tt.Order,
                                         Guid = tt.Guid
                                     }).ToList();

                return typeTemplates;
            }
        }

        public object SaveTypeTemplateList(int typeId, Guid? guid, List<TypeTemplateHelper> typeTemplateList)
        {
            var originalTypeTemplates = this.GetTypeTemplateList(typeId, guid);

            using (var context = new Entities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var typeTemplate in typeTemplateList)
                        {
                            var originalTypeTemplate = originalTypeTemplates.FirstOrDefault(x => x.TypeTemplateId == typeTemplate.TypeTemplateId);

                            if (originalTypeTemplate != null)
                            {
                                var originalTypeTemplateValue = JsonConvert.SerializeObject(originalTypeTemplate).Replace("[]", "null");
                                var newTypeTemplateValue = JsonConvert.SerializeObject(typeTemplate).Replace("[]", "null");

                                if (originalTypeTemplateValue == newTypeTemplateValue)
                                {
                                    continue;
                                }
                            }

                            if (typeTemplate.TypeTemplateId < 0)
                            {
                                var typeTemplateType = context.TypeTemplateTypes.FirstOrDefault(x => x.Code == typeTemplate.TypeTemplateTypeCode);

                                context.TypeTemplates.Add(new TypeTemplate
                                {
                                    TypeId = typeId,
                                    TypeTemplateType = typeTemplateType,
                                    Name = typeTemplate.Name,
                                    Code = typeTemplate.Code,
                                    Content = typeTemplate.Content,
                                    Order = typeTemplate.Order,
                                    Guid = typeTemplate.Guid,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            else
                            {
                                var record = context.TypeTemplates.First(x => x.TypeTemplateId == typeTemplate.TypeTemplateId);

                                var typeTemplateType = context.TypeTemplateTypes.FirstOrDefault(x => x.Code == typeTemplate.TypeTemplateTypeCode);

                                record.Name = typeTemplate.Name;
                                record.Code = typeTemplate.Code;
                                record.TypeTemplateType = typeTemplateType;
                                record.Content = typeTemplate.Content;
                                record.Order = typeTemplate.Order;
                                record.UpdatedById = 0;
                                record.UpdatedDate = DateTime.Now;
                            }
                        }

                        // Process removed type templates
                        var newTypeTemplateIds = typeTemplateList.Select(x => x.TypeTemplateId).ToList();
                        foreach (var originalTypeTemplate in originalTypeTemplates.Where(x => !newTypeTemplateIds.Contains(x.TypeTemplateId)))
                        {
                            var record = context.TypeTemplates.FirstOrDefault(x => x.TypeId == originalTypeTemplate.TypeTemplateId);

                            if (record != null)
                            {
                                context.TypeTemplates.Remove(record);
                            }
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }

            return this.GetTypeTemplateList(typeId);
        }

        public object GetAgreementTemplateList(int agreementId)
        {
            using (var context = new Entities())
            {
                var agreement = context.Agreements.FirstOrDefault(x => x.AgreementId == agreementId);
                if (agreement == null)
                {
                    agreement = this.CreateAgreement();
                }

                var agreementTemplates = (from at in context.AgreementTemplates
                                          where at.AgreementId == agreement.AgreementId
                                          orderby at.Order
                                          select new AgreementTemplateHelper
                                          {
                                              AgreementTemplateId = at.AgreementTemplateId,
                                              AgreementId = at.AgreementId,
                                              TypeTemplateId = at.TypeTemplateId,
                                              Name = at.TypeTemplate.Name,
                                              Code = at.TypeTemplate.Code,
                                              TypeTemplateTypeCode = at.TypeTemplate.TypeTemplateType.Code,
                                              RenderAsFormInd = at.TypeTemplate.TypeTemplateType.RenderAsFormInd,
                                              Content = at.Content,
                                              Order = at.Order,
                                              Guid = at.Guid
                                          }).ToList();

                var fields = this.GetFieldListByType(agreement.TypeId);

                var fieldValues = this.GetFieldValueList(agreement.AgreementId, context);

                return new
                {
                    agreementId = agreement.AgreementId,
                    agreementTemplates,
                    fields,
                    fieldValues
                };
            }
        }

        public object SaveFieldValueList(int agreementId, List<AgreementFieldValueHelper> fieldValueList)
        {
            using (var context = new Entities())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var fieldValue in fieldValueList)
                        {
                            /*if (fieldValue.IsRemoved)
                            {
                                var record = context.AgreementFieldValues.FirstOrDefault(x => x.AgreementFieldValueId == fieldValue.AgreementFieldValueId);

                                if (record != null)
                                {
                                    context.AgreementFieldValues.Remove(record);
                                }
                            }
                            else */
                            if (fieldValue.AgreementFieldValueId < 0)
                            {
                                context.AgreementFieldValues.Add(new AgreementFieldValue
                                {
                                    AgreementId = agreementId,
                                    FieldId = fieldValue.FieldId,
                                    Value = fieldValue.Value,
                                    CreatedById = 0,
                                    CreatedDate = DateTime.Now
                                });
                            }
                            else
                            {
                                var record = context.AgreementFieldValues.First(x => x.AgreementFieldValueId == fieldValue.AgreementFieldValueId);

                                record.AgreementId = agreementId;
                                record.FieldId = fieldValue.FieldId;
                                record.Value = fieldValue.Value;
                                record.UpdatedById = 0;
                                record.UpdatedDate = DateTime.Now;
                            }
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

                var fieldValues = this.GetFieldValueList(agreementId, context);

                return new
                {
                    fieldValues
                };
            }
        }

        public Agreement CreateAgreement()
        {
            using (var context = new Entities())
            {
                Agreement agreement;
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {
                        var type = context.Types.First(x => x.Order == 1);
                        var agreementStatusType = context.AgreementStatusTypes.First(x => x.Order == 1);

                        agreement = new Agreement
                        {
                            Type = type,
                            AgreementStatusType = agreementStatusType,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        };

                        context.Agreements.Add(agreement);

                        var typeTemplates = context.TypeTemplates.Where(x => x.TypeId == type.TypeId);
                        foreach (var typeTemplate in typeTemplates)
                        {
                            var agreementTemplate = new AgreementTemplate
                            {
                                Agreement = agreement,
                                TypeTemplate = typeTemplate,
                                Content = typeTemplate.Content,
                                Order = typeTemplate.Order,
                                Guid = typeTemplate.Guid,
                                CreatedById = 0,
                                CreatedDate = DateTime.Now
                            };

                            context.AgreementTemplates.Add(agreementTemplate);
                        }

                        var workflowStatusType = context.WorkflowStatusTypes.First(x => x.Code == "Pending");

                        var typeWorkflowSteps = context.TypeWorkflowSteps.Where(x => x.TypeId == type.TypeId);
                        foreach (var typeWorkflowStep in typeWorkflowSteps)
                        {
                            var agreementWorkflowStepStatus = new AgreementWorkflowStepStatus
                            {
                                Agreement = agreement,
                                WorkflowStep = typeWorkflowStep.WorkflowStep,
                                WorkflowStatusType = workflowStatusType,
                                CreatedById = 0,
                                CreatedDate = DateTime.Now
                            };

                            context.AgreementWorkflowStepStatuses.Add(agreementWorkflowStepStatus);
                        }

                        context.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

                return agreement;
            }
        }

        private List<AgreementFieldValueHelper> GetFieldValueList(int agreementId, Entities context)
        {
            var fieldValues = (from afv in context.AgreementFieldValues
                               where afv.AgreementId == agreementId
                               select new AgreementFieldValueHelper
                               {
                                   AgreementFieldValueId = afv.AgreementFieldValueId,
                                   FieldId = afv.FieldId,
                                   Value = afv.Value
                               }).ToList();

            return fieldValues;
        }

        private List<FieldHelper> GetFieldListByType(int typeId, Guid? guid = null)
        {
            using (var context = new Entities())
            {
                var fields = (from tf in context.TypeFields
                              join f in context.Fields on tf.FieldId equals f.FieldId
                              where ((typeId > 0 && tf.TypeId == typeId) || (guid != null && tf.Type.Guid == guid))
                              && f.ParentFieldId == null
                              orderby f.Order
                              select new FieldHelper
                              {
                                  FieldId = f.FieldId,
                                  ParentFieldId = f.ParentFieldId,
                                  Name = f.Name,
                                  Code = f.Code,
                                  FieldTypeCode = f.FieldType.Code,
                                  FieldMultiplicityInd = f.FieldMultiplicityInd,
                                  MaxLength = f.MaxLength,
                                  RequiredInd = f.RequiredInd,
                                  Order = f.Order,
                                  Guid = f.Guid,
                                  FieldRenderingTypeCode = f.FieldRenderingType.Code
                              }).ToList();

                foreach (var field in fields)
                {
                    var children = context.Fields.Where(x => x.ParentFieldId == field.FieldId);

                    field.Children = (from c in children
                                        orderby c.Order
                                        select new FieldChildHelper
                                        {
                                            FieldId = c.FieldId,
                                            ParentFieldId = c.ParentFieldId,
                                            Name = c.Name,
                                            Code = c.Code,
                                            FieldTypeCode = c.FieldType.Code,
                                            RequiredInd = c.RequiredInd,
                                            Order = c.Order,
                                            Guid = c.Guid,
                                            FieldRenderingTypeCode = c.FieldRenderingType.Code
                                        }).ToList();

                    if (field.Children.Any(x => x.RequiredInd == true))
                    {
                        field.RequiredInd = true;
                    }

                    foreach (var child in field.Children)
                    {
                        child.ListItems = (from fli in context.FieldListItems
                                            where fli.FieldId == child.FieldId
                                            orderby fli.ListItem.Order
                                            select new ListItemHelper
                                            {
                                                ListItemId = fli.ListItemId,
                                                Name = fli.ListItem.Name,
                                                Order = fli.ListItem.Order,
                                                Guid = fli.ListItem.Guid
                                            }).ToList();
                    }
                }

                foreach (var field in fields)
                {
                    field.ListItems = (from fli in context.FieldListItems
                                       where fli.FieldId == field.FieldId
                                       orderby fli.ListItem.Order
                                       select new ListItemHelper
                                       {
                                           ListItemId = fli.ListItemId,
                                           Name = fli.ListItem.Name,
                                           Order = fli.ListItem.Order,
                                           Guid = fli.ListItem.Guid
                                       }).ToList();
                }

                fields = fields.Where(x => x.ParentFieldId == null).OrderBy(x => x.Order).ToList();

                return fields;
            }
        }

        private void UpdateFieldListItems(Field field, IEnumerable<ListItemHelper> listItems, FieldHelper originalField, Entities context)
        {
            if (listItems != null)
            {
                foreach (var listItem in listItems)
                {
                    var originalListItem = originalField.ListItems.FirstOrDefault(x => x.ListItemId == listItem.ListItemId);

                    if (originalListItem != null)
                    {
                        var originalListItemValue = JsonConvert.SerializeObject(originalListItem).Replace("[]", "null");
                        var newListItemValue = JsonConvert.SerializeObject(listItem).Replace("[]", "null");

                        if (originalListItemValue == newListItemValue)
                        {
                            continue;
                        }
                    }

                    if (listItem.ListItemId < 0)
                    {
                        var newListItem = new ListItem
                        {
                            Name = listItem.Name,
                            Code = listItem.Code,
                            Order = listItem.Order,
                            Guid = listItem.Guid,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        };

                        context.ListItems.Add(newListItem);

                        context.FieldListItems.Add(new FieldListItem
                        {
                            Field = field,
                            ListItem = newListItem,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        });
                    }
                    else
                    {
                        var record = context.ListItems.First(x => x.ListItemId == listItem.ListItemId);

                        record.Name = listItem.Name;
                        record.Code = listItem.Code;
                        record.Order = listItem.Order;
                        record.UpdatedById = 0;
                        record.UpdatedDate = DateTime.Now;
                    }
                }

                // Process removed field list items
                var newFieldListItemIds = listItems.Select(x => x.ListItemId).ToList();
                foreach (var originalFieldListItem in originalField.ListItems.Where(x => !newFieldListItemIds.Contains(x.ListItemId)))
                {
                    var record = context.FieldListItems.FirstOrDefault(x => x.ListItemId == originalFieldListItem.ListItemId);

                    if (record != null)
                    {
                        context.FieldListItems.Remove(record);
                    }
                }
            }
        }

        private void UpdateFieldChildren(Field parentField, IEnumerable<FieldChildHelper> children, int typeId, FieldHelper originalField, Entities context)
        {
            if (children != null)
            {
                foreach (var childField in children)
                {
                    var originalChildField = originalField.Children.FirstOrDefault(x => x.FieldId == childField.FieldId);

                    if (originalChildField != null)
                    {
                        var originalChildFieldValue = JsonConvert.SerializeObject(originalChildField).Replace("[]", "null");
                        var newChildFieldValue = JsonConvert.SerializeObject(childField).Replace("[]", "null");

                        if (originalChildFieldValue == newChildFieldValue)
                        {
                            continue;
                        }
                    }

                    if (childField.FieldId < 0)
                    {
                        var fieldType = context.FieldTypes.FirstOrDefault(x => x.Code == childField.FieldTypeCode);
                        var fieldRenderingType = context.FieldRenderingTypes.FirstOrDefault(x => x.Code == childField.FieldRenderingTypeCode);

                        var newChildField = new Field
                        {
                            Field1 = parentField,
                            Name = childField.Name,
                            Code = childField.Code,
                            Order = childField.Order,
                            FieldType = fieldType,
                            FieldMultiplicityInd = childField.FieldMultiplicityInd,
                            MaxLength = childField.MaxLength,
                            RequiredInd = childField.RequiredInd,
                            FieldRenderingType = fieldRenderingType,
                            Guid = childField.Guid,
                            CreatedById = 0,
                            CreatedDate = DateTime.Now
                        };

                        context.Fields.Add(newChildField);

                        this.UpdateFieldListItems(newChildField, childField.ListItems, originalField, context);
                    }
                    else
                    {
                        var record = context.Fields.First(x => x.FieldId == childField.FieldId);

                        var fieldType = context.FieldTypes.FirstOrDefault(x => x.Code == childField.FieldTypeCode);
                        var fieldRenderingType = context.FieldRenderingTypes.FirstOrDefault(x => x.Code == childField.FieldRenderingTypeCode);

                        record.Name = childField.Name;
                        record.Code = childField.Code;
                        record.Order = childField.Order;
                        record.FieldType = fieldType;
                        record.FieldMultiplicityInd = childField.FieldMultiplicityInd;
                        record.MaxLength = childField.MaxLength;
                        record.RequiredInd = childField.RequiredInd;
                        record.FieldRenderingType = fieldRenderingType;
                        record.UpdatedById = 0;
                        record.UpdatedDate = DateTime.Now;

                        this.UpdateFieldListItems(record, childField.ListItems, originalField, context);
                    }
                }

                // Process removed child fields
                var newChildFieldIds = children.Select(x => x.FieldId).ToList();
                foreach (var originalChildField in originalField.Children.Where(x => !newChildFieldIds.Contains(x.FieldId)))
                {
                    var record = context.Fields.FirstOrDefault(x => x.FieldId == originalChildField.FieldId);

                    if (record != null)
                    {
                        foreach (var fieldListItem in record.FieldListItems.ToList())
                        {
                            context.FieldListItems.Remove(fieldListItem);
                            context.ListItems.Remove(fieldListItem.ListItem);
                        }

                        context.Fields.Remove(record);
                    }
                }
            }
        }
    }
}
