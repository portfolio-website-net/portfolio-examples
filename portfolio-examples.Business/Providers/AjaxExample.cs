﻿// <copyright file="AjaxExample.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Providers
{
    using System.Text;
    using Data;
    using Helpers;
    using HeyRed.MarkdownSharp;
    using Interfaces;

    public class AjaxExample : IAjaxExample
    {
        public Customer GetCustomer(int? id)
        {
            using (var db = new Entities())
            {
                var customer = db.Customers.Find(id);
                return customer;
            }
        }

        public string GetHighlightedTextValue(string search)
        {
            string[] searchValues = search.ToLower().Split(' ');
            string textToSearch = "This text contains words which are listed in alphabetical order: apple, banana, candy, ice cream, lollipop.";

            return SearchHelper.GetHighlightedTextValue(searchValues, textToSearch);
        }

        public string CleanHtmlMarkup(string text)
        {
            while (text.Contains("  "))
            {
                text = text.Replace("  ", " ");
            }

            var reverseMarkdownConverter = new ReverseMarkdown.Converter();
            var markdownText = reverseMarkdownConverter.Convert(text);

            var markdownConverter = new Markdown();

            var buffer = new StringBuilder();
            foreach (var line in markdownText.Split('\n'))
            {
                var cleanedLine = line.Trim();

                if (!string.IsNullOrEmpty(cleanedLine))
                {
                    buffer.AppendLine(cleanedLine);
                }
            }

            markdownText = buffer.ToString();

            string cleanedHtml = markdownConverter.Transform(markdownText);

            return cleanedHtml;
        }
    }
}
