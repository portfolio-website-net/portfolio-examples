﻿// <copyright file="MySQLAddresses.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;
    using Data.MySQL;
    using Interfaces;
    using MySql.Data.MySqlClient;
    using ViewModels;

    public class MySQLAddresses : IAddresses
    {
        public IQueryable<AddressViewModel> HydrateAll(Entities optionalEntitiesContext = null)
        {
            var addresses = new List<AddressViewModel>();

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT AddressID, 
                                        AddressLine1,
                                        AddressLine2,
                                        City,
                                        StateProvince,
                                        CountryRegion,
                                        PostalCode
                                   FROM Address";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    int address2Index = reader.GetOrdinal("AddressLine2");

                    addresses.Add(new AddressViewModel
                    {
                        AddressID = reader.GetInt32("AddressID"),
                        AddressLine1 = reader.GetString("AddressLine1"),
                        AddressLine2 = reader.IsDBNull(address2Index) ? null : reader.GetString("AddressLine2"),
                        City = reader.GetString("City"),
                        StateProvince = reader.GetString("StateProvince"),
                        CountryRegion = reader.GetString("CountryRegion"),
                        PostalCode = reader.GetString("PostalCode")
                    });
                }

                dbConnection.Close();
            }

            return addresses.AsQueryable();
        }

        public AddressViewModel Hydrate(int id)
        {
            var address = new AddressViewModel();

            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"SELECT AddressID, 
                                        AddressLine1,
                                        AddressLine2,
                                        City,
                                        StateProvince,
                                        CountryRegion,
                                        PostalCode
                                   FROM Address
                                  WHERE AddressID = @id";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int address2Index = reader.GetOrdinal("AddressLine2");

                    address = new AddressViewModel
                    {
                        AddressID = reader.GetInt32("AddressID"),
                        AddressLine1 = reader.GetString("AddressLine1"),
                        AddressLine2 = reader.IsDBNull(address2Index) ? null : reader.GetString("AddressLine2"),
                        City = reader.GetString("City"),
                        StateProvince = reader.GetString("StateProvince"),
                        CountryRegion = reader.GetString("CountryRegion"),
                        PostalCode = reader.GetString("PostalCode")
                    };
                }

                dbConnection.Close();
            }

            return address;
        }

        public int Dehydrate(AddressViewModel viewModel)
        {
            int id = 0;
            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = string.Empty;

                if (viewModel.AddressID == 0)
                {
                    query = @"INSERT INTO Address (AddressLine1, AddressLine2, City, StateProvince, CountryRegion, PostalCode, rowguid, ModifiedDate)
                              VALUES (@addressLine1, @addressLine2, @city, @stateProvince, @countryRegion, @postalCode, @rowguid, @ModifiedDate)";
                }
                else
                {
                    query = @"UPDATE Address
                                 SET AddressLine1 = @addressLine1,
                                     AddressLine2 = @addressLine2,
                                     City = @city,
                                     StateProvince = @stateProvince,
                                     CountryRegion = @countryRegion,
                                     PostalCode = @postalCode,
                                     ModifiedDate = @ModifiedDate
                               WHERE AddressID = @id";
                }

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@addressLine1", viewModel.AddressLine1);
                cmd.Parameters.AddWithValue("@addressLine2", viewModel.AddressLine2);
                cmd.Parameters.AddWithValue("@city", viewModel.City);
                cmd.Parameters.AddWithValue("@stateProvince", viewModel.StateProvince);
                cmd.Parameters.AddWithValue("@countryRegion", viewModel.CountryRegion);
                cmd.Parameters.AddWithValue("@postalCode", viewModel.PostalCode);

                if (viewModel.AddressID == 0)
                {
                    cmd.Parameters.AddWithValue("@rowguid", Guid.NewGuid().ToString());
                }
                else
                {
                    cmd.Parameters.AddWithValue("@id", viewModel.AddressID);
                }

                cmd.Parameters.AddWithValue("@ModifiedDate", DateTime.Now.ToString());

                var reader = cmd.ExecuteNonQuery();
                id = (int)cmd.LastInsertedId;

                dbConnection.Close();
            }

            return id;
        }

        public void Remove(int id)
        {
            var dbConnection = DBConnection.Instance();
            if (dbConnection.IsConnect())
            {
                string query = @"DELETE FROM Address
                                  WHERE AddressID = @id";

                var cmd = new MySqlCommand();
                cmd.Connection = dbConnection.Connection;
                cmd.CommandText = query;
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);

                var reader = cmd.ExecuteNonQuery();

                dbConnection.Close();
            }
        }
    }
}
