﻿// <copyright file="EmployeesExtract.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.Business.Providers
{
    using System;
    using System.Collections.Generic;
    using Helpers;
    using Interfaces;

    public class EmployeesExtract : IEmployeesExtract
    {
        public void StartEmployeeExtract(string startDate, string endDate)
        {
            if (string.IsNullOrEmpty(endDate))
            {
                endDate = startDate;
            }

            var date = DateTime.Parse(startDate);

            var employeesHelper = new EmployeesHelper();
            var dataUrlInfoList = new List<EmployeesHelper.DataUrlInfo>();
            while (date <= DateTime.Parse(endDate))
            {
                var urlInfo = employeesHelper.GetDataUrlInfoForDate(date);
                if (urlInfo == null)
                {
                    continue;
                }

                dataUrlInfoList.Add(urlInfo);
                employeesHelper.ProcessData(dataUrlInfoList);
                date = date.AddMonths(1);
            }

            employeesHelper.ProcessData(dataUrlInfoList);
        }
    }
}
