//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PortfolioExamples.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Employee
    {
        public int EmployeeId { get; set; }
        public int DepartmentId { get; set; }
        public System.DateTime Date { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Position { get; set; }
        public Nullable<decimal> Salary { get; set; }
        public Nullable<decimal> Wage { get; set; }
        public int Page { get; set; }
        public int CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedById { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    
        public virtual Department Department { get; set; }
    }
}
