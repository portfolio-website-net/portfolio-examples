//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PortfolioExamples.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class AgreementTemplate
    {
        public int AgreementTemplateId { get; set; }
        public int AgreementId { get; set; }
        public int TypeTemplateId { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }
        public Nullable<System.Guid> Guid { get; set; }
        public int CreatedById { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> UpdatedById { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
    
        public virtual Agreement Agreement { get; set; }
        public virtual TypeTemplate TypeTemplate { get; set; }
    }
}
