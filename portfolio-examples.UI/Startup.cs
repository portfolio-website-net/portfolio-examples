﻿// <copyright file="Startup.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

[assembly: Microsoft.Owin.OwinStartup(typeof(PortfolioExamples.UI.Startup))]

namespace PortfolioExamples.UI
{
    using Hangfire;
    using Hangfire.Dashboard;
    using Owin;

    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard(
                "/hangfire",
                new DashboardOptions { Authorization = new[] { new AuthorizationFilter() } });

            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }

        private class AuthorizationFilter : IDashboardAuthorizationFilter
        {
            public bool Authorize(DashboardContext context)
            {
                // Allow all users to see the Dashboard (potentially dangerous).
                return true;
            }
        }
    }
}
