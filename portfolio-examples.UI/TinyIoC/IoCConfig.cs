﻿// <copyright file="IoCConfig.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.TinyIoC
{
    using System.Web.Mvc;
    using Business.Interfaces;
    using Business.Providers;
    using global::TinyIoC;

    public static class IoCConfig
    {
        public static void Register()
        {
            var container = TinyIoCContainer.Current;
            container.Register<IAjaxExample, AjaxExample>().AsPerRequestSingleton();
            container.Register<IApp, App>().AsPerRequestSingleton();
            container.Register<IEmployeesExtract, EmployeesExtract>().AsPerRequestSingleton();
            container.Register<IAddresses, SQLServerAddresses>().AsPerRequestSingleton();
            container.Register<IPlaylistExtract, PlaylistExtract>().AsPerRequestSingleton();

            DependencyResolver.SetResolver(new TinyIocMvcDependencyResolver(container));
        }
    }
}