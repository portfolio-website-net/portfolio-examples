var isInIframe = (window.location != window.parent.location) ? true : false;

function setTitle(text) {
    $('#dialog-form .modal-title').html(text);
}

function setParentTitle(title) {
    if (!isAtRootWindow()) {
        parent.setTitle(title);
    }
}

function hideFooterIfChild() {
    if (!isAtRootWindow()) {
        $('footer').hide();
    }
}

function initPage(title) {
    setParentTitle(title);
    hideFooterIfChild()
}

function closePage() {
    if (!isAtRootWindow()) {
        parent.closePage();
    }
    else {
        $('.modal-header .close').click();
    }
}

function refreshParentGrid() {
    if (isInIframe && parent.refreshGrid) {
        parent.refreshGrid();
    }
}

function isAtRootWindow() {
    try {
        if (isInIframe && parent.closePage) {
            return false;
        }
        else {
            return true;
        }
    }
    catch (e) {
        return true;
    }
}

function showError(fieldname, message) {
    $("#" + fieldname).addClass("validation_error");
    $("#" + fieldname).removeClass("validation_success");
    $("#" + fieldname + "_error").removeClass("hide");
    if (message != null) {
        $("#" + fieldname + "_error").text(message);
    }
}

function hideError(fieldname) {
    $("#" + fieldname).removeClass("validation_error");
    $("#" + fieldname + "_error").addClass("hide");
}

function applyRequiredFieldValidation(source, id) {

    var valid = true;

    if ($('#' + id).length) {
        if (source == null || (source != null && source.id == id)) {
            if ($('#' + id).val() == "" || $('#' + id).val() == null) {
                showError(id);
                valid = false;
            }
            else {
                hideError(id);
            }
        }
    }

    return valid;
}