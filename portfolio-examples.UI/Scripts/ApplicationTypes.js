var timerHandleRef;

var dropdownItemList = {};
var dropdownCount = 0;

var appTypeList = null;
var fieldList = null;
var workflowStepList = null;
var typeTemplateList = null;

var currentTypeId = 0;
var currentFieldId = 0;

var newItemId = -1;

var saveCounter = 0;
    
// Drag-and-Drop Source URL: https://jsfiddle.net/bgrins/tzYbU/
// Reference URL: http://www.foliotek.com/devblog/make-table-rows-sortable-using-jquery-ui-sortable/

var fixHelper = function (e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
};

// Source URL: https://stackoverflow.com/questions/4992383/use-jquerys-find-on-json-object#4992429
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}

function sortByOrder(a, b) {
    var aOrder = a.Order;
    var bOrder = b.Order;
    return ((aOrder < bOrder) ? -1 : ((aOrder > bOrder) ? 1 : 0));
}

function scrollToId(id) {
    $('html, body').animate({ scrollTop: $('#' + id).offset().top - 100 }, 1000);
}

function copyObj(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function hasObjChanged(obj1, obj2) {
    return JSON.stringify(obj1) != JSON.stringify(obj2);
}

function getAppTypeById(typeId) {

    var result = getObjects(appTypeList, 'TypeId', typeId);

    if (result && result.length > 0) {
        return result[0];
    }
    else {
        return null;
    }
}

function removeAppTypeById(typeId) {

    var obj = getAppTypeById(typeId);
    obj.IsRemoved = true;
    obj.IsUpdated = true;
}

function getNewAppTypeCount() {
    var count = 0;
    for (var i = 0; i < appTypeList.length; i++) {
        if ((appTypeList[i].TypeId < 0 || appTypeList[i].Name.indexOf("New Application Type ") >= 0) && !appTypeList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function getNewAppTypeOrder() {
    var count = 0;
    for (var i = 0; i < appTypeList.length; i++) {
        if (!appTypeList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function addAppType() {
    var obj = new Object();
    obj.TypeId = newItemId--;
    obj.Name = 'New Application Type ' + (getNewAppTypeCount() + 1);
    obj.Code = null;
    obj.Order = getNewAppTypeOrder() + 1;
    obj.IsRemoved = false;
    obj.IsUpdated = true;

    appTypeList.push(obj);

    initializeAppTypeLinks();

    updateAppTypeListFromPage();
    saveAppTypeList(true);
}

function updateAppTypeListFromPage() {

    var count = 1;
    $.each(appTypeList, function () {

        if (!this.IsRemoved) {

            var objCopy = copyObj(this);

            if ($('#AppTypeName').length && currentTypeId == this.TypeId) {
                this.Name = $('#AppTypeName').val();
            }

            var rowField = $('#' + 'AppType_' + this.TypeId);
            var rowNum = rowField.parents('td').parent().parent().children().index(rowField.parents('td').parent()) + 1;

            this.Order = rowNum;

            if (hasObjChanged(this, objCopy)) {
                this.IsUpdated = true;
            }
        }
    });
}

function getFieldById(fieldId) {

    var result = getObjects(fieldList, 'FieldId', fieldId);

    if (result && result.length > 0) {
        return result[0];
    }
    else {
        return null;
    }
}

function removeFieldById(fieldId) {

    var obj = getFieldById(fieldId);
    obj.IsRemoved = true;
    obj.IsUpdated = true;
}

function getNewFieldCount(parentFieldId) {
    var count = 0;

    if (parentFieldId) {
        var parentField = getFieldById(parentFieldId);
        for (var i = 0; i < parentField.Children.length; i++) {
            if ((parentField.Children[i].FieldId < 0 || parentField.Children[i].Name.indexOf("New Field ") >= 0) && !parentField.Children[i].IsRemoved) {
                count++;
            }
        }
    }
    else {
        for (var i = 0; i < fieldList.length; i++) {
            if ((fieldList[i].FieldId < 0 || fieldList[i].Name.indexOf("New Field ") >= 0) && !fieldList[i].IsRemoved) {
                count++;
            }
        }
    }

    return count;
}

function getNewFieldOrder(parentFieldId) {
    var count = 0;

    if (parentFieldId) {
        var parentField = getFieldById(parentFieldId);
        for (var i = 0; i < parentField.Children.length; i++) {
            if (!parentField.Children[i].IsRemoved) {
                count++;
            }
        }
    }
    else {
        for (var i = 0; i < fieldList.length; i++) {
            if (!fieldList[i].IsRemoved) {
                count++;
            }
        }
    }

    return count;
}

function addField(parentFieldId) {
    var obj = new Object();
    obj.FieldId = newItemId--;
    obj.ParentFieldId = parentFieldId;
    obj.Name = 'New Field ' + (getNewFieldCount(parentFieldId) + 1);
    obj.Code = null;
    obj.FieldTypeName = '[Select One]';
    obj.FieldTypeCode = null;
    obj.FieldMultiplicityInd = false;
    obj.MaxLength = null;
    obj.RequiredInd = false;
    obj.FieldRenderingTypeName = '[Select One]';
    obj.FieldRenderingTypeCode = null;
    obj.Order = getNewFieldOrder(parentFieldId) + 1;

    if (!parentFieldId) {
        obj.Children = [];
    }

    obj.HasListItems = false;
    obj.ListItems = [];
    obj.IsRemoved = false;
    obj.IsUpdated = true;

    if (!parentFieldId) {
        fieldList.push(obj);
    }
    else {
        var parentField = getFieldById(parentFieldId);
        parentField.Children.push(obj);
    }

    populateFieldList(true);

    if (parentFieldId) {
        populateFieldListEditor(parentFieldId);
    }
}

function updateFieldListFromPage(fieldId) {

    var fieldPrefix = '';
    var filteredFieldList = fieldList;

    // If an ID is passed, this will render the field list editor for the child fields
    if (fieldId) {
        fieldPrefix = 'MultipleFieldList_';

        var field = getFieldById(fieldId);
        filteredFieldList = field.Children;
    }

    var count = 1;
    $.each(filteredFieldList, function () {

        if (!this.IsRemoved
            && $('#' + fieldPrefix + 'FieldName_' + this.FieldId).length) {

            var objCopy = copyObj(this);

            this.Name = $('#' + fieldPrefix + 'FieldName_' + this.FieldId).val();
            this.FieldTypeName = $('#' + fieldPrefix + 'FieldType_' + this.FieldId + ' option:selected').text();
            this.FieldTypeCode = $('#' + fieldPrefix + 'FieldType_' + this.FieldId).val();

            if ($('#' + fieldPrefix + 'FieldRequired_' + this.FieldId).is(":checked")) {
                this.RequiredInd = true;
            }
            else {
                this.RequiredInd = false;
            }

            var rowField = $('#' + fieldPrefix + 'FieldName_' + this.FieldId);
            var rowNum = rowField.parents('td').parent().parent().children().index(rowField.parents('td').parent()) + 1;

            this.Order = rowNum;

            if (hasObjChanged(this, objCopy)) {
                this.IsUpdated = true;

                if (fieldId) {
                    var field = getFieldById(fieldId);
                    field.IsUpdated = true;
                }
            }
        }
    });
}

function updateFieldFromModal() {

    if ($('#FieldName:visible').length) {
        var fieldId = $('#FieldId').val();
        var field = getFieldById(fieldId);

        tinymce.triggerSave();

        var objCopy = copyObj(field);

        field.Name = $('#FieldName').val();
        field.FieldTypeName = $('#FieldType option:selected').text();
        field.FieldTypeCode = $('#FieldType').val();

        if ($('#' + 'FieldMultiplicity').is(":checked")) {
            field.FieldMultiplicityInd = true;
        }
        else {
            field.FieldMultiplicityInd = false;
        }

        field.MaxLength = $('#FieldMaxLength').val();

        if ($('#' + 'FieldCurrencySetting').is(":checked")) {
            field.FieldRenderingTypeName = 'Whole Dollars';
            field.FieldRenderingTypeCode = 'WholeDollars';
        }
        else if ($('#' + 'RenderAsCheckboxes').is(":checked")) {
            field.FieldRenderingTypeName = 'Render as Checkboxes';
            field.FieldRenderingTypeCode = 'RenderAsCheckboxes';
        }
        else if ($('#' + 'RenderAsMultiSelect').is(":checked")) {
            field.FieldRenderingTypeName = 'Render as Multi-Select';
            field.FieldRenderingTypeCode = 'RenderAsMultiSelect';
        }
        else {
            field.FieldRenderingTypeName = '';
            field.FieldRenderingTypeCode = '';
        }

        if ($('#' + 'FieldValidationRequired').is(":checked")) {
            field.RequiredInd = true;
        }
        else {
            field.RequiredInd = false;
        }

        if (hasObjChanged(field, objCopy)) {
            field.IsUpdated = true;

            if (field.ParentFieldId) {
                var parentField = getFieldById(field.ParentFieldId);
                parentField.IsUpdated = true;
            }
        }

        updateListItemsFromModal();
        updateFieldListFromPage(fieldId);
    }
}

function getListItemById(listItemId) {

    var result = getObjects(fieldList, 'ListItemId', listItemId);

    if (result && result.length > 0) {
        return result[0];
    }
    else {
        return null;
    }
}

function removeListItemById(listItemId) {

    var obj = getListItemById(listItemId);
    obj.IsRemoved = true;
    obj.IsUpdated = true;        
}

function getNewListItemCount(fieldId) {
    var field = getFieldById(fieldId);
    var count = 0;
    for (var i = 0; i < field.ListItems.length; i++) {
        if ((field.ListItems[i].ListItemId < 0 || field.ListItems[i].Name.indexOf("New Item ") >= 0) && !field.ListItems[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function getNewListItemOrder(fieldId) {
    var field = getFieldById(fieldId);
    var count = 0;
    for (var i = 0; i < field.ListItems.length; i++) {
        if (!field.ListItems[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function addListItem(fieldId) {
    var field = getFieldById(fieldId);
    var obj = new Object();
    obj.ListItemId = newItemId--;
    obj.Name = 'New Item ' + (getNewListItemCount(fieldId) + 1);
    obj.Order = getNewListItemOrder(fieldId) + 1;
    obj.IsRemoved = false;
    obj.IsUpdated = true;

    field.HasListItems = true;

    field.ListItems.push(obj);

    field.IsUpdated = true;

    if (field.ParentFieldId) {
        var parentField = getFieldById(field.ParentFieldId);
        parentField.IsUpdated = true;
    }

    populateSelectionList(fieldId);
}

function updateListItemsFromModal() {

    if ($('#FieldName:visible').length) {
        var fieldId = $('#FieldId').val();
        var field = getFieldById(fieldId);

        field.HasListItems = (getNewListItemCount(fieldId) != 0);

        var count = 1;
        $.each(field.ListItems, function () {

            if (!this.IsRemoved) {

                var objCopy = copyObj(this);

                this.Name = $('#' + 'SelectionListItemName_' + this.ListItemId).val();

                var rowField = $('#' + 'SelectionListItemName_' + this.ListItemId);
                var rowNum = rowField.parents('td').parent().parent().children().index(rowField.parents('td').parent()) + 1;

                this.Order = rowNum;

                if (hasObjChanged(this, objCopy)) {
                    this.IsUpdated = true;
                    field.IsUpdated = true;

                    if (field.ParentFieldId) {
                        var parentField = getFieldById(field.ParentFieldId);
                        parentField.IsUpdated = true;
                    }
                }
            }
        });
    }
}

function getWorkflowStepById(workflowStepId) {

    var result = getObjects(workflowStepList, 'WorkflowStepId', workflowStepId);

    if (result && result.length > 0) {
        return result[0];
    }
    else {
        return null;
    }
}

function removeWorkflowStepById(workflowStepId) {

    var obj = getWorkflowStepById(workflowStepId);
    obj.IsRemoved = true;
    obj.IsUpdated = true;
}

function getNewWorkflowStepCount() {
    var count = 0;
    for (var i = 0; i < workflowStepList.length; i++) {
        if ((workflowStepList[i].WorkflowStepId < 0 || workflowStepList[i].Name.indexOf("New Workflow Step ") >= 0) && !workflowStepList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function getNewWorkflowStepOrder() {
    var count = 0;
    for (var i = 0; i < workflowStepList.length; i++) {
        if (!workflowStepList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function addWorkflowStep() {
    var obj = new Object();
    obj.WorkflowStepId = newItemId--;
    obj.Name = 'New Workflow Step ' + (getNewWorkflowStepCount() + 1);
    obj.Code = null;
    obj.StatusText = null;
    obj.EmailTemplateText = null;
    obj.AssignmentTypeName = '[Select One]';
    obj.AssignmentTypeCode = null;
    obj.Order = getNewWorkflowStepOrder() + 1;
    obj.IsRemoved = false;
    obj.IsUpdated = true;

    workflowStepList.push(obj);

    populateWorkflowStepList(true);
}

function updateWorkflowStepListFromPage() {

    var count = 1;
    $.each(workflowStepList, function () {

        if (!this.IsRemoved) {

            var objCopy = copyObj(this);

            this.Name = $('#' + 'WorkflowStepName_' + this.WorkflowStepId).val();
            this.AssignmentTypeName = $('#' + 'WorkflowStepType_' + this.WorkflowStepId + ' option:selected').text();
            this.AssignmentTypeCode = $('#' + 'WorkflowStepType_' + this.WorkflowStepId).val();

            var rowField = $('#' + 'WorkflowStepName_' + this.WorkflowStepId);
            var rowNum = rowField.parents('td').parent().parent().children().index(rowField.parents('td').parent()) + 1;

            this.Order = rowNum;

            if (hasObjChanged(this, objCopy)) {
                this.IsUpdated = true;
            }
        }
    });
}

function updateWorkflowStepFromModal() {

    if ($('#WorkflowStepName:visible').length) {
        var workflowStepId = $('#WorkflowStepId').val();
        var workflowStep = getWorkflowStepById(workflowStepId);

        tinymce.triggerSave();

        var objCopy = copyObj(workflowStep);

        workflowStep.Name = $('#WorkflowStepName').val();
        workflowStep.AssignmentTypeName = $('#WorkflowStepAssignmentType option:selected').text();
        workflowStep.AssignmentTypeCode = $('#WorkflowStepAssignmentType').val();
        workflowStep.StatusText = $('#WorkflowStepStatusText').val();
        workflowStep.EmailTemplateText = $('#WorkflowStepEmailTemplateText').val();

        if (hasObjChanged(workflowStep, objCopy)) {
            workflowStep.IsUpdated = true;
        }
    }
}

function getTypeTemplateById(typeTemplateId) {

    var result = getObjects(typeTemplateList, 'TypeTemplateId', typeTemplateId);

    if (result && result.length > 0) {
        return result[0];
    }
    else {
        return null;
    }
}

function removeTypeTemplateById(typeTemplateId) {

    var obj = getTypeTemplateById(typeTemplateId);
    obj.IsRemoved = true;
    obj.IsUpdated = true;
}

function getNewTypeTemplateCount() {
    var count = 0;
    for (var i = 0; i < typeTemplateList.length; i++) {
        if ((typeTemplateList[i].TypeTemplateId < 0 || typeTemplateList[i].Name.indexOf("New Template ") >= 0) && !typeTemplateList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function getNewTypeTemplateOrder() {
    var count = 0;
    for (var i = 0; i < typeTemplateList.length; i++) {
        if (!typeTemplateList[i].IsRemoved) {
            count++;
        }
    }

    return count;
}

function addTypeTemplate() {
    var obj = new Object();
    obj.TypeTemplateId = newItemId--;
    obj.Name = 'New Template ' + (getNewTypeTemplateCount() + 1);
    obj.Code = null;
    obj.TypeTemplateTypeCode = null;
    obj.TypeTemplateTypeName = '[Select One]';
    obj.Content = null;
    obj.Order = getNewTypeTemplateOrder() + 1;
    obj.IsRemoved = false;
    obj.IsUpdated = true;

    typeTemplateList.push(obj);

    populateTypeTemplateList(true);
}

function updateTypeTemplateListFromPage() {

    var count = 1;
    $.each(typeTemplateList, function () {

        if (!this.IsRemoved) {

            var objCopy = copyObj(this);

            this.Name = $('#' + 'TypeTemplateName_' + this.TypeTemplateId).val();
            this.TypeTemplateTypeName = $('#' + 'TypeTemplateType_' + this.TypeTemplateId + ' option:selected').text();
            this.TypeTemplateTypeCode = $('#' + 'TypeTemplateType_' + this.TypeTemplateId).val();

            var rowField = $('#' + 'TypeTemplateName_' + this.TypeTemplateId);
            var rowNum = rowField.parents('td').parent().parent().children().index(rowField.parents('td').parent()) + 1;

            this.Order = rowNum;

            if (hasObjChanged(this, objCopy)) {
                this.IsUpdated = true;
            }
        }
    });
}

function updateTypeTemplateFromModal() {

    if ($('#TypeTemplateName:visible').length) {
        var typeTemplateId = $('#TypeTemplateId').val();
        var typeTemplate = getTypeTemplateById(typeTemplateId);

        tinymce.triggerSave();

        var objCopy = copyObj(typeTemplate);

        typeTemplate.Name = $('#TypeTemplateName').val();
        typeTemplate.TypeTemplateTypeName = $('#TypeTemplateType option:selected').text();
        typeTemplate.TypeTemplateTypeCode = $('#TypeTemplateType').val();
        typeTemplate.Content = $('#TypeTemplateContent').val();

        if (hasObjChanged(typeTemplate, objCopy)) {
            typeTemplate.IsUpdated = true;
        }
    }
}

function populateSelectionList(fieldId) {

    var field = getFieldById(fieldId);

    $('#tblSelectionItemList tbody').html('');

    field.ListItems.sort(sortByOrder);

    $.each(field.ListItems, function () {

        if (!this.IsRemoved) {
            $('#tblSelectionItemList tbody').append('<tr>'
                + '<td>'
                    + '<input type="text" class="form-control form-control-sm ListItemsModalChangeDetection" id="SelectionListItemName_' + this.ListItemId + '" name="SelectionListItemName_' + this.ListItemId + '" data-id="' + this.ListItemId + '" data-fieldid="' + fieldId + '" value="' + this.Name + '" />'
                + '</td>'
                + '<td>'
                    + '<a href="#" class="SelectionListItemRemoveLink" id="SelectionListItemRemove_' + this.ListItemId + '" name="SelectionListItemRemove_' + this.ListItemId + '" data-id="' + this.ListItemId + '" data-fieldid="' + fieldId + '">Remove</a>'
                + '</td>'
            + '</tr>');
        }
    });

    $('#tblSelectionItemList tbody').sortable({
        helper: fixHelper,
        stop: function (event, ui) {
            saveAndReloadListItemsModal();
        }
    }).disableSelection();

    $('#btnCreateNewListItem').data('fieldid', fieldId);

    $('#btnCreateNewListItem').off('click');
    $('#btnCreateNewListItem').on('click', function (event) {
        var btn = $(event.currentTarget);
        var fieldId = btn.data('fieldid');
        addListItem(fieldId);

        updateListItemsFromModal();
        saveFieldList(true);
    });

    $('.SelectionListItemRemoveLink').off('click');
    $('.SelectionListItemRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this list item?')) {
            var link = $(event.currentTarget);
            var fieldId = link.data('fieldid');
            var listItemId = link.data('id');

            removeListItemById(listItemId);

            populateSelectionList(fieldId);

            saveAndReloadListItemsModal();
        }
    });

    initializeChangeDetection();
}

function updateSelectionListIds(fieldId) {

    var field = getFieldById(fieldId);

    if (field) {

        field.ListItems.sort(sortByOrder);

        $.each(field.ListItems, function () {

            if (!this.IsRemoved) {

                var listItem = this;

                $('#tblSelectionItemList tbody [id^="' + 'SelectionListItemName_"]').eq(listItem.Sort - 1).each(function () {
                    $(this).attr('id', 'SelectionListItemName_' + listItem.ListItemId);
                    $(this).attr('name', 'SelectionListItemName_' + listItem.ListItemId);
                    $(this).attr('data-id', listItem.ListItemId);
                });

                $('#tblSelectionItemList tbody [id^="' + 'SelectionListItemRemove_"]').eq(listItem.Sort - 1).each(function () {
                    $(this).attr('id', 'SelectionListItemRemove_' + listItem.ListItemId);
                    $(this).attr('name', 'SelectionListItemRemove_' + listItem.ListItemId);
                    $(this).attr('data-id', listItem.ListItemId);
                });
            }
        });
    }
}

function initializeFieldEditModal() {

    $('#FieldEditModal').off('show.bs.modal');
    $('#FieldEditModal').on('show.bs.modal', function (event) {

        var link = null;

        if (event.relatedTarget) {
            link = $(event.relatedTarget);
        }

        var fieldId = 0;

        if (link) {
            fieldId = link.data('id');
        }
        else {
            fieldId = currentFieldId;
        }

        populateFieldEditModal($(this), fieldId);
    });
}

initializeFieldEditModal();

function initializeWorkflowStepEditModal() {

    $('#WorkflowStepEditModal').off('show.bs.modal');
    $('#WorkflowStepEditModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var workflowStepId = link.data('id');
        var workflowStep = getWorkflowStepById(workflowStepId);
        var modal = $(this);

        tinymce.remove();

        modal.find('.modal-title').text('Edit Workflow Step: ' + workflowStep.Name);
        modal.find('.modal-body #WorkflowStepId').val(workflowStep.WorkflowStepId);
        modal.find('.modal-body #WorkflowStepName').val(workflowStep.Name);
        modal.find('.modal-body #WorkflowStepStatusText').val(workflowStep.StatusText);
        modal.find('.modal-body #WorkflowStepEmailTemplateText').val(workflowStep.EmailTemplateText);
        populateDropdown('WorkflowStepAssignmentType', 'AssignmentTypes', workflowStep.AssignmentTypeCode);

        tinymce.init({
            selector: '.RichText',
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor textcolor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | code | help',
        });

        initializeChangeDetection();
    });
}

initializeWorkflowStepEditModal();

function initializeTypeTemplateEditModal() {

    $('#TypeTemplateEditModal').off('show.bs.modal');
    $('#TypeTemplateEditModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var typeTemplateId = link.data('id');
        var typeTemplate = getTypeTemplateById(typeTemplateId);
        var modal = $(this);

        tinymce.remove();

        modal.find('.modal-title').text('Edit Template: ' + typeTemplate.Name);
        modal.find('.modal-body #TypeTemplateId').val(typeTemplate.TypeTemplateId);
        modal.find('.modal-body #TypeTemplateName').val(typeTemplate.Name);
        modal.find('.modal-body #TypeTemplateContent').val(typeTemplate.Content);
        populateDropdown('TypeTemplateType', 'TypeTemplateTypes', typeTemplate.TypeTemplateTypeCode);

        tinymce.init({
            selector: '.RichText',
            menubar: false,
            plugins: [
              'advlist autolink lists link image charmap print preview anchor textcolor',
              'searchreplace visualblocks code fullscreen',
              'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | code | help',
        });

        initializeChangeDetection();
    });

    $('#TypeTemplateEditModal').off('shown.bs.modal');
    $('#TypeTemplateEditModal').on('shown.bs.modal', function (event) {
        $(document).off('focusin.modal');
    });

    $('#TypeTemplateEditModal').off('hide.bs.modal');
    $('#TypeTemplateEditModal').on('hide.bs.modal', function (event) {
        saveAndReloadTypeTemplateModal();
    });
}

initializeTypeTemplateEditModal();

function populateFieldEditModal(modal, fieldId) {

    var field = getFieldById(fieldId);

    modal.find('.FieldProperties').hide();

    if (field.ParentFieldId == null) {
        modal.find('#btnEditParentField').hide();
    }
    else {
        modal.find('#btnEditParentField').show();
        modal.find('#btnEditParentField').data('id', field.ParentFieldId);
    }

    if (field.FieldTypeCode == 'CollectionOfFields') {
        modal.find('#pnlMultipleFieldProperties').show();

        populateFieldListEditor(field.FieldId);
    }
    else if (field.FieldTypeCode == 'SelectionList') {
        modal.find('#pnlSelectionListFieldProperties').show();
        modal.find('#pnlRequiredFieldProperty').show();

        populateSelectionList(fieldId);
    }
    else if (field.FieldTypeCode == 'CurrencyAmount') {
        modal.find('#pnlSingleFieldProperties').show();
        modal.find('#pnlCurrencyFieldProperties').show();
        modal.find('#pnlRequiredFieldProperty').show();
    }
    else {
        modal.find('#pnlSingleFieldProperties').show();
        modal.find('#pnlRequiredFieldProperty').show();
    }

    modal.find('.modal-title').text('Edit Field: ' + field.Name);
    modal.find('.modal-body #FieldId').val(field.FieldId);
    modal.find('.modal-body #FieldName').val(field.Name);

    if (field.FieldMultiplicityInd) {
        modal.find('.modal-body #FieldMultiplicity').prop('checked', true);
    }
    else {
        modal.find('.modal-body #FieldMultiplicity').prop('checked', false);
    }

    modal.find('.modal-body #FieldMaxLength').val(field.MaxLength);

    if (field.FieldRenderingTypeCode == 'WholeDollars') {
        modal.find('.modal-body #FieldCurrencySetting').prop('checked', true);
    }
    else {
        modal.find('.modal-body #FieldCurrencySetting').prop('checked', false);
    }

    if (field.FieldRenderingTypeCode == 'RenderAsCheckboxes') {
        modal.find('.modal-body #RenderAsCheckboxes').prop('checked', true);
    }
    else {
        modal.find('.modal-body #RenderAsCheckboxes').prop('checked', false);
    }

    if (field.FieldRenderingTypeCode == 'RenderAsMultiSelect') {
        modal.find('.modal-body #RenderAsMultiSelect').prop('checked', true);
    }
    else {
        modal.find('.modal-body #RenderAsMultiSelect').prop('checked', false);
    }

    if (field.RequiredInd) {
        modal.find('.modal-body #FieldValidationRequired').prop('checked', true);
    }
    else {
        modal.find('.modal-body #FieldValidationRequired').prop('checked', false);
    }

    populateDropdown('FieldType', 'FieldTypes', field.FieldTypeCode, field);               

    initializeChangeDetection();

    currentFieldId = fieldId;        
}

function getDropdownItemList(fieldType) {

    dropdownCount++;

    $.ajax({
        url: 'GetDropdownItemListAjax',
        type: 'POST',
        data: {
            fieldType: fieldType
        },
        success: function (response) {

            dropdownItemList[fieldType] = response.items;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}
    
function populateDropdown(id, fieldType, selectedVal, fieldObj) {
    var options = $('#' + id);

    options.html('');
    options.append($("<option />").val('').text('[Select One]'));

    $.each(dropdownItemList[fieldType], function () {

        if (fieldObj != null
            && fieldObj.ParentFieldId != null
            && this.Code == 'CollectionOfFields') {
            // Skip item to allow only one level for the parent-child relationship
        }
        else {
            options.append($("<option />").val(this.Code).text(this.Name));
        }
    });

    if (selectedVal) {
        options.val(selectedVal);
    }
}

function populateApplicationType() {

    if (Object.keys(dropdownItemList).length == dropdownCount
        && fieldList != null
        && workflowStepList != null
        && typeTemplateList != null) {

        if (timerHandleRef) {
            clearTimeout(timerHandleRef);
        }

        populateFieldList(true);
        populateWorkflowStepList(true);
        populateTypeTemplateList(true);
            
        $('#pnlApplicationTypeDetails').show();
    
        scrollToId('pnlApplicationTypeDetails');

        $('#AjaxLoader').hide();
    }
}

function populateFieldList(includeEditor) {

    $('#lstFields').html('');

    fieldList.sort(sortByOrder);

    $.each(fieldList, function () {

        if (!this.IsRemoved) {
            if (this.Children == null || this.Children.length == 0) {
                $('#lstFields').append('<li><i data-feather="file"></i> ' + this.Name + ' (' + this.FieldTypeName + ') <a href="#" data-toggle="modal" data-target="#FieldEditModal" data-id="' + this.FieldId + '">Edit</a></li>');
            }
            else {
                var item = '';
                item += '<li>';
                item += '<i data-feather="folder"></i> ' + this.Name + ' (' + this.FieldTypeName + ') <a href="#" data-toggle="modal" data-target="#FieldEditModal" data-id="' + this.FieldId + '">Edit</a>';
                item += '<ul style="list-style: none; padding-left: 30px;">';

                this.Children.sort(sortByOrder);

                $.each(this.Children, function () {
                    if (!this.IsRemoved) {
                        item += '<li><i data-feather="play"></i> ' + this.Name + ' (' + this.FieldTypeName + ') <a href="#" data-toggle="modal" data-target="#FieldEditModal" data-id="' + this.FieldId + '">Edit</a></li>';
                    }
                });

                item += '</ul>';
                item += '</li>';

                $('#lstFields').append(item);
            }
        }
    });

    if (includeEditor) {
        populateFieldListEditor();
    }

    initializeIcons();

    initializeChangeDetection();
}

function populateFieldListEditor(fieldId) {

    var tableId = 'tblFieldList';
    var fieldPrefix = '';
    var filteredFieldList = fieldList;

    var changeDetectionClass = 'FieldListChangeDetection';

    // If an ID is passed, this will render the field list editor for the child fields
    if (fieldId) {
        tableId = 'tblMultipleFieldList';
        fieldPrefix = 'MultipleFieldList_';

        changeDetectionClass = 'FieldModalChangeDetection';

        var field = getFieldById(fieldId);

        if (field != null) {
            filteredFieldList = field.Children;

            $('#btnCreateNewFieldChild').data('parentfieldid', fieldId);
        }
        else {
            filteredFieldList = null;
        }
    }

    if (filteredFieldList != null) {
        $('#' + tableId + ' tbody').html('');

        filteredFieldList.sort(sortByOrder);

        $.each(filteredFieldList, function () {

            if (!this.IsRemoved) {
                var checkedString = 'checked="checked"';
                if (!this.RequiredInd) {
                    checkedString = '';
                }

                $('#' + tableId + ' tbody').append('<tr>'
                    + '<td>'
                        + '<input type="text" class="form-control form-control-sm ' + changeDetectionClass + '" id="' + fieldPrefix + 'FieldName_' + this.FieldId + '" name="' + fieldPrefix + 'FieldName_' + this.FieldId + '" data-id="' + this.FieldId + '" value="' + this.Name + '" />'
                    + '</td>'
                    + '<td>'
                        + '<select id="' + fieldPrefix + 'FieldType_' + this.FieldId + '" name="' + fieldPrefix + 'FieldType_' + this.FieldId + '" data-id="' + this.FieldId + '" class="form-control form-control-sm ' + changeDetectionClass + '">'
                        + '</select>'
                    + '</td>'
                    + '<td>'
                        + '<div class="form-check">'
                            + '<input class="form-check-input position-static ' + changeDetectionClass + '" type="checkbox" id="' + fieldPrefix + 'FieldRequired_' + this.FieldId + '" name="' + fieldPrefix + 'FieldRequired_' + this.FieldId + '" data-id="' + this.FieldId + '" value="1" ' + checkedString + ' />'
                        + '</div>'
                    + '</td>'
                    + '<td>'
                        + '<a class="FieldEditLink" id="' + fieldPrefix + 'FieldEdit_' + this.FieldId + '" name="' + fieldPrefix + 'FieldEdit_' + this.FieldId + '" data-id="' + this.FieldId + '" href="#">Edit</a>'
                    + '</td>'
                    + '<td>'
                        + '<a href="#" class="' + fieldPrefix + 'FieldRemoveLink" id="' + fieldPrefix + 'FieldRemove_' + this.FieldId + '" name="' + fieldPrefix + 'FieldRemove_' + this.FieldId + '" data-id="' + this.FieldId + '">Remove</a>'
                    + '</td>'
                + '</tr>');

                populateDropdown(fieldPrefix + 'FieldType_' + this.FieldId, 'FieldTypes', this.FieldTypeCode, this);
            }
        });

        initializeChangeDetection();

        if (fieldId) {
            $('#' + tableId + ' tbody').sortable({
                helper: fixHelper,
                stop: function (event, ui) {
                    saveAndReloadFieldModal();
                }
            }).disableSelection();
        }
        else {
            $('#' + tableId + ' tbody').sortable({
                helper: fixHelper,
                stop: function (event, ui) {
                    saveAndReloadFieldList();
                }
            }).disableSelection();
        }

        initializeFieldEditLinks();
    }
}

function updateFieldListIds(fieldId) {

    var tableId = 'tblFieldList';
    var fieldPrefix = '';
    var filteredFieldList = fieldList;

    if (fieldId) {
        tableId = 'tblMultipleFieldList';
        fieldPrefix = 'MultipleFieldList_';

        var field = getFieldById(fieldId);

        if (field) {
            filteredFieldList = field.Children;
        }
        else {
            filteredFieldList = null;
        }
    }

    if (filteredFieldList != null) {
        filteredFieldList.sort(sortByOrder);

        $.each(filteredFieldList, function () {

            if (!this.IsRemoved) {

                var field = this;

                $('#' + tableId + ' tbody [id^="' + fieldPrefix + 'FieldName_"]').eq(field.Sort - 1).each(function () {
                    $(this).attr('id', fieldPrefix + 'FieldName_' + field.FieldId);
                    $(this).attr('name', fieldPrefix + 'FieldName_' + field.FieldId);
                    $(this).attr('data-id', field.FieldId);
                });

                $('#' + tableId + ' tbody [id^="' + fieldPrefix + 'FieldType_"]').eq(field.Sort - 1).each(function () {
                    $(this).attr('id', fieldPrefix + 'FieldType_' + field.FieldId);
                    $(this).attr('name', fieldPrefix + 'FieldType_' + field.FieldId);
                    $(this).attr('data-id', field.FieldId);
                });

                $('#' + tableId + ' tbody [id^="' + fieldPrefix + 'FieldRequired_"]').eq(field.Sort - 1).each(function () {
                    $(this).attr('id', fieldPrefix + 'FieldRequired_' + field.FieldId);
                    $(this).attr('name', fieldPrefix + 'FieldRequired_' + field.FieldId);
                    $(this).attr('data-id', field.FieldId);
                });

                $('#' + tableId + ' tbody [id^="' + fieldPrefix + 'FieldEdit_"]').eq(field.Sort - 1).each(function () {
                    $(this).attr('id', fieldPrefix + 'FieldEdit_' + field.FieldId);
                    $(this).attr('name', fieldPrefix + 'FieldEdit_' + field.FieldId);
                    $(this).attr('data-id', field.FieldId);
                });

                $('#' + tableId + ' tbody [id^="' + fieldPrefix + 'FieldRemove_"]').eq(field.Sort - 1).each(function () {
                    $(this).attr('id', fieldPrefix + 'FieldRemove_' + field.FieldId);
                    $(this).attr('name', fieldPrefix + 'FieldRemove_' + field.FieldId);
                    $(this).attr('data-id', field.FieldId);
                });
            }
        });
    }
}

function populateAppTypeList() {

    $('#tblAppTypeList tbody').html('');

    appTypeList.sort(sortByOrder);

    $.each(appTypeList, function () {

        var activeRowClass = ' class="table-success"';

        if (this.TypeId != currentTypeId) {
            activeRowClass = '';
        }

        if (!this.IsRemoved) {
            $('#tblAppTypeList tbody').append('<tr' + activeRowClass + '>'
                + '<td>'
                    + '<a href="#" class="AppTypeLink" id="AppType_' + this.TypeId + '" data-id="' + this.TypeId + '">' + this.Name + '</a>'
                + '</td>'
                + '<td>'
                    + '<a href="#" class="AppTypeRemoveLink" id="TypeRemove_' + this.TypeId + '" name="TypeRemove_' + this.TypeId + '" data-id="' + this.TypeId + '">Remove</a>'
                + '</td>'
            + '</tr>');
        }
    });

    $('#tblAppTypeList tbody').sortable({
        helper: fixHelper,
        stop: function (event, ui) {
            saveAndReloadAppTypeList();
        }
    }).disableSelection();

    initializeChangeDetection();
}

function populateWorkflowStepList(includeEditor) {

    $('#lstWorkflowStepList').html('');

    if (includeEditor) {
        $('#tblWorkflowStepList tbody').html('');
    }

    workflowStepList.sort(sortByOrder);

    $.each(workflowStepList, function () {

        if (!this.IsRemoved) {
            $('#lstWorkflowStepList').append('<li><i data-feather="check-square"></i> ' + this.Name + ' (' + this.AssignmentTypeName + ') <a href="#" data-toggle="modal" data-target="#WorkflowStepEditModal" data-id="' + this.WorkflowStepId + '">Edit</a></li>');

            if (includeEditor) {
                $('#tblWorkflowStepList tbody').append('<tr>'
                    + '<td>'
                        + '<input type="text" class="form-control form-control-sm WorkflowStepListChangeDetection" id="WorkflowStepName_' + this.WorkflowStepId + '" name="WorkflowStepName_' + this.WorkflowStepId + '" data-id="' + this.WorkflowStepId + '" value="' + this.Name + '" />'
                    + '</td>'
                    + '<td>'
                        + '<select class="form-control form-control-sm WorkflowStepListChangeDetection" id="WorkflowStepType_' + this.WorkflowStepId + '" name="WorkflowStepType_' + this.WorkflowStepId + '" data-id="' + this.WorkflowStepId + '">'
                        + '</select>'
                    + '</td>'
                    + '<td>'
                        + '<a id="WorkflowStepEdit_' + this.WorkflowStepId + '" name="WorkflowStepEdit_' + this.WorkflowStepId + '" data-toggle="modal" data-target="#WorkflowStepEditModal" data-id="' + this.WorkflowStepId + '" href="#">Edit</a>'
                    + '</td>'
                    + '<td>'
                        + '<a href="#" class="WorkflowStepRemoveLink" id="WorkflowStepRemove_' + this.WorkflowStepId + '" name="WorkflowStepRemove_' + this.WorkflowStepId + '" data-id="' + this.WorkflowStepId + '">Remove</a>'
                    + '</td>'
                + '</tr>');

                populateDropdown('WorkflowStepType_' + this.WorkflowStepId, 'AssignmentTypes', this.AssignmentTypeCode);
            }
        }
    });

    $('#tblWorkflowStepList tbody').sortable({
        helper: fixHelper,
        stop: function (event, ui) {
            saveAndReloadWorkflowStepList();
        }
    }).disableSelection();

    $('#btnCreateNewWorkflowStep').off('click');
    $('#btnCreateNewWorkflowStep').on('click', function (event) {
        addWorkflowStep();

        updateWorkflowStepListFromPage();
        saveWorkflowStepList(true);
    });

    $('.WorkflowStepRemoveLink').off('click');
    $('.WorkflowStepRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this workflow step?')) {
            var link = $(event.currentTarget);
            var workflowStepId = link.data('id');

            removeWorkflowStepById(workflowStepId);

            populateWorkflowStepList(true);

            saveAndReloadWorkflowStepList();
        }
    });

    initializeIcons();

    initializeChangeDetection();
}

function updateWorkflowStepListIds() {

    workflowStepList.sort(sortByOrder);
        
    $.each(workflowStepList, function () {

        if (!this.IsRemoved) {

            var workflowStep = this;

            $('#tblWorkflowStepList tbody [id^="' + 'WorkflowStepName_"]').eq(workflowStep.Sort - 1).each(function () {
                $(this).attr('id', 'WorkflowStepName_' + workflowStep.WorkflowStepId);
                $(this).attr('name', 'WorkflowStepName_' + workflowStep.WorkflowStepId);
                $(this).attr('data-id', workflowStep.WorkflowStepId);
            });

            $('#tblWorkflowStepList tbody [id^="' + 'WorkflowStepType_"]').eq(workflowStep.Sort - 1).each(function () {
                $(this).attr('id', 'WorkflowStepType_' + workflowStep.WorkflowStepId);
                $(this).attr('name', 'WorkflowStepType_' + workflowStep.WorkflowStepId);
                $(this).attr('data-id', workflowStep.WorkflowStepId);
            });

            $('#tblWorkflowStepList tbody [id^="' + 'WorkflowStepEdit_"]').eq(workflowStep.Sort - 1).each(function () {
                $(this).attr('id', 'WorkflowStepEdit_' + workflowStep.WorkflowStepId);
                $(this).attr('name', 'WorkflowStepEdit_' + workflowStep.WorkflowStepId);
                $(this).attr('data-id', workflowStep.WorkflowStepId);
            });

            $('#tblWorkflowStepList tbody [id^="' + 'WorkflowStepRemove_"]').eq(workflowStep.Sort - 1).each(function () {
                $(this).attr('id', 'WorkflowStepRemove_' + workflowStep.WorkflowStepId);
                $(this).attr('name', 'WorkflowStepRemove_' + workflowStep.WorkflowStepId);
                $(this).attr('data-id', workflowStep.WorkflowStepId);
            });
        }
    });    
}

function populateTypeTemplateList(includeEditor) {

    $('#lstTypeTemplateList').html('');

    if (includeEditor) {
        $('#tblTypeTemplateList tbody').html('');
    }

    typeTemplateList.sort(sortByOrder);

    $.each(typeTemplateList, function () {

        if (!this.IsRemoved) {
            $('#lstTypeTemplateList').append('<li><i data-feather="check-square"></i> ' + this.Name + ' (' + this.TypeTemplateTypeName + ') <a href="#" data-toggle="modal" data-target="#TypeTemplateEditModal" data-id="' + this.TypeTemplateId + '">Edit</a></li>');
                                
            if (includeEditor) {
                $('#tblTypeTemplateList tbody').append('<tr>'
                    + '<td>'
                        + '<input type="text" class="form-control form-control-sm TypeTemplateListChangeDetection" id="TypeTemplateName_' + this.TypeTemplateId + '" name="TypeTemplateName_' + this.TypeTemplateId + '" data-id="' + this.TypeTemplateId + '" value="' + this.Name + '" />'
                    + '</td>'
                    + '<td>'
                        + '<select class="form-control form-control-sm TypeTemplateListChangeDetection" id="TypeTemplateType_' + this.TypeTemplateId + '" name="TypeTemplateType_' + this.TypeTemplateId + '" data-id="' + this.TypeTemplateId + '">'
                        + '</select>'
                    + '</td>'
                    + '<td>'
                        + '<a id="TypeTemplateEdit_' + this.TypeTemplateId + '" name="TypeTemplateEdit_' + this.TypeTemplateId + '" data-toggle="modal" data-target="#TypeTemplateEditModal" data-id="' + this.TypeTemplateId + '" href="#">Edit</a>'
                    + '</td>'
                    + '<td>'
                        + '<a href="#" class="TypeTemplateRemoveLink" id="TypeTemplateRemove_' + this.TypeTemplateId + '" name="TypeTemplateRemove_' + this.TypeTemplateId + '" data-id="' + this.TypeTemplateId + '">Remove</a>'
                    + '</td>'
                + '</tr>');

                populateDropdown('TypeTemplateType_' + this.TypeTemplateId, 'TypeTemplateTypes', this.TypeTemplateTypeCode);
            }
        }
    });

    $('#tblTypeTemplateList tbody').sortable({
        helper: fixHelper,
        stop: function (event, ui) {
            saveAndReloadTypeTemplateList();
        }
    }).disableSelection();

    $('#btnCreateNewTypeTemplate').off('click');
    $('#btnCreateNewTypeTemplate').on('click', function (event) {
        addTypeTemplate();

        updateTypeTemplateListFromPage();
        saveTypeTemplateList(true);
    });

    $('.TypeTemplateRemoveLink').off('click');
    $('.TypeTemplateRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this template?')) {
            var link = $(event.currentTarget);
            var typeTemplateId = link.data('id');

            removeTypeTemplateById(typeTemplateId);

            populateTypeTemplateList(true);

            saveAndReloadTypeTemplateList();
        }
    });

    initializeIcons();

    initializeChangeDetection();
}

function updateTypeTemplateListIds() {

    typeTemplateList.sort(sortByOrder);

    $.each(typeTemplateList, function () {

        if (!this.IsRemoved) {

            var typeTemplate = this;

            $('#tblTypeTemplateList tbody [id^="' + 'TypeTemplateName_"]').eq(typeTemplate.Sort - 1).each(function () {
                $(this).attr('id', 'TypeTemplateName_' + typeTemplate.TypeTemplateId);
                $(this).attr('name', 'TypeTemplateName_' + typeTemplate.TypeTemplateId);
                $(this).attr('data-id', typeTemplate.TypeTemplateId);
            });

            $('#tblTypeTemplateList tbody [id^="' + 'TypeTemplateType_"]').eq(typeTemplate.Sort - 1).each(function () {
                $(this).attr('id', 'TypeTemplateType_' + typeTemplate.TypeTemplateId);
                $(this).attr('name', 'TypeTemplateType_' + typeTemplate.TypeTemplateId);
                $(this).attr('data-id', typeTemplate.TypeTemplateId);
            });

            $('#tblTypeTemplateList tbody [id^="' + 'TypeTemplateEdit_"]').eq(typeTemplate.Sort - 1).each(function () {
                $(this).attr('id', 'TypeTemplateEdit_' + typeTemplate.TypeTemplateId);
                $(this).attr('name', 'TypeTemplateEdit_' + typeTemplate.TypeTemplateId);
                $(this).attr('data-id', typeTemplate.TypeTemplateId);
            });

            $('#tblTypeTemplateList tbody [id^="' + 'TypeTemplateRemove_"]').eq(typeTemplate.Sort - 1).each(function () {
                $(this).attr('id', 'TypeTemplateRemove_' + typeTemplate.TypeTemplateId);
                $(this).attr('name', 'TypeTemplateRemove_' + typeTemplate.TypeTemplateId);
                $(this).attr('data-id', typeTemplate.TypeTemplateId);
            });
        }
    });
}

function initializeFieldEditLinks() {

    $('#btnCreateNewField').off('click');
    $('#btnCreateNewField').on('click', function (event) {
        addField();

        updateFieldListFromPage();
        saveFieldList(true);
    });

    $('#btnCreateNewFieldChild').off('click');
    $('#btnCreateNewFieldChild').on('click', function (event) {
        var btn = $(event.currentTarget);
        var parentFieldId = btn.data('parentfieldid');
        addField(parentFieldId);

        updateFieldFromModal();
        saveFieldList(true);
    });

    $('.FieldEditLink').off('click');
    $('.FieldEditLink').on('click', function (event) {
        event.preventDefault();

        var link = $(event.currentTarget);
        var fieldId = link.data('id');
        currentFieldId = fieldId;

        if ($('#FieldEditModal:visible').length) {

            populateFieldEditModal($('#FieldEditModal'), fieldId)
        }
        else {
            $('#FieldEditModal').modal();
        }
    });

    $('.FieldRemoveLink').off('click');
    $('.FieldRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this field?')) {
            var link = $(event.currentTarget);
            var fieldId = link.data('id');

            removeFieldById(fieldId);

            populateFieldList(true);

            saveAndReloadFieldList();
        }
    });

    $('.MultipleFieldList_FieldRemoveLink').off('click');
    $('.MultipleFieldList_FieldRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this field?')) {
            var link = $(event.currentTarget);
            var fieldId = link.data('id');

            var field = getFieldById(fieldId);

            removeFieldById(fieldId);

            populateFieldListEditor(field.ParentFieldId);

            saveAndReloadFieldModal();

            populateFieldList(true);
        }
    });
}

function initializeAppTypeLinks() {

    populateAppTypeList();

    $('#btnCreateNewAppType').off('click');
    $('#btnCreateNewAppType').on('click', function (event) {
        addAppType();
    });

    $('.AppTypeLink').off('click');
    $('.AppTypeLink').on('click', function (event) {
        event.preventDefault();

        if (timerHandleRef) {
            clearTimeout(timerHandleRef);
        }

        var link = $(event.currentTarget);
        var typeId = link.data('id');
        currentTypeId = typeId;

        var appType = getAppTypeById(typeId);

        $('.AppTypeLabel').html(appType.Name);
        $('#AppTypeName').val(appType.Name);

        dropdownItemList = {};
        dropdownCount = 0;

        getDropdownItemList('FieldTypes');
        getDropdownItemList('AssignmentTypes');
        getDropdownItemList('TypeTemplateTypes');

        fieldList = null;
        workflowStepList = null;
        typeTemplateList = null;
            
        $('#pnlApplicationTypeDetails').hide();

        getFieldList(typeId);
        getWorkflowStepList(typeId);
        getTypeTemplateList(typeId);

        timerHandleRef = setInterval(populateApplicationType, 100);

        populateAppTypeList();
        initializeAppTypeLinks();

        $('#AjaxLoader').show();
    });

    $('.AppTypeRemoveLink').off('click');
    $('.AppTypeRemoveLink').on('click', function (event) {
        event.preventDefault();

        if (confirm('Are you sure you want to remove this application type?')) {
            var link = $(event.currentTarget);
            var typeId = link.data('id');

            removeAppTypeById(typeId);

            if (typeId == currentTypeId) {
                dropdownItemList = {};
                dropdownCount = 0;

                getDropdownItemList('FieldTypes');
                getDropdownItemList('AssignmentTypes');
                getDropdownItemList('TypeTemplateTypes');

                fieldList = null;
                workflowStepList = null;
                typeTemplateList = null;

                currentTypeId = 0;

                $('#pnlApplicationTypeDetails').hide();
            }

            initializeAppTypeLinks();

            saveAndReloadAppTypeList();
        }
    });

    initializeChangeDetection();
}

function saveAndReloadAllData() {

    saveCounter = 0;

    if (timerHandleRef) {
        clearTimeout(timerHandleRef);
    }

    saveAppTypeList();

    if (currentTypeId) {
        saveFieldList();
        saveWorkflowStepList();
        saveTypeTemplateList();
    }

    timerHandleRef = setInterval(reloadDataAndRepopulate, 100);
}

function saveAppTypeList(reload) {

    saveCounter++;

    if (reload) {
        $('#AjaxLoader').show();
    }

    $.ajax({
        url: 'SaveAppTypeListAjax',
        type: 'POST',
        data: {
            appTypeList: appTypeList
        },
        success: function (response) {

            saveCounter--;

            appTypeList = response.types;

            if (reload) {                    
                initializeAppTypeLinks();
                $('#AjaxLoader').hide();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function saveFieldList(reload) {

    saveCounter++;

    if (reload) {
        $('#AjaxLoader').show();
    }

    $.ajax({
        url: 'SaveFieldListAjax',
        type: 'POST',
        data: {
            typeId: currentTypeId,
            fieldList: fieldList
        },
        success: function (response) {

            saveCounter--;

            fieldList = response.fields;

            if (reload) {                    
                populateFieldList(true);
                populateFieldListEditor(currentFieldId);
                $('#AjaxLoader').hide();
            }
            else {
                populateFieldList();
                updateFieldListIds();
                updateFieldListIds(currentFieldId);
                updateSelectionListIds(currentFieldId);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function saveWorkflowStepList(reload) {

    saveCounter++;

    if (reload) {
        $('#AjaxLoader').show();
    }

    $.ajax({
        url: 'SaveWorkflowStepListAjax',
        type: 'POST',
        data: {
            typeId: currentTypeId,
            workflowStepList: workflowStepList
        },
        success: function (response) {

            saveCounter--;

            workflowStepList = response.workflowSteps;

            if (reload) {                    
                populateWorkflowStepList(true);
                $('#AjaxLoader').hide();
            }
            else {
                populateWorkflowStepList();
                updateWorkflowStepListIds();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function saveTypeTemplateList(reload) {

    saveCounter++;

    if (reload) {
        $('#AjaxLoader').show();
    }

    $.ajax({
        url: 'SaveTypeTemplateListAjax',
        type: 'POST',
        data: {
            typeId: currentTypeId,
            typeTemplateList: typeTemplateList
        },
        success: function (response) {

            saveCounter--;

            typeTemplateList = response.typeTemplates;

            if (reload) {                    
                populateTypeTemplateList(true);
                $('#AjaxLoader').hide();
            }
            else {
                populateTypeTemplateList();
                updateTypeTemplateListIds();
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function reloadDataAndRepopulate() {

    if (saveCounter == 0) {

        if (timerHandleRef) {
            clearTimeout(timerHandleRef);
        }

        populateAppTypeList();
        initializeAppTypeLinks();

        if (currentTypeId) {

            var typeId = currentTypeId;

            var link = $(event.currentTarget);
            var typeId = link.data('id');
            currentTypeId = typeId;

            var appType = getAppTypeById(typeId);

            $('.AppTypeLabel').html(appType.Name);
            $('#AppTypeName').val(appType.Name);

            dropdownItemList = {};
            dropdownCount = 0;

            getDropdownItemList('FieldTypes');
            getDropdownItemList('AssignmentTypes');
            getDropdownItemList('TypeTemplateTypes');

            fieldList = null;
            workflowStepList = null;
            typeTemplateList = null;

            getFieldList(typeId);
            getWorkflowStepList(typeId);
            getTypeTemplateList(typeId);

            timerHandleRef = setInterval(populateApplicationType, 100);
        }
    }
}

function saveAndReloadAppTypeList() {
    updateAppTypeListFromPage();
    saveAppTypeList(true);
}

function saveAndReloadFieldList() {
    updateFieldListFromPage();
    saveFieldList();        
}

function saveAndReloadWorkflowStepList() {
    updateWorkflowStepListFromPage();
    saveWorkflowStepList();
}

function saveAndReloadTypeTemplateList() {
    updateTypeTemplateListFromPage();
    saveTypeTemplateList();
}

function saveAndReloadFieldModal() {
    updateFieldFromModal();
    saveFieldList();
}

function saveAndReloadListItemsModal() {
    updateListItemsFromModal();
    saveFieldList();
}

function saveAndReloadWorkflowStepModal() {
    updateWorkflowStepFromModal();
    saveWorkflowStepList();
}

function saveAndReloadTypeTemplateModal() {
    updateTypeTemplateFromModal();
    saveTypeTemplateList();
}

function initializeChangeDetection() {

    $('.AppTypeListChangeDetection').off('change');
    $('.AppTypeListChangeDetection').on('change', function (event) {
        saveAndReloadAppTypeList();
    });

    $('.FieldListChangeDetection').off('change');
    $('.FieldListChangeDetection').on('change', function (event) {
        saveAndReloadFieldList();
    });

    $('.WorkflowStepListChangeDetection').off('change');
    $('.WorkflowStepListChangeDetection').on('change', function (event) {
        saveAndReloadWorkflowStepList();
    });
        
    $('.TypeTemplateListChangeDetection').off('change');
    $('.TypeTemplateListChangeDetection').on('change', function (event) {
        saveAndReloadTypeTemplateList();
    });
        
    $('.FieldModalChangeDetection').off('change');
    $('.FieldModalChangeDetection').on('change', function (event) {
        saveAndReloadFieldModal();
    });

    $('.ListItemsModalChangeDetection').off('change');
    $('.ListItemsModalChangeDetection').on('change', function (event) {
        saveAndReloadListItemsModal();
    });

    $('.WorkflowStepModalChangeDetection').off('change');
    $('.WorkflowStepModalChangeDetection').on('change', function (event) {
        saveAndReloadWorkflowStepModal();
    });
        
    $('.TypeTemplateModalChangeDetection').off('change');
    $('.TypeTemplateModalChangeDetection').on('change', function (event) {
        saveAndReloadTypeTemplateModal();
    });

    $('#FieldType').off('change');
    $('#FieldType').on('change', function (event) {
        updateFieldFromModal();
        populateFieldEditModal($('#FieldEditModal'), currentFieldId);
        saveFieldList();
    });
}

function getAppTypeList() {

    $('#AjaxLoader').show();

    $.ajax({
        url: 'GetAppTypeListAjax',
        type: 'POST',
        data: {

        },
        success: function (response) {

            appTypeList = response.types;

            initializeAppTypeLinks();

            $('#AjaxLoader').hide();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function getWorkflowStepList(typeId) {

    $.ajax({
        url: 'GetWorkflowStepListAjax',
        type: 'POST',
        data: {
            typeId: typeId
        },
        success: function (response) {

            workflowStepList = response.workflowSteps
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function getTypeTemplateList(typeId) {

    $.ajax({
        url: 'GetTypeTemplateListAjax',
        type: 'POST',
        data: {
            typeId: typeId
        },
        success: function (response) {

            typeTemplateList = response.typeTemplates;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

function getFieldList(typeId) {

    $.ajax({
        url: 'GetFieldListAjax',
        type: 'POST',
        data: {
            typeId: typeId
        },
        success: function (response) {

            fieldList = response.fields;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
        }
    });
}

getAppTypeList();
