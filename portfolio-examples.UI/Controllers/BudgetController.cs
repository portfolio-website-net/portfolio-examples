﻿// <copyright file="BudgetController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Web.Mvc;
    using System.Web.SessionState;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class BudgetController : BaseController
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult JQueryVersion()
        {
            return this.View();
        }
    }
}