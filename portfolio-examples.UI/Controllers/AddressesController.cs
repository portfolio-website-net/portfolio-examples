﻿// <copyright file="AddressesController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System;
    using System.Web.Mvc;
    using Business.Helpers;
    using Business.Helpers.Enums;
    using Business.Interfaces;
    using Business.Providers;
    using Business.ViewModels;
    using global::TinyIoC;

    public class AddressesController : BaseController
    {
        private IAddresses provider;

        public AddressesController(IAddresses dependency)
        {
            this.provider = dependency;
        }

        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult Edit(int? id, string databaseType)
        {
            var viewModel = new AddressViewModel();

            if (id.HasValue)
            {
                this.AssignProvider(databaseType);

                viewModel = this.provider.Hydrate(id.Value);
            }

            return this.ProcessResponse(viewModel);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditSave(int? id, string databaseType, AddressViewModel viewModel)
        {
            this.AssignProvider(databaseType);

            if (id.HasValue && viewModel.RemoveRecord)
            {
                this.provider.Remove(id.Value);
            }
            else
            {
                viewModel.AddressID = id.HasValue ? id.Value : 0;
                id = this.provider.Dehydrate(viewModel);
            }

            viewModel.ResponseType = ResponseType.ChangesSaved;

            return this.ProcessResponse(viewModel);
        }

        public ActionResult IndexAjax(string databaseType)
        {
            var viewModel = new AddressCollectionViewModel
            {
                DrawValue = int.Parse(this.Request["draw"]),
                StartValue = int.Parse(this.Request["start"]),
                LengthValue = int.Parse(this.Request["length"]),
                SearchValue = this.Request["search[value]"],
                OrderColumnValue = int.Parse(this.Request["order[0][column]"]),
                OrderDirectionValue = this.Request["order[0][dir]"],
                OrderColumnName = this.Request["columns[" + this.Request["order[0][column]"] + "][data]"]
            };

            this.AssignProvider(databaseType);

            var resultCollection = AddressesHelper.GetGridResults(viewModel, this.provider);

            return this.Json(new
            {
                draw = resultCollection.DrawValue,
                recordsTotal = resultCollection.RecordsTotal,
                recordsFiltered = resultCollection.RecordsFiltered,
                data = resultCollection.DataResult
            });
        }

        public ActionResult ChangesSaved()
        {
            return this.View();
        }

        private void AssignProvider(string databaseType)
        {
            DatabaseType databaseTypeVal;

            if (string.IsNullOrEmpty(databaseType)
                || !Enum.TryParse(databaseType, out databaseTypeVal))
            {
                databaseTypeVal = DatabaseType.SQLServer;
            }

            if (databaseTypeVal == DatabaseType.SQLServer)
            {
                this.provider = TinyIoCContainer.Current.Resolve<SQLServerAddresses>();
            }
            else if (databaseTypeVal == DatabaseType.MySQL)
            {
                this.provider = TinyIoCContainer.Current.Resolve<MySQLAddresses>();
            }
        }
    }
}