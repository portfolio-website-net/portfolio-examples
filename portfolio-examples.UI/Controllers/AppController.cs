﻿// <copyright file="AppController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Business.Helpers.App;
    using Business.Interfaces;
    using Data;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class AppController : BaseController
    {
        private readonly IApp provider;

        public AppController(IApp dependency)
        {
            this.provider = dependency;
        }

        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult ApplicationTypes()
        {
            return this.View();
        }

        public ActionResult Applications(int? agreementId)
        {
            if (!agreementId.HasValue)
            {
                using (var context = new Entities())
                {
                    var firstAgreement = context.ApplicationsGridViews.FirstOrDefault();
                    if (firstAgreement != null)
                    {
                        agreementId = firstAgreement.AgreementId;
                    }
                }
            }

            this.ViewBag.AgreementId = agreementId;

            return this.View();
        }

        public ActionResult ApplicationsGridAjax()
        {
            var drawValue = int.Parse(this.Request["draw"]);
            var startValue = int.Parse(this.Request["start"]);
            var lengthValue = int.Parse(this.Request["length"]);
            var searchValue = this.Request["search[value]"];
            var orderColumnValue = int.Parse(this.Request["order[0][column]"]);
            var orderDirectionValue = this.Request["order[0][dir]"];
            var orderColumnName = this.Request["columns[" + orderColumnValue + "][data]"];

            return this.Json(this.provider.ApplicationsGrid(drawValue, startValue, lengthValue, searchValue, orderColumnValue, orderDirectionValue, orderColumnName));
        }

        public ActionResult GetAllDropdownItemListsAjax()
        {
            return this.Json(new
            {
                FieldTypes = this.provider.GetDropdownItemList("FieldTypes"),
                AssignmentTypes = this.provider.GetDropdownItemList("AssignmentTypes"),
                TypeTemplateTypes = this.provider.GetDropdownItemList("TypeTemplateTypes"),
            });
        }

        public ActionResult GetAppTypeListAjax()
        {
            return this.Json(this.provider.GetAppTypeList());
        }

        public ActionResult SaveAppTypeListAjax(List<AppTypeHelper> appTypeList)
        {
            return this.Json(this.provider.SaveAppTypeList(appTypeList));
        }

        public ActionResult GetAllAppTypeListItemsAjax(int typeId)
        {
            return this.Json(new
            {
                FieldList = this.provider.GetFieldList(typeId),
                WorkflowStepList = this.provider.GetWorkflowStepList(typeId),
                TypeTemplateList = this.provider.GetTypeTemplateList(typeId),
            });
        }

        [ValidateInput(false)]
        public ActionResult SaveFieldListAjax(int typeId, Guid? guid, List<FieldHelper> fieldList)
        {
            return this.Json(this.provider.SaveFieldList(typeId, guid, fieldList));
        }

        [ValidateInput(false)]
        public ActionResult SaveWorkflowStepListAjax(int typeId, Guid? guid, List<WorkflowStepHelper> workflowStepList)
        {
            return this.Json(this.provider.SaveWorkflowStepList(typeId, guid, workflowStepList));
        }

        [ValidateInput(false)]
        public ActionResult SaveTypeTemplateListAjax(int typeId, Guid? guid, List<TypeTemplateHelper> typeTemplateList)
        {
            return this.Json(this.provider.SaveTypeTemplateList(typeId, guid, typeTemplateList));
        }

        public ActionResult GetAgreementTemplateListAjax(int agreementId)
        {
            return this.Json(this.provider.GetAgreementTemplateList(agreementId));
        }

        public ActionResult SaveFieldValueListAjax(int agreementId, List<AgreementFieldValueHelper> fieldValueList)
        {
            return this.Json(this.provider.SaveFieldValueList(agreementId, fieldValueList));
        }

        public ActionResult IsOnlineAjax()
        {
            return this.Content(string.Empty);
        }
    }
}