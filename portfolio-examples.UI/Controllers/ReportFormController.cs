﻿// <copyright file="ReportFormController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System;
    using System.Drawing;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using EO.Pdf;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class ReportFormController : BaseController
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public void GeneratePdf(string formValues)
        {
            var url = "https://portfolio-examples.Portfolio Website/ReportForm?print=1&formValues=" + HttpUtility.UrlEncode(formValues);
            var document = new PdfDocument();
            document.Info.Title = "ReportForm";
            document.Info.CreationDate = DateTime.Now;
            document.Info.Author = "portfolio-examples";
            var options = new HtmlToPdfOptions();
            options.PageSize = new SizeF(8.5f, 11f);
            options.OutputArea = new RectangleF(0.25f, 0.25f, 8.0f, 10.5f);
            options.ZoomLevel = 1.00f;

            using (var session = HtmlToPdfSession.Create(options))
            {
                session.Options.TriggerMode = HtmlToPdfTriggerMode.Auto;
                session.LoadUrl(url);
                session.RenderAsPDF(document);
            }

            this.Response.Clear();
            this.Response.ClearHeaders();
            this.Response.AddHeader("content-disposition", "inline; filename=\"ReportForm.pdf\"");
            this.Response.ContentType = "application/pdf";

            document.Save(this.Response.OutputStream);
            this.Response.End();
        }
    }
}