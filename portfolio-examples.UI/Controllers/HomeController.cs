﻿// <copyright file="HomeController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Threading;
    using System.Web.Mvc;
    using System.Web.Script.Serialization;
    using Business.Helpers;
    using DocumentFormat.OpenXml.Packaging;
    using Hangfire;
    using Stripe;

    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return this.View();
        }

        public ActionResult RichTextExample()
        {
            return this.View();
        }

        public ActionResult RichTextCleanExample()
        {
            return this.View();
        }

        public ActionResult TextCharactersExample()
        {
            return this.View();
        }

        public ActionResult AjaxTooltipExample()
        {
            return this.View();
        }

        public ActionResult DialogPopupExample()
        {
            return this.View();
        }

        public ActionResult HighlightingSearchExample()
        {
            return this.View();
        }

        public ActionResult OpenXMLExample()
        {
            return this.View();
        }

        public void GenerateDocument(string text)
        {
            string templateFile = this.Server.MapPath("~/Templates/MyTemplate.docx");
            string destinationFile = this.Server.MapPath("~/Templates/" + Guid.NewGuid() + ".docx");

            System.IO.File.Copy(templateFile, destinationFile, true);

            using (var doc = WordprocessingDocument.Open(destinationFile, true))
            {
                DocumentHelper.FillTemplate(doc, text);
            }

            this.Response.Clear();
            this.Response.ClearHeaders();
            this.Response.AddHeader("content-disposition", "inline; filename=\"" + "MyDocument.docx" + "\"");
            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

            try
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(destinationFile);
                this.Response.OutputStream.Write(fileBytes, 0, fileBytes.Length);
            }
            finally
            {
                System.IO.File.Delete(destinationFile);
            }

            this.Response.Flush();
            this.Response.End();
        }

        public ActionResult PdfExample()
        {
            return this.View();
        }

        public void GeneratePdf(string text)
        {
            var document = PdfHelper.GenerateReport(text, this);
            this.Response.Clear();
            this.Response.ClearHeaders();
            this.Response.AddHeader("content-disposition", "inline; filename=\"MyReport.pdf\"");
            this.Response.ContentType = "application/pdf";

            document.Save(this.Response.OutputStream);
            this.Response.End();
        }

        public ActionResult ValidationExample()
        {
            return this.View();
        }

        public ActionResult OfficeToPdfExample()
        {
            return this.View();
        }

        public void GenerateOfficeToPdfExample()
        {
            var documents = new List<string>();
            documents.Add(this.Server.MapPath("~/Templates/MyTemplate.docx"));
            documents.Add(this.Server.MapPath("~/Templates/MySpreadsheet.xlsx"));
            documents.Add(this.Server.MapPath("~/Templates/MyPresentation.pptx"));
            documents.Add(this.Server.MapPath("~/Content/Images/logo.png"));

            var stream = OfficeToPdfHelper.ConvertDocuments(documents);
            if (stream != null)
            {
                this.Response.Clear();
                this.Response.ClearHeaders();
                this.Response.AddHeader("Content-Disposition", "inline; filename=\"" + "MyPdf.pdf" + "\"");
                this.Response.ContentType = "application/pdf";

                StreamHelper.CopyStream(stream, this.Response.OutputStream);
            }
        }

        public ActionResult CloudPdfExample()
        {
            return this.View();
        }

        public ActionResult GenerateCloudPdf(string text)
        {
            string apiKey = "ba553e67-e575-4d92-a7cf-5204ff02c4db";
            string value = CloudPdfHelper.GenerateReport(text, this);

            using (var client = new WebClient())
            {
                // Build the conversion options
                var options = new NameValueCollection();
                options.Add("apikey", apiKey);
                options.Add("value", value);

                // Call the API to convert to a PDF
                var ms = new MemoryStream(client.UploadValues("http://api.html2pdfrocket.com/pdf", options));

                // Make the file a downloadable attachment - comment this out to show it directly inside
                this.HttpContext.Response.AddHeader("content-disposition", "inline; filename=MyCloudPdf.pdf");

                // Return the file as a PDF
                return new FileStreamResult(ms, "application/pdf");
            }
        }

        public ActionResult ActiveServerConnectionExample()
        {
            return this.View();
        }

        public ActionResult StripeExample()
        {
            return this.View();
        }

        public ActionResult StripeChargeExample(string stripeEmail, string stripeToken)
        {
            var customers = new StripeCustomerService();
            var charges = new StripeChargeService();

            var customer = customers.Create(new StripeCustomerCreateOptions
            {
                Email = stripeEmail,
                SourceToken = stripeToken
            });

            var charge = charges.Create(new StripeChargeCreateOptions
            {
                Amount = 500,
                Description = "Sample Charge",
                Currency = "usd",
                CustomerId = customer.Id
            });

            return this.View();
        }

        public ActionResult ClientSideDownloadExample()
        {
            return this.View();
        }

        public ActionResult EmailImageExample()
        {
            return this.View();
        }

        public ActionResult EmailImageSentExample()
        {
            EmailHelper.SendEmail();

            return this.View();
        }

        public ActionResult HangfireExample()
        {
            return this.View();
        }

        public ActionResult GenerateHangfireQueueItem(string text)
        {
            string documentGuid = Guid.NewGuid().ToString();

            // Run a fire-and-forget job
            BackgroundJob.Enqueue(() => this.StartHangfireQueueTask(this.Request.Url.GetLeftPart(UriPartial.Authority), documentGuid, text));

            return this.Content(documentGuid);
        }

        public void StartHangfireQueueTask(string applicationPath, string documentGuid, string text)
        {
            using (var client = new WebClient())
            {
                var parameters = new NameValueCollection();
                parameters.Add("documentGuid", documentGuid);
                parameters.Add("text", text);

                client.UploadValues(applicationPath + "/Home/GeneratePdfViaHangfire", "POST", parameters);
            }
        }

        public void GeneratePdfViaHangfire(string documentGuid, string text)
        {
            string apiKey = "ba553e67-e575-4d92-a7cf-5204ff02c4db";
            string value = CloudPdfHelper.GenerateReport(text, this);

            using (var client = new WebClient())
            {
                // Build the conversion options
                var options = new NameValueCollection();
                options.Add("apikey", apiKey);
                options.Add("value", value);

                // Call the API to convert to a PDF
                var ms = new MemoryStream(client.UploadValues("http://api.html2pdfrocket.com/pdf", options));

                // Wait 8 seconds to simulate a processing request
                Thread.Sleep(8000);

                using (var file = new FileStream(this.Server.MapPath("~/Temp/") + documentGuid + ".pdf", FileMode.Create, FileAccess.Write))
                {
                    ms.CopyTo(file);
                }
            }
        }

        public ActionResult GetHangfireResult(string documentGuid)
        {
            if (System.IO.File.Exists(this.Server.MapPath("~/Temp/") + documentGuid + ".pdf"))
            {
                return this.Content(true.ToString());
            }
            else
            {
                return this.Content(string.Empty);
            }
        }

        public ActionResult ExportToExcelExample()
        {
            return this.View();
        }

        public void GenerateExcelExport(string text)
        {
            var stream = new ExportToExcelHelper().GenerateExcelReport();

            this.Response.Clear();
            this.Response.ClearHeaders();
            this.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=\"MyReport.xlsx\"");
            this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            StreamHelper.CopyStream(stream, this.Response.OutputStream);
        }

        public ActionResult SmmryExample()
        {
            return this.View();
        }

        public ActionResult GetSmmry(string url, int? sentenceCount = 7)
        {
            string apiKey = "A125E96A37";
            string apiUrl = "http://api.smmry.com/&SM_API_KEY=" + apiKey;

            using (var client = new WebClient())
            {
                var json = client.DownloadString(apiUrl
                    + "&SM_LENGTH=" + sentenceCount
                    + "&SM_WITH_BREAK"
                    + "&SM_URL=" + url);

                var parser = new JavaScriptSerializer();
                var model = parser.Deserialize<SmmryJsonHelper>(json);
                model.url = url;

                return this.View(model);
            }
        }
    }
}