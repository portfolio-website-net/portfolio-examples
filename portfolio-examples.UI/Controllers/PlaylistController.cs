﻿// <copyright file="PlaylistController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Business.Interfaces;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class PlaylistController : BaseController
    {
        private readonly IPlaylistExtract provider;

        public PlaylistController(IPlaylistExtract dependency)
        {
            this.provider = dependency;
        }

        public ActionResult Index()
        {
            this.provider.StartPlaylistExtract();

            return this.View();
        }
    }
}