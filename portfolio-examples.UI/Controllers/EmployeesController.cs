﻿// <copyright file="EmployeesController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Business.Interfaces;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public class EmployeesController : BaseController
    {
        private readonly IEmployeesExtract provider;

        public EmployeesController(IEmployeesExtract dependency)
        {
            this.provider = dependency;
        }

        public ActionResult Index(string startDate, string endDate)
        {
            this.provider.StartEmployeeExtract(startDate, endDate);

            return this.View();
        }
    }
}