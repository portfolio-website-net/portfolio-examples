﻿// <copyright file="AjaxExampleController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Net;
    using System.Web.Mvc;
    using Business.Interfaces;

    public class AjaxExampleController : BaseController
    {
        private readonly IAjaxExample provider;

        public AjaxExampleController(IAjaxExample dependency)
        {
            this.provider = dependency;
        }

        public ActionResult TooltipDetailsAjax(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var customer = this.provider.GetCustomer(id);
            if (customer == null)
            {
                return this.HttpNotFound();
            }

            this.ViewBag.CustomerName = customer.FirstName + " " + customer.LastName;

            return this.PartialView();
        }

        public ActionResult SearchHighlightAjax(string search)
        {
            return this.Content(this.provider.GetHighlightedTextValue(search));
        }

        public ActionResult IsOnlineAjax()
        {
            return this.Content(true.ToString());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CleanHtmlMarkupAjax(string text)
        {
            string cleanedHtml = this.provider.CleanHtmlMarkup(text);

            return this.Content(cleanedHtml);
        }
    }
}