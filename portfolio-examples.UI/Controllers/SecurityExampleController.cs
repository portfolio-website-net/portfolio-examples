﻿// <copyright file="SecurityExampleController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Web.Mvc;
    using Business.Helpers;

    public class SecurityExampleController : BaseController
    {
        public ActionResult Index()
        {
            return this.View();
        }

        [ValidateUser(Module = ModuleTypes.MyModule1)]
        public ActionResult MyModule1ReadOnlyAccess()
        {
            return this.View();
        }

        [ValidateUser(Module = ModuleTypes.MyModule1, RequiresFullAccess = true)]
        public ActionResult MyModule1FullAccess()
        {
            return this.View();
        }

        [ValidateUser(Module = ModuleTypes.MyModule1, RequiresAdmin = true)]
        public ActionResult MyModule1AdminAccess()
        {
            return this.View();
        }

        [ValidateUser(Module = ModuleTypes.MyModule1, Module2 = ModuleTypes.MyModule2)]
        public ActionResult MyModule1OrMyModule2ReadOnlyAccess()
        {
            return this.View();
        }
    }
}