﻿// <copyright file="ValidateUserAttribute.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Business.Helpers;

    public class ValidateUserAttribute : AuthorizeAttribute
    {
        public ModuleTypes Module { get; set; }

        public ModuleTypes Module2 { get; set; }

        public bool RequiresFullAccess { get; set; }

        public bool RequiresAdmin { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool authorized = false;

            if (filterContext.RequestContext.HttpContext.Request.QueryString["username"] != null)
            {
                string userName = filterContext.RequestContext.HttpContext.Request.QueryString["username"];

                string key = BaseController.UsersCacheKey;
                var users = (List<User>)HttpRuntime.Cache[key];
                var currentUser = users.FirstOrDefault(x => x.UserName == userName);

                if (currentUser != null)
                {
                    if (this.RequiresAdmin && currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module && x.PermissionType == PermissionTypes.Admin))
                    {
                        authorized = true;
                    }
                    else if (this.RequiresFullAccess &&
                        (currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module && x.PermissionType == PermissionTypes.Admin)
                        || currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module && x.PermissionType == PermissionTypes.FullAccess)))
                    {
                        authorized = true;
                    }
                    else if (!this.RequiresAdmin && !this.RequiresFullAccess && currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module))
                    {
                        authorized = true;
                    }

                    if (!authorized && this.Module2 != ModuleTypes.None)
                    {
                        if (this.RequiresAdmin && currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module2 && x.PermissionType == PermissionTypes.Admin))
                        {
                            authorized = true;
                        }
                        else if (this.RequiresFullAccess &&
                            (currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module2 && x.PermissionType == PermissionTypes.Admin)
                            || currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module2 && x.PermissionType == PermissionTypes.FullAccess)))
                        {
                            authorized = true;
                        }
                        else if (!this.RequiresAdmin && !this.RequiresFullAccess && currentUser.ModulePermissions.Any(x => x.ModuleType == this.Module2))
                        {
                            authorized = true;
                        }
                    }
                }
            }

            if (!authorized)
            {
                this.HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                                                                {
                                                                    { "controller", "Error" },
                                                                    { "action", "Forbidden" }
                                                                });
        }
    }
}