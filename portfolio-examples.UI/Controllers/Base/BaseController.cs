﻿// <copyright file="BaseController.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.UI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using Business.Helpers;
    using Business.Helpers.Enums;
    using Business.ViewModels;

    public class BaseController : Controller
    {
        public const string UsersCacheKey = "UsersCache";

        public BaseController()
        {
            var users = new List<User>();

            // User 1
            var user1 = new User
            {
                UserName = "bsmith",
                FirstName = "Bob",
                LastName = "Smith",
                ModulePermissions = new List<ModulePermission>()
            };

            user1.ModulePermissions.Add(new ModulePermission
            {
                ModuleType = ModuleTypes.MyModule1,
                PermissionType = PermissionTypes.FullAccess
            });

            // User 2
            var user2 = new User
            {
                UserName = "kmiller",
                FirstName = "Kate",
                LastName = "Miller",
                ModulePermissions = new List<ModulePermission>()
            };

            user2.ModulePermissions.Add(new ModulePermission
            {
                ModuleType = ModuleTypes.MyModule1,
                PermissionType = PermissionTypes.Admin
            });

            // User 3
            var user3 = new User
            {
                UserName = "hwilson",
                FirstName = "Heather",
                LastName = "Wilson",
                ModulePermissions = new List<ModulePermission>()
            };

            user3.ModulePermissions.Add(new ModulePermission
            {
                ModuleType = ModuleTypes.MyModule1,
                PermissionType = PermissionTypes.ReadOnly
            });

            user3.ModulePermissions.Add(new ModulePermission
            {
                ModuleType = ModuleTypes.MyModule2,
                PermissionType = PermissionTypes.Admin
            });

            users.Add(user1);
            users.Add(user2);
            users.Add(user3);

            HttpRuntime.Cache.Insert(UsersCacheKey, users, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20));
        }

        public ActionResult ProcessResponse(BaseViewModel viewModel)
        {
            if (viewModel.ResponseType == ResponseType.Forbidden)
            {
                return this.Redirect("~/Error/Forbidden");
            }
            else if (viewModel.ResponseType == ResponseType.NotFound)
            {
                return this.HttpNotFound();
            }
            else if (viewModel.ResponseType == ResponseType.ChangesSaved)
            {
                return this.RedirectToAction("ChangesSaved");
            }
            else if (viewModel.ResponseType == ResponseType.RedirectToUrl)
            {
                return this.RedirectToAction(viewModel.RedirectToAction);
            }
            else if (viewModel.ResponseType == ResponseType.PartialView)
            {
                return this.PartialView(viewModel.PartialView);
            }
            else
            {
                return this.View(viewModel);
            }
        }
    }
}