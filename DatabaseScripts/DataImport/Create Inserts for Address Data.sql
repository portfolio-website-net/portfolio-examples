SELECT 'INSERT INTO Address (AddressID, AddressLine1, AddressLine2, City, StateProvince, CountryRegion, PostalCode, rowguid, ModifiedDate) VALUES (' +
       '''' + CONVERT(nvarchar(max), AddressID) + ''',' +	   
	   '''' + CONVERT(nvarchar(max), REPLACE(AddressLine1, '''', '''''')) + ''',' +
	   '''' + ISNULL(CONVERT(nvarchar(max), REPLACE(AddressLine2, '''', '''''')), 'NULL') + ''',' +
	   '''' + CONVERT(nvarchar(max), REPLACE(City, '''', '''''')) + ''',' +
	   '''' + CONVERT(nvarchar(max), StateProvince) + ''',' +
	   '''' + CONVERT(nvarchar(max), CountryRegion) + ''',' +
	   '''' + CONVERT(nvarchar(max), PostalCode) + ''',' +
	   '''' + CONVERT(nvarchar(max), rowguid) + ''',' +
	   '''' + CONVERT(nvarchar(max), ModifiedDate, 120) + ''');'
  FROM [SalesLT].[Address]