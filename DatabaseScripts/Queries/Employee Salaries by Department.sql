-- Employee salaries by department
DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e)

;WITH cteCurrentPay(EmployeeId, Salary)
     AS (SELECT e.EmployeeId,
                e.Salary
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate)
SELECT Department = d.[Name],
       Employees = COUNT(*),
       [Average Salary] = '$' + CONVERT(varchar, CONVERT(MONEY, AVG(cp.Salary)), 1),
       [Maximum Salary] = '$' + CONVERT(varchar, CONVERT(MONEY, MAX(cp.Salary)), 1),
       [Minimum Salary] = '$' + CONVERT(varchar, CONVERT(MONEY, MIN(cp.Salary)), 1),
       [Standard Deviation] = '$' + CONVERT(varchar, CONVERT(MONEY, STDEV(cp.Salary)), 1)
  FROM cteCurrentPay cp
 INNER JOIN Employees.Employees e ON cp.EmployeeId = e.EmployeeId
 INNER JOIN Employees.Departments d ON e.DepartmentId = d.DepartmentId
 WHERE e.Salary > 0
 GROUP BY d.[Name]
 ORDER BY Department