-- Total salary/wage cost by department
DECLARE @EstimatedSalaryHoursPerWeek INT
DECLARE @EstimatedSalaryWeeksPerYear INT
SET @EstimatedSalaryHoursPerWeek = 40
SET @EstimatedSalaryWeeksPerYear = 52

DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e);

WITH cteCurrentPay(EmployeeId, Salary, Wage)
     AS (SELECT e.EmployeeId,
                e.Salary,
                e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate)
SELECT Department = d.[Name],
       [Total Salary] = '$' + CONVERT(varchar, CONVERT(MONEY, SUM(cp.Salary)), 1),
       [Total Wage] = '$' + CONVERT(varchar, CONVERT(MONEY, SUM(cp.Wage)), 1),
       [Total Cost] = '$' + CONVERT(varchar, CONVERT(MONEY, SUM(ISNULL(cp.Salary, 0) + ISNULL(cp.Wage, 0))), 1)
  FROM cteCurrentPay cp
 INNER JOIN Employees.Employees e ON cp.EmployeeId = e.EmployeeId
 INNER JOIN Employees.Departments d ON e.DepartmentId = d.DepartmentId
 GROUP BY d.[Name]
 ORDER BY Department