-- Salary percent increase by position
DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e);

DECLARE @MonthAdjustment INT
SET @MonthAdjustment = -69 -- 2012-07-15

DECLARE @MostRecentDateMinusMonthAdjustment DATE
SET @MostRecentDateMinusMonthAdjustment = DATEADD(MONTH, @MonthAdjustment, @MostRecentDate)

SELECT @MostRecentDateMinusMonthAdjustment

;WITH cteCurrentPay1(FirstName, LastName, Position, DepartmentId, Salary)
     AS (SELECT e.FirstName,
                e.LastName,
                e.Position,
                e.DepartmentId,
                e.Salary
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate),
     cteCurrentPay2(FirstName, LastName, Position, DepartmentId, Salary)
     AS (SELECT e.FirstName,
                e.LastName,
                e.Position,
                e.DepartmentId,
                e.Salary
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDateMinusMonthAdjustment)
SELECT t.Department,
       t.Position,
       '$' + CONVERT(varchar, CONVERT(MONEY, t.[Current]), 1) AS [Current],
       '$' + CONVERT(varchar, CONVERT(MONEY, t.[CurrentMinusAdj]), 1) AS [CurrentMinusAdj],
       CONVERT(varchar, CONVERT(decimal(18, 4), (t.[Current]
                                              - t.[CurrentMinusAdj]) / t.[CurrentMinusAdj]), 1) AS [Average Percent Increase]
  FROM
  (
    SELECT Department = d.[Name],
       Position = cp1.Position,
           [Current] = AVG(cp1.Salary),
           [CurrentMinusAdj] = AVG(cp2.Salary)
      FROM cteCurrentPay1 cp1
     INNER JOIN cteCurrentPay2 cp2 ON cp1.FirstName = cp2.FirstName
           AND cp1.LastName = cp2.LastName
           AND cp1.Position = cp2.Position
           AND cp1.DepartmentId = cp2.DepartmentId
     INNER JOIN Employees.Departments d ON cp1.DepartmentId = d.DepartmentId
     WHERE cp1.Salary > 0
       AND cp2.Salary > 0
     GROUP BY d.[Name],
          cp1.Position
  ) t
 ORDER BY t.Department,
          t.Position