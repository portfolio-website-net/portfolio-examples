-- Overall cost per month
DECLARE @EstimatedSalaryHoursPerWeek INT
DECLARE @EstimatedSalaryWeeksPerYear INT
SET @EstimatedSalaryHoursPerWeek = 40
SET @EstimatedSalaryWeeksPerYear = 52

SELECT CONVERT(nvarchar(4), YEAR(e.[Date])) + '-'
         + REPLICATE('0',2-LEN(CONVERT(nvarchar(2), MONTH(e.[Date]))))
         + CONVERT(nvarchar(2), MONTH(e.[Date])) AS [Month],
       '$' + CONVERT(varchar, CONVERT(MONEY, SUM(ISNULL(e.Salary, 0)
           + ISNULL(e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear, 0))), 1) AS Cost
  FROM Employees.Employees e
 GROUP BY e.[Date]
 ORDER BY e.[Date]