SELECT TOP 10 [Artist],
       COUNT(1) AS [PlayCount]
  FROM [PlaylistItems] 
 GROUP BY [Artist]
 ORDER BY [PlayCount] DESC, [Artist]
