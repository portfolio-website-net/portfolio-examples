-- Average salary/wage cost per position
DECLARE @EstimatedSalaryHoursPerWeek INT
DECLARE @EstimatedSalaryWeeksPerYear INT
SET @EstimatedSalaryHoursPerWeek = 40
SET @EstimatedSalaryWeeksPerYear = 52

DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e)

;WITH cteCurrentPay(EmployeeId, Salary, Wage)
     AS (SELECT e.EmployeeId,
                e.Salary,
                e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate)
SELECT TOP 200 Position = e.Position,
       [Average Salary] = '$' + CONVERT(varchar, CONVERT(MONEY, AVG(cp.Salary)), 1),
       [Average Wage] = '$' + CONVERT(varchar, CONVERT(MONEY, AVG(cp.Wage)), 1),
       [Average Total Cost] = '$' + CONVERT(varchar, CONVERT(MONEY, AVG(ISNULL(cp.Salary, 0) + ISNULL(cp.Wage, 0))), 1)
  FROM cteCurrentPay cp
 INNER JOIN Employees.Employees e ON cp.EmployeeId = e.EmployeeId
 INNER JOIN Employees.Departments d ON e.DepartmentId = d.DepartmentId
 GROUP BY e.Position
 ORDER BY Position