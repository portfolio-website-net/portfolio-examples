-- Headcount per month
SELECT CONVERT(nvarchar(4), YEAR(e.[Date])) + '-'
         + REPLICATE('0',2-LEN(CONVERT(nvarchar(2), MONTH(e.[Date]))))
         + CONVERT(nvarchar(2), MONTH(e.[Date])) AS [Month],
       COUNT(1) AS [Count]
  FROM Employees.Employees e
 GROUP BY e.[Date]
 ORDER BY e.[Date]