-- Get employee records per date
SELECT t.[Date],
       t.RecordCount,
       t.CurrentPage,
       t.[PageCount],
       CAST((CAST(t.CurrentPage AS float)
             / CAST(t.[PageCount] AS float)) * 100 AS decimal(10, 2)
       ) AS PercentComplete
  FROM
  (
        SELECT t2.[Date] AS [Date],
               t2.RecordCount AS RecordCount,
               (SELECT MAX(e2.[Page])
                  FROM Employees.Employees e2
                 WHERE e2.[Date] = t2.[Date]) AS CurrentPage,
               ep.[PageCount] AS [PageCount]
          FROM
          (
                SELECT e.[Date] As [Date],
                       COUNT(1) AS RecordCount
                  FROM Employees.Employees e
                 GROUP BY e.[Date]
          ) t2
         INNER JOIN Employees.EmployeePages ep ON t2.[Date] = ep.[Date]
  ) t
 ORDER BY t.[Date]

-- Get overall record count
SELECT COUNT(1) AS TotalRecordCount
  FROM Employees.Employees e
