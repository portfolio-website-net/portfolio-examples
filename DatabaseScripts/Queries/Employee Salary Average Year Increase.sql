-- Employee salary average year increase
DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e);

DECLARE @MostRecentDateMinus1Year DATE
SET @MostRecentDateMinus1Year = DATEADD(YEAR, -1, @MostRecentDate)

;WITH cteCurrentPay1(FirstName, LastName, Position, DepartmentId, Salary)
     AS (SELECT e.FirstName,
                e.LastName,
                e.Position,
                e.DepartmentId,
                e.Salary
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate),
     cteCurrentPay2(FirstName, LastName, Position, DepartmentId, Salary)
     AS (SELECT e.FirstName,
                e.LastName,
                e.Position,
                e.DepartmentId,
                e.Salary
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDateMinus1Year)
SELECT t.Department,
       '$' + CONVERT(varchar, CONVERT(MONEY, t.[Average Salary Last Year]), 1) AS [Average Salary Last Year],
       '$' + CONVERT(varchar, CONVERT(MONEY, t.[Average Salary This Year]), 1) AS [Average Salary This Year],
       '$' + CONVERT(varchar, CONVERT(MONEY, (t.[Average Salary This Year]
                                              - t.[Average Salary Last Year])), 1) AS [Average Salary Increase]
  FROM
  (
    SELECT Department = d.[Name],
           [Average Salary Last Year] = AVG(cp2.Salary),
           [Average Salary This Year] = AVG(cp1.Salary)
      FROM cteCurrentPay1 cp1
     INNER JOIN cteCurrentPay2 cp2 ON cp1.FirstName = cp2.FirstName
           AND cp1.LastName = cp2.LastName
           AND cp1.Position = cp2.Position
           AND cp1.DepartmentId = cp2.DepartmentId
     INNER JOIN Employees.Departments d ON cp1.DepartmentId = d.DepartmentId
     WHERE cp1.Salary > 0
       AND cp2.Salary > 0
     GROUP BY d.[Name]
  ) t
 ORDER BY t.Department