SELECT DISTINCT [Artist],
       [Song]
  FROM [PlaylistItems] 
 ORDER BY [Artist], [Song]
