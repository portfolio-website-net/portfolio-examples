-- Employee count by pay range
DECLARE @EstimatedSalaryHoursPerWeek INT
DECLARE @EstimatedSalaryWeeksPerYear INT
SET @EstimatedSalaryHoursPerWeek = 40
SET @EstimatedSalaryWeeksPerYear = 52

DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e)

SELECT t.[Pay Range],
       COUNT(1) AS [Count]
  FROM
  (
        SELECT CASE WHEN (e.Salary BETWEEN 0 AND 20000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 0 AND 20000) THEN '$0 to $20K'
                    WHEN (e.Salary BETWEEN 20000 + 1 AND 30000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 20000 + 1 AND 30000) THEN '$20K to $30K'
                    WHEN (e.Salary BETWEEN 30000 + 1 AND 40000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 30000 + 1 AND 40000) THEN '$30K to $40K'
                    WHEN (e.Salary BETWEEN 40000 + 1 AND 50000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 40000 + 1 AND 50000) THEN '$40K to $50K'
                    WHEN (e.Salary BETWEEN 50000 + 1 AND 60000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 50000 + 1 AND 60000) THEN '$50K to $60K'
                    WHEN (e.Salary BETWEEN 60000 + 1 AND 70000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 60000 + 1 AND 70000) THEN '$60K to $70K'
                    WHEN (e.Salary BETWEEN 70000 + 1 AND 80000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 70000 + 1 AND 80000) THEN '$70K to $80K'
                    WHEN (e.Salary BETWEEN 80000 + 1 AND 90000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 80000 + 1 AND 90000) THEN '$80K to $90K'
                    WHEN (e.Salary BETWEEN 90000 + 1 AND 100000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 90000 + 1 AND 100000) THEN '$90K to $100K'
                    WHEN (e.Salary BETWEEN 100000 + 1 AND 110000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 100000 + 1 AND 110000) THEN '$100K to $1100K'
                    WHEN (e.Salary BETWEEN 110000 + 1 AND 120000)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear BETWEEN 110000 + 1 AND 120000) THEN '$110K to $120K'
                    WHEN (e.Salary >= 120000 + 1)
                      OR (e.Wage * @EstimatedSalaryHoursPerWeek * @EstimatedSalaryWeeksPerYear >= 120000 + 1) THEN '> $120K'
                END AS [Pay Range]
          FROM Employees.Employees e
         WHERE e.[Date] = @MostRecentDate
           AND (e.Salary IS NOT NULL OR e.Wage IS NOT NULL)
  ) t
 GROUP BY t.[Pay Range]
 ORDER BY COUNT(1) DESC