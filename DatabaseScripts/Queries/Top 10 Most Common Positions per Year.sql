-- Top 10 most common positions per year
SELECT t.*
  FROM
  (
    SELECT t2.Position,
           t2.[Year],
           t2.[Count],
           ROW_NUMBER() OVER (
                            PARTITION BY t2.[Year]
                                ORDER BY t2.[Year],
                                         t2.[Count] DESC
                            ) AS [Rank]
      FROM
      (
        SELECT e.Position,
               YEAR(e.[Date]) AS [Year],
               COUNT(1) AS [Count]
          FROM Employees.Employees e
         GROUP BY e.Position,
                  YEAR(e.[Date])
      ) t2
  ) t
 WHERE t.[Rank] <= 10
 ORDER BY t.[Year],
          t.[Count] DESC