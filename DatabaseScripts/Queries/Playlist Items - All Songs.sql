SELECT s.[Url],
       FORMAT(pi.[Time], 'MM/dd/yyyy h:mm tt') AS [Time],
	   pi.[Artist],
	   pi.[Song]
  FROM [PlaylistItems] pi
 INNER JOIN [Stations] s ON pi.StationId = s.StationId
 ORDER BY pi.[Time], [Artist], [Song]
