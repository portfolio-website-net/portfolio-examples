-- Employee wages by department
DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e)

;WITH cteCurrentPay(EmployeeId, Wage)
     AS (SELECT e.EmployeeId,
                e.Wage
           FROM Employees.Employees e
          WHERE e.[Date] = @MostRecentDate)
SELECT Department = d.[Name],
       Employees = COUNT(*),
       [Average Wage] = '$' + CONVERT(varchar, CONVERT(MONEY, AVG(cp.Wage)), 1),
       [Maximum Wage] = '$' + CONVERT(varchar, CONVERT(MONEY, MAX(cp.Wage)), 1),
       [Minimum Wage] = '$' + CONVERT(varchar, CONVERT(MONEY, MIN(cp.Wage)), 1),
       [Standard Deviation] = '$' + CONVERT(varchar, CONVERT(MONEY, STDEV(cp.Wage)), 1)
  FROM cteCurrentPay cp
 INNER JOIN Employees.Employees e ON cp.EmployeeId = e.EmployeeId
 INNER JOIN Employees.Departments d ON e.DepartmentId = d.DepartmentId
 WHERE e.Wage > 0
 GROUP BY d.[Name]
 ORDER BY Department