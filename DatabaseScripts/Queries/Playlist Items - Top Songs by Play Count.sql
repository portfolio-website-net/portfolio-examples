SELECT TOP 10 [Artist],
       [Song],
       COUNT(1) AS [PlayCount]
  FROM [PlaylistItems] 
 GROUP BY [Artist], [Song]
 ORDER BY [PlayCount] DESC, [Artist], [Song]
