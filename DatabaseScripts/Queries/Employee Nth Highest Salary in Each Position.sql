-- Employee Nth highest salary in each group
DECLARE @MostRecentDate DATE
SET @MostRecentDate = (SELECT MAX(e.[Date])
                         FROM Employees.Employees e)

SELECT TOP 10
       t.FirstName,
       t.LastName,
       t.Position,
       '$' + CONVERT(varchar, CONVERT(MONEY, t.Salary), 1) AS Salary,
       [ROW NUMBER] AS 'Highest Salary'
  FROM (
        SELECT e.FirstName,
               e.LastName,
               e.Position,
               e.Salary,
               ROW_NUMBER() OVER (
                        PARTITION BY e.Position
                            ORDER BY e.Salary DESC
                        ) AS [ROW NUMBER]
          FROM Employees.Employees e
         WHERE e.[Date] = @MostRecentDate
           AND e.Salary > 0
       ) t
 WHERE [ROW NUMBER] = 3
 ORDER BY t.Salary DESC