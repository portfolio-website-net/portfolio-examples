DELETE FROM App.[Agreements] WHERE 1 = 1
	
INSERT INTO App.[Agreements] ([TypeId], [AgreementStatusTypeId], [CreatedById], [CreatedDate]) VALUES ((SELECT TypeId FROM App.[Types] WHERE Name = 'Recreation Event Agreement (Wet Signature)'), (SELECT AgreementStatusTypeId FROM App.AgreementStatusTypes WHERE Code = 'Draft'), 0, GetDate())

DECLARE @AgreementId int
SET @AgreementId = (SELECT SCOPE_IDENTITY())

INSERT INTO App.[AgreementTemplates] ([AgreementId], [TypeTemplateId], [Content], [Order], [CreatedById], [CreatedDate])
SELECT @AgreementId,
       TypeTemplateId,
	   'The event name is: [Event Name]', 
	   1,
	   0, 
	   GetDate()
  FROM App.TypeTemplates tt
 WHERE tt.TypeId = (SELECT TypeId FROM App.Agreements WHERE AgreementId = @AgreementId)