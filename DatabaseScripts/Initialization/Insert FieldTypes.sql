DELETE FROM App.FieldTypes WHERE 1 = 1
	
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Collection of Fields', 'CollectionOfFields', 1, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Currency Amount', 'CurrencyAmount', 2, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Email Address', 'EmailAddress', 3, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Date Picker', 'DatePicker', 4, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Phone Number', 'PhoneNumber', 5, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Selection List', 'SelectionList', 6, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('State List', 'StateList', 7, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Text', 'Text', 8, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Upload', 'Upload', 9, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Whole Number Amount', 'WholeNumberAmount', 10, 0, GetDate())
INSERT INTO App.FieldTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Zipcode', 'Zipcode', 11, 0, GetDate())
