DELETE FROM App.[AgreementStatusTypes] WHERE 1 = 1
	
INSERT INTO App.[AgreementStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Draft', 'Draft', 1, 0, GetDate())
INSERT INTO App.[AgreementStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('In Progress', 'InProgress', 2, 0, GetDate())
INSERT INTO App.[AgreementStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Complete', 'Complete', 3, 0, GetDate())
INSERT INTO App.[AgreementStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Cancelled', 'Cancelled', 4, 0, GetDate())
INSERT INTO App.[AgreementStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Deleted', 'Deleted', 5, 0, GetDate())

