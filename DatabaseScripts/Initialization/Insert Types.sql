DELETE FROM App.[Types] WHERE 1 = 1
	
INSERT INTO App.[Types] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Recreation Event Agreement (Wet Signature)', 1, 0, GetDate())
INSERT INTO App.[Types] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Recreation Event Agreement (E-Signature)', 2, 0, GetDate())
INSERT INTO App.[Types] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Special Recreation Legal Agreement', 3, 0, GetDate())

