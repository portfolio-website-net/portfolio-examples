DELETE FROM App.AssignmentTypes WHERE 1 = 1
	
INSERT INTO App.AssignmentTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Director', 'Director', 1, 0, GetDate())
INSERT INTO App.AssignmentTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Employee', 'Employee', 2, 0, GetDate())
INSERT INTO App.AssignmentTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Legal', 'Legal', 3, 0, GetDate())
INSERT INTO App.AssignmentTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Manager', 'Manager', 4, 0, GetDate())
INSERT INTO App.AssignmentTypes ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Section', 'Section', 5, 0, GetDate())
