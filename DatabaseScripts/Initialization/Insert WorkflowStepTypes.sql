DELETE FROM App.[WorkflowStatusTypes] WHERE 1 = 1
	
INSERT INTO App.[WorkflowStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Pending', 'Pending', 1, 0, GetDate())
INSERT INTO App.[WorkflowStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Complete', 'Complete', 2, 0, GetDate())
INSERT INTO App.[WorkflowStatusTypes] ([Name], [Code], [Order], [CreatedById], [CreatedDate]) VALUES ('Rejected', 'Rejected', 3, 0, GetDate())
