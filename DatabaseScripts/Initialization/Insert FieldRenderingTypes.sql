DELETE FROM App.[FieldRenderingTypes] WHERE 1 = 1
	
INSERT INTO App.[FieldRenderingTypes] ([Name], [Code], [CreatedById], [CreatedDate]) VALUES ('Whole Dollars', 'WholeDollars', 0, GetDate())
INSERT INTO App.[FieldRenderingTypes] ([Name], [Code], [CreatedById], [CreatedDate]) VALUES ('Render as Checkboxes', 'RenderAsCheckboxes', 0, GetDate())
INSERT INTO App.[FieldRenderingTypes] ([Name], [Code], [CreatedById], [CreatedDate]) VALUES ('Render as MultiSelect', 'RenderAsMultiSelect', 0, GetDate())
