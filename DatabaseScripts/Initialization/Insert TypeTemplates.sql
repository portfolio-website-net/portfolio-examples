DELETE FROM App.[TypeTemplates] WHERE 1 = 1
DELETE FROM App.[TypeTemplateTypes] WHERE 1 = 1	

INSERT INTO App.[TypeTemplateTypes] ([Name], [Code], [RenderAsFormInd], [Order], [CreatedById], [CreatedDate]) VALUES ('Event Application (Public Application Form)', 'EventApplication', 1, 1, 0, GetDate())
INSERT INTO App.[TypeTemplateTypes] ([Name], [Code], [RenderAsFormInd], [Order], [CreatedById], [CreatedDate]) VALUES ('Event Agreement (Legal Document Body)', 'EventAgreement', 2, 0, 0, GetDate())
INSERT INTO App.[TypeTemplateTypes] ([Name], [Code], [RenderAsFormInd], [Order], [CreatedById], [CreatedDate]) VALUES ('Event Detail Information (Document Attachment)', 'EventDetailInformation', 0, 3, 0, GetDate())

INSERT INTO App.[TypeTemplates] ([TypeId], [TypeTemplateTypeId], [Name], [Code], [Content], [Order], [CreatedById], [CreatedDate]) VALUES (1, (SELECT TypeTemplateTypeId FROM App.[TypeTemplateTypes] WHERE Code = 'EventApplication'), 'Event Agreement Application Form', 'ApplicationForm', '<h1>Welcome to the Event Agreement Application</h1>', 1, 0, GetDate())
INSERT INTO App.[TypeTemplates] ([TypeId], [TypeTemplateTypeId], [Name], [Code], [Content], [Order], [CreatedById], [CreatedDate]) VALUES (1, (SELECT TypeTemplateTypeId FROM App.[TypeTemplateTypes] WHERE Code = 'EventApplication'), 'Event Agreement', 'EventAgreement', '<h1>EVENT AGREEMENT</h1>', 2, 0, GetDate())
INSERT INTO App.[TypeTemplates] ([TypeId], [TypeTemplateTypeId], [Name], [Code], [Content], [Order], [CreatedById], [CreatedDate]) VALUES (1, (SELECT TypeTemplateTypeId FROM App.[TypeTemplateTypes] WHERE Code = 'EventDetailInformation'), 'Event Details', 'EventDetails', '<h1>EVENT AGREEMENT</h1>', 3, 0, GetDate())

