DELETE FROM App.[FieldListItems] WHERE 1 = 1
DELETE FROM App.[ListItems] WHERE 1 = 1
	
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Biking', 1, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Hiking', 2, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Motorbikes', 3, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Picnicking', 4, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Hunting', 5, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Swimming', 6, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Playground', 7, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Overnight Camping', 8, 0, GetDate())
INSERT INTO App.[ListItems] ([Name], [Order], [CreatedById], [CreatedDate]) VALUES ('Daytime Camping', 9, 0, GetDate())

INSERT INTO App.[FieldListItems] ([FieldId], [ListItemId], [CreatedById], [CreatedDate])
SELECT (SELECT FieldId FROM App.[Fields] WHERE Code = 'EventType'), [ListItemId], [CreatedById], [CreatedDate]
  FROM App.[ListItems]