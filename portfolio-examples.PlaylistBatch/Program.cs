﻿// <copyright file="Program.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.PlaylistBatch
{
    using Business.Interfaces;
    using Business.Providers;

    public class Program
    {
        public static void Main(string[] args)
        {
            IPlaylistExtract provider = new PlaylistExtract();

            while (true)
            {
                provider.StartPlaylistExtract();
            }
        }
    }
}
