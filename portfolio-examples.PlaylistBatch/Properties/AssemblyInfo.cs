﻿// <copyright file="AssemblyInfo.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#pragma warning disable SA1028 // Code must not contain trailing whitespace

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("portfolio-examples.PlaylistBatch")]
#pragma warning restore SA1028 // Code must not contain trailing whitespace
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hewlett-Packard Company")]
[assembly: AssemblyProduct("portfolio-examples.PlaylistBatch")]
[assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

#pragma warning disable SA1028 // Code must not contain trailing whitespace

// Setting ComVisible to false makes the types in this assembly not visible 
#pragma warning disable SA1028 // Code must not contain trailing whitespace

// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
#pragma warning restore SA1028 // Code must not contain trailing whitespace
#pragma warning restore SA1028 // Code must not contain trailing whitespace

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e33132c6-4e35-4379-961c-ac94afabad3a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
#pragma warning disable SA1028 // Code must not contain trailing whitespace

 // Minor Version 
 //      Build Number
 //      Revision
 //
#pragma warning disable SA1028 // Code must not contain trailing whitespace

 // You can specify all the values or you can default the Build and Revision Numbers 
 // by using the '*' as shown below:
 // [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
#pragma warning restore SA1028 // Code must not contain trailing whitespace
#pragma warning restore SA1028 // Code must not contain trailing whitespace
[assembly: AssemblyFileVersion("1.0.0.0")]
