﻿CREATE TABLE [Attendance].[Counters] (
    [CounterId]      INT               IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (100)    NOT NULL,
    [StatusTypeId]   INT               NOT NULL,
    [LocationId]     INT               NOT NULL,
    [CounterTypeId]  INT               NOT NULL,
    [SerialNumber]   NVARCHAR (50)     NULL,
    [Description]    NVARCHAR (MAX)    NULL,
    [Latitude]       FLOAT (53)        NULL,
    [Longitude]      FLOAT (53)        NULL,
    [GeographyPoint] [sys].[geography] NULL,
    [CreatedById]    INT               NOT NULL,
    [CreatedDate]    DATETIME          NOT NULL,
    [UpdatedById]    INT               NULL,
    [UpdatedDate]    DATETIME          NULL,
    CONSTRAINT [PK_Counters] PRIMARY KEY CLUSTERED ([CounterId] ASC),
    CONSTRAINT [FK_Counters_CounterTypes] FOREIGN KEY ([CounterTypeId]) REFERENCES [Attendance].[CounterTypes] ([CounterTypeId]),
    CONSTRAINT [FK_Counters_Locations] FOREIGN KEY ([LocationId]) REFERENCES [Attendance].[Locations] ([LocationId]),
    CONSTRAINT [FK_Counters_StatusTypes] FOREIGN KEY ([StatusTypeId]) REFERENCES [Attendance].[StatusTypes] ([StatusTypeId])
);
GO

