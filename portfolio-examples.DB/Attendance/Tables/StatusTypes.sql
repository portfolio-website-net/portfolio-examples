﻿CREATE TABLE [Attendance].[StatusTypes] (
    [StatusTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (100) NOT NULL,
    [Code]         NVARCHAR (50)  NOT NULL,
    [Order]        INT            NOT NULL,
    [CreatedById]  INT            NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [UpdatedById]  INT            NULL,
    [UpdatedDate]  DATETIME       NULL,
    CONSTRAINT [PK_StatusTypes] PRIMARY KEY CLUSTERED ([StatusTypeId] ASC)
);
GO

