﻿CREATE TABLE [Attendance].[CounterFormulas] (
    [CounterFormulaId] INT            IDENTITY (1, 1) NOT NULL,
    [CounterId]        INT            NOT NULL,
    [StartDate]        DATE           NOT NULL,
    [EndDate]          DATE           NULL,
    [CounterFormula]   NVARCHAR (500) NULL,
    [CreatedById]      INT            NOT NULL,
    [CreatedDate]      DATETIME       NOT NULL,
    [UpdatedById]      INT            NULL,
    [UpdatedDate]      DATETIME       NULL,
    CONSTRAINT [PK_CounterFormulas] PRIMARY KEY CLUSTERED ([CounterFormulaId] ASC),
    CONSTRAINT [FK_CounterFormulas_Counters] FOREIGN KEY ([CounterId]) REFERENCES [Attendance].[Counters] ([CounterId])
);
GO

