﻿CREATE TABLE [Attendance].[CounterAttachments] (
    [CounterAttachmentId] INT            IDENTITY (1, 1) NOT NULL,
    [CounterId]           INT            NOT NULL,
    [FileName]            NVARCHAR (200) NOT NULL,
    [CreatedById]         INT            NOT NULL,
    [CreatedDate]         DATETIME       NOT NULL,
    [UpdatedById]         INT            NULL,
    [UpdatedDate]         DATETIME       NULL,
    CONSTRAINT [PK_CounterAttachments] PRIMARY KEY CLUSTERED ([CounterAttachmentId] ASC),
    CONSTRAINT [FK_CounterAttachments_Counters] FOREIGN KEY ([CounterId]) REFERENCES [Attendance].[Counters] ([CounterId])
);
GO

