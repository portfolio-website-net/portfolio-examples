﻿CREATE TABLE [Attendance].[CounterRecords] (
    [CounterRecordId] INT      IDENTITY (1, 1) NOT NULL,
    [CounterId]       INT      NOT NULL,
    [Date]            DATETIME NOT NULL,
    [Count]           INT      NOT NULL,
    [CreatedById]     INT      NOT NULL,
    [CreatedDate]     DATETIME NOT NULL,
    [UpdatedById]     INT      NULL,
    [UpdatedDate]     DATETIME NULL,
    CONSTRAINT [PK_CounterRecords] PRIMARY KEY CLUSTERED ([CounterRecordId] ASC),
    CONSTRAINT [FK_CounterRecords_Counters] FOREIGN KEY ([CounterId]) REFERENCES [Attendance].[Counters] ([CounterId])
);
GO

