﻿CREATE TABLE [Attendance].[Locations] (
    [LocationId]  INT            IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [CreatedById] INT            NOT NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [UpdatedById] INT            NULL,
    [UpdatedDate] DATETIME       NULL,
    CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED ([LocationId] ASC)
);
GO

