﻿CREATE TABLE [Attendance].[LocationApprovals] (
    [LocationApprovalId] INT            IDENTITY (1, 1) NOT NULL,
    [LocationId]         INT            NOT NULL,
    [ApprovedBy]         NVARCHAR (100) NOT NULL,
    [ApprovalTypeId]     INT            NOT NULL,
    [ApprovalDate]       DATETIME       NOT NULL,
    [CreatedById]        INT            NOT NULL,
    [CreatedDate]        DATETIME       NOT NULL,
    [UpdatedById]        INT            NULL,
    [UpdatedDate]        DATETIME       NULL,
    CONSTRAINT [PK_LocationApprovals] PRIMARY KEY CLUSTERED ([LocationApprovalId] ASC),
    CONSTRAINT [FK_LocationApprovals_ApprovalTypes] FOREIGN KEY ([ApprovalTypeId]) REFERENCES [Attendance].[ApprovalTypes] ([ApprovalTypeId]),
    CONSTRAINT [FK_LocationApprovals_Locations] FOREIGN KEY ([LocationId]) REFERENCES [Attendance].[Locations] ([LocationId])
);
GO

