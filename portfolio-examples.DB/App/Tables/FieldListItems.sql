﻿CREATE TABLE [App].[FieldListItems] (
    [FieldListItemId] INT      IDENTITY (1, 1) NOT NULL,
    [FieldId]         INT      NOT NULL,
    [ListItemId]      INT      NOT NULL,
    [CreatedById]     INT      NOT NULL,
    [CreatedDate]     DATETIME NOT NULL,
    [UpdatedById]     INT      NULL,
    [UpdatedDate]     DATETIME NULL,
    CONSTRAINT [PK_FieldListItems] PRIMARY KEY CLUSTERED ([FieldListItemId] ASC),
    CONSTRAINT [FK_FieldListItems_Fields] FOREIGN KEY ([FieldId]) REFERENCES [App].[Fields] ([FieldId]),
    CONSTRAINT [FK_FieldListItems_ListItems] FOREIGN KEY ([ListItemId]) REFERENCES [App].[ListItems] ([ListItemId])
);

