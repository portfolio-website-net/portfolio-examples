﻿CREATE TABLE [App].[AgreementFieldValues] (
    [AgreementFieldValueId] INT              IDENTITY (1, 1) NOT NULL,
    [AgreementId]           INT              NOT NULL,
    [FieldId]               INT              NOT NULL,
    [Value]                 NVARCHAR (MAX)   NULL,
    [Guid]                  UNIQUEIDENTIFIER NULL,
    [CreatedById]           INT              NOT NULL,
    [CreatedDate]           DATETIME         NOT NULL,
    [UpdatedById]           INT              NULL,
    [UpdatedDate]           DATETIME         NULL,
    CONSTRAINT [PK_AgreementFieldValues] PRIMARY KEY CLUSTERED ([AgreementFieldValueId] ASC),
    CONSTRAINT [FK_AgreementFieldValues_Agreements] FOREIGN KEY ([AgreementId]) REFERENCES [App].[Agreements] ([AgreementId]),
    CONSTRAINT [FK_AgreementFieldValues_Fields] FOREIGN KEY ([FieldId]) REFERENCES [App].[Fields] ([FieldId])
);



