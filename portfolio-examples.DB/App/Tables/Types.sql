﻿CREATE TABLE [App].[Types] (
    [TypeId]         INT              IDENTITY (1, 1) NOT NULL,
    [OriginalTypeId] INT              NULL,
    [Name]           NVARCHAR (100)   NOT NULL,
    [Code]           NVARCHAR (50)    NULL,
    [Order]          INT              NOT NULL,
    [Guid]           UNIQUEIDENTIFIER NULL,
    [CreatedById]    INT              NOT NULL,
    [CreatedDate]    DATETIME         NULL,
    [UpdatedById]    INT              NULL,
    [UpdatedDate]    DATETIME         NULL,
    CONSTRAINT [PK_Types] PRIMARY KEY CLUSTERED ([TypeId] ASC),
    CONSTRAINT [FK_Types_Types] FOREIGN KEY ([OriginalTypeId]) REFERENCES [App].[Types] ([TypeId])
);


GO

