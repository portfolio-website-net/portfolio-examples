﻿CREATE TABLE [App].[AgreementTemplates] (
    [AgreementTemplateId] INT              IDENTITY (1, 1) NOT NULL,
    [AgreementId]         INT              NOT NULL,
    [TypeTemplateId]      INT              NOT NULL,
    [Content]             NVARCHAR (MAX)   NOT NULL,
    [Order]               INT              NOT NULL,
    [Guid]                UNIQUEIDENTIFIER NULL,
    [CreatedById]         INT              NOT NULL,
    [CreatedDate]         DATETIME         NOT NULL,
    [UpdatedById]         INT              NULL,
    [UpdatedDate]         DATETIME         NULL,
    CONSTRAINT [PK_AgreementTemplates] PRIMARY KEY CLUSTERED ([AgreementTemplateId] ASC),
    CONSTRAINT [FK_AgreementTemplates_Agreements] FOREIGN KEY ([AgreementId]) REFERENCES [App].[Agreements] ([AgreementId]),
    CONSTRAINT [FK_AgreementTemplates_TypeTemplates] FOREIGN KEY ([TypeTemplateId]) REFERENCES [App].[TypeTemplates] ([TypeTemplateId])
);


GO

