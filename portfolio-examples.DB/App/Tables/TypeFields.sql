﻿CREATE TABLE [App].[TypeFields] (
    [TypeFieldId] INT      IDENTITY (1, 1) NOT NULL,
    [TypeId]      INT      NOT NULL,
    [FieldId]     INT      NOT NULL,
    [CreatedById] INT      NOT NULL,
    [CreatedDate] DATETIME NOT NULL,
    [UpdatedById] INT      NULL,
    [UpdatedDate] DATETIME NULL,
    CONSTRAINT [PK_TypeFields] PRIMARY KEY CLUSTERED ([TypeFieldId] ASC),
    CONSTRAINT [FK_TypeFields_Fields] FOREIGN KEY ([FieldId]) REFERENCES [App].[Fields] ([FieldId]),
    CONSTRAINT [FK_TypeFields_Types] FOREIGN KEY ([TypeId]) REFERENCES [App].[Types] ([TypeId])
);

