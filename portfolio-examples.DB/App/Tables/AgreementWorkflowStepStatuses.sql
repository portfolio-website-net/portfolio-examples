﻿CREATE TABLE [App].[AgreementWorkflowStepStatuses] (
    [AgreementWorkflowStepStatusesId] INT      IDENTITY (1, 1) NOT NULL,
    [AgreementId]                     INT      NOT NULL,
    [WorkflowStepId]                  INT      NOT NULL,
    [WorkflowStatusTypeId]            INT      NOT NULL,
    [ApprovedById]                    INT      NULL,
    [ApprovedDate]                    DATETIME NULL,
    [CreatedById]                     INT      NOT NULL,
    [CreatedDate]                     DATETIME NOT NULL,
    [UpdatedById]                     INT      NULL,
    [UpdatedDate]                     DATETIME NULL,
    CONSTRAINT [PK_AgreementWorkflowSteps] PRIMARY KEY CLUSTERED ([AgreementWorkflowStepStatusesId] ASC),
    CONSTRAINT [FK_AgreementWorkflowStepStatuses_Agreements] FOREIGN KEY ([AgreementId]) REFERENCES [App].[Agreements] ([AgreementId]),
    CONSTRAINT [FK_AgreementWorkflowStepStatuses_WorkflowStatusTypes] FOREIGN KEY ([WorkflowStatusTypeId]) REFERENCES [App].[WorkflowStatusTypes] ([WorkflowStatusTypeId]),
    CONSTRAINT [FK_AgreementWorkflowStepStatuses_WorkflowSteps] FOREIGN KEY ([WorkflowStepId]) REFERENCES [App].[WorkflowSteps] ([WorkflowStepId])
);

