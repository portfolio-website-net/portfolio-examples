﻿CREATE TABLE [App].[FieldRenderingTypes] (
    [FieldRenderingTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (100) NOT NULL,
    [Code]                 NVARCHAR (50)  NULL,
    [CreatedById]          INT            NOT NULL,
    [CreatedDate]          DATETIME       NOT NULL,
    [UpdatedById]          INT            NULL,
    [UpdatedDate]          DATETIME       NULL,
    CONSTRAINT [PK_FieldRenderingTypes] PRIMARY KEY CLUSTERED ([FieldRenderingTypeId] ASC)
);
GO

