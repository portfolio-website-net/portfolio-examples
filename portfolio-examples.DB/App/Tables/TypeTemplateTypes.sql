﻿CREATE TABLE [App].[TypeTemplateTypes] (
    [TypeTemplateTypeId] INT              IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (100)   NOT NULL,
    [Code]               NVARCHAR (50)    NOT NULL,
    [RenderAsFormInd]    BIT              NOT NULL,
    [Order]              INT              NOT NULL,
    [Guid]               UNIQUEIDENTIFIER NULL,
    [CreatedById]        INT              NOT NULL,
    [CreatedDate]        DATETIME         NOT NULL,
    [UpdatedById]        INT              NULL,
    [UpdatedDate]        DATETIME         NULL,
    CONSTRAINT [PK_TypeTemplateTypes] PRIMARY KEY CLUSTERED ([TypeTemplateTypeId] ASC)
);


GO

