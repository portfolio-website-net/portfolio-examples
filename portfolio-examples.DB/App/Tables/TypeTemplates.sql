﻿CREATE TABLE [App].[TypeTemplates] (
    [TypeTemplateId]     INT              IDENTITY (1, 1) NOT NULL,
    [TypeId]             INT              NOT NULL,
    [TypeTemplateTypeId] INT              NULL,
    [Name]               NVARCHAR (100)   NOT NULL,
    [Code]               NVARCHAR (50)    NULL,
    [Content]            NVARCHAR (MAX)   NULL,
    [Order]              INT              NOT NULL,
    [Guid]               UNIQUEIDENTIFIER NULL,
    [CreatedById]        INT              NOT NULL,
    [CreatedDate]        DATETIME         NOT NULL,
    [UpdatedById]        INT              NULL,
    [UpdatedDate]        DATETIME         NULL,
    CONSTRAINT [PK_TypeTemplates] PRIMARY KEY CLUSTERED ([TypeTemplateId] ASC),
    CONSTRAINT [FK_TypeTemplates_Types] FOREIGN KEY ([TypeId]) REFERENCES [App].[Types] ([TypeId]),
    CONSTRAINT [FK_TypeTemplates_TypeTemplateTypes] FOREIGN KEY ([TypeTemplateTypeId]) REFERENCES [App].[TypeTemplateTypes] ([TypeTemplateTypeId])
);


GO

