﻿CREATE TABLE [App].[TypeWorkflowSteps] (
    [TypeWorkflowStepId] INT      IDENTITY (1, 1) NOT NULL,
    [TypeId]             INT      NOT NULL,
    [WorkflowStepId]     INT      NOT NULL,
    [CreatedById]        INT      NOT NULL,
    [CreatedDate]        DATETIME NOT NULL,
    [UpdatedById]        INT      NULL,
    [UpdatedDate]        DATETIME NULL,
    CONSTRAINT [PK_TypeWorkflowSteps] PRIMARY KEY CLUSTERED ([TypeWorkflowStepId] ASC),
    CONSTRAINT [FK_TypeWorkflowSteps_Types] FOREIGN KEY ([TypeId]) REFERENCES [App].[Types] ([TypeId]),
    CONSTRAINT [FK_TypeWorkflowSteps_WorkflowSteps] FOREIGN KEY ([WorkflowStepId]) REFERENCES [App].[WorkflowSteps] ([WorkflowStepId])
);

