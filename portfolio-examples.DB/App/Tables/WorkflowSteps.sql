﻿CREATE TABLE [App].[WorkflowSteps] (
    [WorkflowStepId]    INT              IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (100)   NOT NULL,
    [Code]              NVARCHAR (50)    NULL,
    [Order]             INT              NOT NULL,
    [StatusText]        NVARCHAR (MAX)   NULL,
    [EmailTemplateText] NVARCHAR (MAX)   NULL,
    [AssignmentTypeId]  INT              NULL,
    [Guid]              UNIQUEIDENTIFIER NULL,
    [CreatedById]       INT              NOT NULL,
    [CreatedDate]       DATETIME         NOT NULL,
    [UpdatedById]       INT              NULL,
    [UpdatedDate]       DATETIME         NULL,
    CONSTRAINT [PK_WorkflowSteps] PRIMARY KEY CLUSTERED ([WorkflowStepId] ASC),
    CONSTRAINT [FK_WorkflowSteps_AssignmentTypes] FOREIGN KEY ([AssignmentTypeId]) REFERENCES [App].[AssignmentTypes] ([AssignmentTypeId])
);


GO

