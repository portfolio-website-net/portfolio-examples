﻿CREATE TABLE [App].[ListItems] (
    [ListItemId]  INT              IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100)   NOT NULL,
    [Code]        NVARCHAR (50)    NULL,
    [Order]       INT              NOT NULL,
    [Guid]        UNIQUEIDENTIFIER NULL,
    [CreatedById] INT              NOT NULL,
    [CreatedDate] DATETIME         NOT NULL,
    [UpdatedById] INT              NULL,
    [UpdatedDate] DATETIME         NULL,
    CONSTRAINT [PK_ListItems] PRIMARY KEY CLUSTERED ([ListItemId] ASC)
);


GO

