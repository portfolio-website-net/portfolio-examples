﻿CREATE TABLE [App].[Agreements] (
    [AgreementId]           INT              IDENTITY (1, 1) NOT NULL,
    [TypeId]                INT              NOT NULL,
    [AgreementStatusTypeId] INT              NOT NULL,
    [Guid]                  UNIQUEIDENTIFIER NULL,
    [CreatedById]           INT              NOT NULL,
    [CreatedDate]           DATETIME         NOT NULL,
    [UpdatedById]           INT              NULL,
    [UpdatedDate]           DATETIME         NULL,
    CONSTRAINT [PK_Agreements] PRIMARY KEY CLUSTERED ([AgreementId] ASC),
    CONSTRAINT [FK_Agreements_AgreementStatusTypes] FOREIGN KEY ([AgreementStatusTypeId]) REFERENCES [App].[AgreementStatusTypes] ([AgreementStatusTypeId]),
    CONSTRAINT [FK_Agreements_Types] FOREIGN KEY ([TypeId]) REFERENCES [App].[Types] ([TypeId])
);



