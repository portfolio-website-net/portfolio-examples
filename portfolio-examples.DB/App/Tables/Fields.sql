﻿CREATE TABLE [App].[Fields] (
    [FieldId]              INT              IDENTITY (1, 1) NOT NULL,
    [ParentFieldId]        INT              NULL,
    [Name]                 NVARCHAR (100)   NOT NULL,
    [Code]                 NVARCHAR (50)    NULL,
    [Order]                INT              NOT NULL,
    [FieldTypeId]          INT              NULL,
    [FieldMultiplicityInd] BIT              NOT NULL,
    [MaxLength]            INT              NULL,
    [RequiredInd]          BIT              NOT NULL,
    [FieldRenderingTypeId] INT              NULL,
    [Guid]                 UNIQUEIDENTIFIER NULL,
    [CreatedById]          INT              NOT NULL,
    [CreatedDate]          DATETIME         NOT NULL,
    [UpdatedById]          INT              NULL,
    [UpdatedDate]          DATETIME         NULL,
    CONSTRAINT [PK_Fields] PRIMARY KEY CLUSTERED ([FieldId] ASC),
    CONSTRAINT [FK_Fields_FieldRenderingTypes] FOREIGN KEY ([FieldRenderingTypeId]) REFERENCES [App].[FieldRenderingTypes] ([FieldRenderingTypeId]),
    CONSTRAINT [FK_Fields_Fields1] FOREIGN KEY ([ParentFieldId]) REFERENCES [App].[Fields] ([FieldId]),
    CONSTRAINT [FK_Fields_FieldTypes] FOREIGN KEY ([FieldTypeId]) REFERENCES [App].[FieldTypes] ([FieldTypeId])
);


GO

