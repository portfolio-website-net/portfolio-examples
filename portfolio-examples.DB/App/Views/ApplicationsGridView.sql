﻿CREATE VIEW App.ApplicationsGridView AS

SELECT a.AgreementId AS AgreementId,
       (SELECT afv.[Value]
	      FROM App.AgreementFieldValues afv
		 INNER JOIN App.Fields f ON afv.FieldId = f.FieldId
		 WHERE afv.AgreementId = a.AgreementId
		   AND f.Code = 'EventName') AS [Name],
	   (SELECT li.[Name]
	      FROM App.AgreementFieldValues afv
		 INNER JOIN App.Fields f ON afv.FieldId = f.FieldId
		 INNER JOIN App.ListItems li ON afv.[Value] = li.ListItemId
		 WHERE afv.AgreementId = a.AgreementId
		   AND f.Code = 'EventType') AS [Type],
	   (SELECT li.[Name]
	      FROM App.AgreementFieldValues afv
		 INNER JOIN App.Fields f ON afv.FieldId = f.FieldId
		 INNER JOIN App.ListItems li ON afv.[Value] = li.ListItemId
		 WHERE afv.AgreementId = a.AgreementId
		   AND f.Code = 'EventLocation') AS [Location],
	   (SELECT afv.[Value]
	      FROM App.AgreementFieldValues afv
		 INNER JOIN App.Fields f ON afv.FieldId = f.FieldId
		 WHERE afv.AgreementId = a.AgreementId
		   AND f.Code = 'ContactInformation') AS Contact,
	   (SELECT TOP 1 afv.[Value]
	      FROM App.AgreementFieldValues afv
		 INNER JOIN App.Fields f ON afv.FieldId = f.FieldId
		 WHERE afv.AgreementId = a.AgreementId
		   AND f.Code = 'EventDates') AS Dates,
       ast.[Name] AS [Status]
  FROM App.Agreements a
 INNER JOIN App.AgreementStatusTypes ast ON a.AgreementStatusTypeId = ast.AgreementStatusTypeId
 WHERE ast.Code NOT IN ('Deleted')

GO

