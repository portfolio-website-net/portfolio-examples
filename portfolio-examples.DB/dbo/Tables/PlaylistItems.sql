﻿CREATE TABLE [dbo].[PlaylistItems] (
    [PlaylistItemId] INT            IDENTITY (1, 1) NOT NULL,
    [StationId]      INT            NOT NULL,
    [Time]           DATETIME       NOT NULL,
    [Artist]         NVARCHAR (200) NOT NULL,
    [Song]           NVARCHAR (200) NOT NULL,
    [CreatedById]    INT            NOT NULL,
    [CreatedDate]    DATETIME       NOT NULL,
    [UpdatedById]    INT            NULL,
    [UpdatedDate]    DATETIME       NULL,
    CONSTRAINT [PK_PlaylistItems] PRIMARY KEY CLUSTERED ([PlaylistItemId] ASC),
    CONSTRAINT [FK_PlaylistItems_Stations] FOREIGN KEY ([StationId]) REFERENCES [dbo].[Stations] ([StationId])
);
GO

