﻿CREATE TABLE [dbo].[ApplicationSettings] (
    [ApplicationSettingId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (100) NULL,
    [Value]                NVARCHAR (MAX) NULL,
    [CreatedById]          INT            NOT NULL,
    [CreatedDate]          DATETIME       NOT NULL,
    [UpdatedById]          INT            NULL,
    [UpdatedDate]          DATETIME       NULL,
    CONSTRAINT [PK_ApplicationSettings] PRIMARY KEY CLUSTERED ([ApplicationSettingId] ASC)
);
GO

