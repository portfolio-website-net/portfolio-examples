﻿CREATE TABLE [dbo].[Stations] (
    [StationId]   INT            IDENTITY (1, 1) NOT NULL,
    [Url]         NVARCHAR (200) NOT NULL,
    [FrameId]     NVARCHAR (50)  NULL,
    [Script]      NVARCHAR (MAX) NOT NULL,
    [CreatedById] INT            NOT NULL,
    [CreatedDate] DATETIME       NOT NULL,
    [UpdatedById] INT            NULL,
    [UpdatedDate] DATETIME       NULL,
    CONSTRAINT [PK_Stations] PRIMARY KEY CLUSTERED ([StationId] ASC)
);
GO

