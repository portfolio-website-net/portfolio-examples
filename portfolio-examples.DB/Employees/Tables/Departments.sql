﻿CREATE TABLE [Employees].[Departments] (
    [DepartmentId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (100) NOT NULL,
    [CreatedById]  INT            NOT NULL,
    [CreatedDate]  DATETIME       NOT NULL,
    [UpdatedById]  INT            NULL,
    [UpdatedDate]  DATETIME       NULL,
    CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED ([DepartmentId] ASC)
);
GO

