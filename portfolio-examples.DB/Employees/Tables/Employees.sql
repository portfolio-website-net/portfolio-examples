﻿CREATE TABLE [Employees].[Employees] (
    [EmployeeId]   INT             IDENTITY (1, 1) NOT NULL,
    [DepartmentId] INT             NOT NULL,
    [Date]         DATE            NOT NULL,
    [LastName]     NVARCHAR (100)  NOT NULL,
    [FirstName]    NVARCHAR (100)  NOT NULL,
    [Position]     NVARCHAR (100)  NOT NULL,
    [Salary]       DECIMAL (18, 2) NULL,
    [Wage]         DECIMAL (18, 2) NULL,
    [Page]         INT             NOT NULL,
    [CreatedById]  INT             NOT NULL,
    [CreatedDate]  DATETIME        NOT NULL,
    [UpdatedById]  INT             NULL,
    [UpdatedDate]  DATETIME        NULL,
    CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED ([EmployeeId] ASC),
    CONSTRAINT [FK_Employees_Departments] FOREIGN KEY ([DepartmentId]) REFERENCES [Employees].[Departments] ([DepartmentId])
);
GO

