﻿CREATE TABLE [Employees].[EmployeePages] (
    [EmployeePageId] INT      IDENTITY (1, 1) NOT NULL,
    [Date]           DATE     NOT NULL,
    [PageCount]      INT      NOT NULL,
    [CreatedById]    INT      NOT NULL,
    [CreatedDate]    DATETIME NOT NULL,
    [UpdatedById]    INT      NULL,
    [UpdatedDate]    DATETIME NULL,
    CONSTRAINT [PK_EmployeePages] PRIMARY KEY CLUSTERED ([EmployeePageId] ASC)
);
GO

