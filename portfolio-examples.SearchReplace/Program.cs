﻿// <copyright file="Program.cs" company="Portfolio Website">
// Copyright (c) Portfolio Website. All rights reserved.
// Licensed under the Apache License, Version 2.0. See the LICENSE file in the project root for full license information.
// </copyright>

namespace PortfolioExamples.SearchReplace
{
    using System;
    using System.Configuration;
    using System.IO;

    public class Program
    {
        public static void Main(string[] args)
        {
            string rootfolder = ConfigurationManager.AppSettings["RootFolder"];
            string[] files = Directory.GetFiles(rootfolder, "*.*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                if (Path.GetExtension(file.ToLower()) == ".txt"
                    || Path.GetExtension(file.ToLower()) == ".html")
                {
                    try
                    {
                        var contents = File.ReadAllText(file);
                        contents = contents.Replace(@"http:\/\/staging.portfolio-examples.com\/", @"\/");
                        contents = contents.Replace(@"http:\/\/staging.portfolio-examples.com", @"\/");
                        contents = contents.Replace(@"http://staging.portfolio-examples.com/", @"/");
                        contents = contents.Replace("target=\"_blank\"", "urltargetblank");
                        contents = contents.Replace(" rel=\"noopener noreferrer\"", string.Empty);
                        contents = contents.Replace(" rel=\"noopener\"", string.Empty);
                        contents = contents.Replace("urltargetblank", "target=\"_blank\" rel=\"noopener noreferrer\"");

                        File.SetAttributes(file, FileAttributes.Normal);
                        File.WriteAllText(file, contents);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                if (file.ToLower().EndsWith(".min.js.gz")
                    || file.ToLower().EndsWith(".min.js.txt")
                    || file.ToLower().EndsWith(".min.css.gz")
                    || file.ToLower().EndsWith(".min.css.txt"))
                {
                    File.Delete(file);
                }
            }
        }
    }
}
